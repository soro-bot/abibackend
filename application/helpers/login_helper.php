<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('atlantis_css_url'))
{
	function atlantis_css_url($nom)
	{
		return base_url() . 'assets/atlantis_assets/dist/css/' . $nom . '.css';
	}
}

if ( ! function_exists('login_css_url'))
{
	function login_css_url($nom)
	{
		return base_url() . 'assets/login_assets/css/' . $nom . '.css';
	}
}

if ( ! function_exists('login_js_url'))
{
	function login_js_url($nom)
	{
		return base_url() . 'assets/login_assets/js/' . $nom . '.js';
	}
}

if ( ! function_exists('login_img_url'))
{
	function login_img_url($nom)
	{
		return base_url() . 'assets/login_assets/img/' . $nom;
	}
}

if ( ! function_exists('login_libraries_url'))
{
	function login_libraries_url($nom)
	{
		return base_url() . 'assets/login_assets/libraries/' . $nom;
	}
}

if ( ! function_exists('login_plugins_url'))
{
	function login_plugins_url($nom)
	{
		return base_url() . 'assets/login_assets/plugins/' . $nom;
	}
}

