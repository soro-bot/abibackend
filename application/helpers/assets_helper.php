<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('logo_url'))
{
	 function logo_url($lien_image)
	 { 
	  	return base_url().$lien_image;
	 }
}


if ( ! function_exists('bowers_url'))
{
	function bowers_url()
	{
		return base_url() . 'assets/atlantis_assets/bower_components/';
	}
}

if ( ! function_exists('dist_url'))
{
	function dist_url()
	{
		return base_url() . 'assets/atlantis_assets/dist/';
	}
}

if ( ! function_exists('img_url'))
{
	function img_url()
	{
		return base_url() . 'assets/atlantis_assets/dist/images/';
	}
}

if ( ! function_exists('decontaminate_data'))
{
	function decontaminate_data($post){
		foreach($post as $key => $value){
			$data = trim($value);
			$data = stripslashes($data);
			$data = htmlspecialchars($data);
			$post[$key] = $data;
		}

		return $post;
	}
}



