<!DOCTYPE html>
<html lang="fr">
<head>
    <?php $this->load->view('tpl/login_css_files'); ?>
</head>
<body>

    <div class="modal fade" id="modal_confirm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel3"
         aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×</button>
                    <h3 class="modal-title" id="myModalLabel"><?php echo $this->lang->line('confirm_modal_titre'); ?></h3>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <!-- BEGIN HEADER -->
        <div class="header mb30">
            <div class="menu-bar">
                <div class="row" style="margin: 0">
                    <div class="col-sm-5 col-md-offset-1 logo">
                        <!-- <img src="<?php echo img_url("logo_white.png") ?>"/> -->
                    </div>
                    <div class="col-sm-5 col-sm-offset-1" style="text-align: right">
                        <select style="width: auto; float: right; margin-top: 10px" class="form-control" onchange="javascript:window.location.href='<?php echo site_url(); ?>/LanguageSwitcher/switchLang/'+this.value;">
                            <option value="francais" <?php if($this->session->userdata('site_lang') == "francais") echo 'selected="selected"'; ?>><?php echo $this->lang->line('francais'); ?></option>
                            <option value="english" <?php if($this->session->userdata('site_lang') == "english") echo 'selected="selected"'; ?>><?php echo $this->lang->line('anglais'); ?></option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <!-- END HEADER -->

        <div class="tab-block" style="margin-right: 100px;">
        <form id="regForm"  action="<?php echo site_url('Accueil/login')?>" method="post" role="form" autocomplete="off">
        <legend style="font-size: 25px; text-align: center;"><b><?php echo $this->lang->line('connexion_titre'); ?></b></legend>
        <div class="form-body" style="position: center;">
        <!-- One "tab" for each step in the form: -->
            <div class="form-row">
              <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('login_label'); ?></label>
                <div class="col-sm-8 input-group">
                    <div class="input-group-addon">
                         <i class="fa fa-at"></i>
                    </div>
                   <input type="email" class="form-control" placeholder="<?php echo $this->lang->line('login_label'); ?>" required name="login" onpaste="return false" autocomplete="false"/>
                </div><!-- /.input group -->
              </div>
            </div>
            <div class="form-row">
              <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('mdp_label'); ?></label>
                <div class="col-sm-8 input-group">
                    <div class="input-group-addon">
                         <i class="fa fa-lock"></i>
                    </div>
                  <input type="password" class="form-control" placeholder="<?php echo $this->lang->line('mdp_label'); ?>" required name="password" onpaste="return false" autocomplete="false"/>
                </div><!-- /.input group -->
              </div>
            </div>
            <div class="form-row text-center" style="padding-bottom: 10px;">
              <button type="submit" class="btn btn-warning btn-lg"><?php echo $this->lang->line('connecter_btn'); ?></button>
            </div>

             <div class="form-row text-center" style="padding-bottom: 20px;">
              <a class="btn btn-default" data-target="#myReduction" data-toggle="modal" id="#btn_approvision"><i class="fa fa-lock"></i> <?php echo $this->lang->line('mdp_forget'); ?></a>
            </div>
        </div>

        </form>
        </div>

         <div class="modal fade" id="myReduction" tabindex="1" role="dialog" aria-labelledby="my_modalLabel-1">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><b style="color: #f87114;"><?php echo $this->lang->line('mdp_forget'); ?></b></h4>
              </div>
              <form class="comfirms_1"  action="<?php echo site_url('Accueil/reloadPassword')?>" method="post" role="form" autocomplete="off">
              <input type="hidden" name="envoyer" value="envoyer" />
              <div class="modal-body">
                <div class="form-group"> 
                  <label for="text" class="col-sm-4 control-label"><?php echo $this->lang->line('login_label'); ?></label> 
                  <div class="col-sm-8"> 
                  <input type="email" class="form-control" name="login" placeholder="<?php echo $this->lang->line('login_label'); ?>" autocomplete="false" onpaste="return false"/> 
                  </div> 
                </div>  
               
              </div>
              <div class="modal-footer" style="text-align: center; margin-top: 25px;">
                <a class="btn btn-default" data-dismiss="modal" class="form-control" ><?php echo $this->lang->line('annuler_btn'); ?></a>
                <button id="comfirms_1" type="button" class="btn btn-primary"><?php echo $this->lang->line('valider_btn'); ?></button>
              </div>
             </form>
            </div>
          </div>
        </div>

    </div>
<div class="md-overlay"></div>
<?php $this->load->view('tpl/login_js_files'); ?>
<script type="text/javascript">
$('#regForm').disableAutoFill();

$('#btn_approvision').click(function ()
{ 
    $('#myReduction').modal('show');
});

jQuery(document).ready(function() {
  <?php
      if ($this->session->flashdata("success")){
          echo "toast_success('".$this->session->flashdata("success")."');";
      }

      if ($this->session->flashdata("error")){
          echo "toast_error('".$this->session->flashdata("error")."');";
      }
  ?>
});

    
</script>
<!--    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
</body>
</html>
