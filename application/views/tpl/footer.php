<?php
/**
 * User: Abou KONATE
 * Date: 01/08/2020
 */ 
?>
<div class="footer">
    <div class="footer-inner">
        <div class="footer-content">
            <span class="bigger-120">
                @ <?php echo($this->lang->line('Copyright')); ?> <span class="blue bolder"> <a href="https://sycapay.net" target="_blank">Syca Sas</a></span>
                 
            </span>

            &nbsp; &nbsp;
            <span class="action-buttons" style="display: none;">
                <a href="#">
                    <i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
                </a>

                <a href="#">
                    <i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
                </a>

                <a href="#">
                    <i class="ace-icon fa fa-rss-square orange bigger-150"></i>
                </a>
            </span>
        </div>
    </div>
</div>