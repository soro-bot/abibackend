﻿<?php
/**
 * User: Abou KONATE
 * Date: 01/08/2020
 */ 
?>
<!-- jQuery 3 -->
<script src="<?php echo bowers_url(); ?>jquery/dist/jquery-3.6.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo bowers_url(); ?>jquery-ui/jquery-ui.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo bowers_url(); ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo bowers_url(); ?>DataTables/datatables.min.js"></script>
<!-- ============================================================== -->
<!--Custom JavaScript -->
<script src="<?php echo dist_url(); ?>confirms/confirms.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<!-- page specific plugin scripts -->
<!--[if lte IE 8]>
<script src="assets/js/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo dist_url(); ?>js/jquery-ui.custom.min.js"></script>
<script src="<?php echo dist_url(); ?>js/jquery.ui.touch-punch.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script>
<!-- ace scripts -->
<script src="<?php echo dist_url(); ?>js/ace-elements.min.js"></script>
<script src="<?php echo dist_url(); ?>js/ace.min.js"></script>
<script src="<?php echo login_js_url('jquery.toast')?>" type="text/javascript"></script>
<!-- page script -->
<script type="text/javascript">


    <?php
    if ($this->session->flashdata("success")){
        echo "toast_success('".$this->session->flashdata("success")."');";
    }
    ?>
    <?php
    if ($this->session->flashdata("error")){
        echo "toast_error('".$this->session->flashdata("error")."');";
    }
    ?>

  $('#comfirms_1').on("click", function () {
   $( '.comfirms_1' ).find( 'select, textarea, input' ).each(function()
   {
      if( ! $( this ).prop( 'required' ))
      {

      }
      else
      {
          if ( ! $( this ).val() ) {
              fail = true;
              name = $( this ).attr( 'name' );
              fail_log += name + " is required \n";
          }
      }
  });

});

$('#comfirms_2').on("click", function () {
     $( '.comfirms_2' ).find( 'select, textarea, input' ).each(function()
     {
        if( ! $( this ).prop( 'required' ))
        {

        }
        else
        {
            if ( ! $( this ).val() ) {
                fail = true;
                name = $( this ).attr( 'name' );
                fail_log += name + " is required \n";
            }
        }
    });

});


$('#comfirms_1').confirm({
    theme: 'material',
    title: 'CONFIRMATION !',
    content: '<?php echo $this->lang->line('confirm_modal_text'); ?>',
    buttons: {
        cancel: {
        text: '<?php echo $this->lang->line('confirm_modal_annuler_btn'); ?>',
        btnClass: 'btn-default',
      },
        confirm: {
        text: '<?php echo $this->lang->line('confirm_modal_valider_btn'); ?>',
        btnClass: 'btn-primary',
        action: function () {
              $(".comfirms_1").submit();
          }
        },
    },

});

$('#comfirms_2').confirm({
    theme: 'material',
    title: 'CONFIRMATION !',
    content: '<?php echo $this->lang->line('confirm_modal_text'); ?>',
    buttons: {
        cancel: {
        text: '<?php echo $this->lang->line('confirm_modal_annuler_btn'); ?>',
        btnClass: 'btn-default',
      },
        confirm: {
        text: '<?php echo $this->lang->line('confirm_modal_valider_btn'); ?>',
        btnClass: 'btn-primary',
        action: function () {
            $(".comfirms_2").submit();
          }
        },
    }
});

function toast_warning(texte){
        $.toast({
                text: texte, // Text that is to be shown in the toast
                heading: 'Attention', // Optional heading to be shown on the toast
                icon: 'warning', // Type of toast icon
                showHideTransition: 'fade', // fade, slide or plain
                allowToastClose: true, // Boolean value true or false
                hideAfter: 5000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time

                position: 'top-right', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values


                textAlign: 'left',  // Text alignment i.e. left, right or center
                loader: true,  // Whether to show loader or not. True by default
                loaderBg: '#9EC600',  // Background color of the toast loader

        });
}
function toast_information(texte){
        $.toast({
                text: "texte", // Text that is to be shown in the toast
                heading: 'Information', // Optional heading to be shown on the toast
                icon: 'info', // Type of toast icon
                showHideTransition: 'fade', // fade, slide or plain
                allowToastClose: true, // Boolean value true or false
                hideAfter: 5000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time

                position: 'top-right', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values


                textAlign: 'left',  // Text alignment i.e. left, right or center
                loader: true,  // Whether to show loader or not. True by default
                loaderBg: '#9EC600',  // Background color of the toast loader

        });
}
function toast_success(texte){
        $.toast({
                text: texte, // Text that is to be shown in the toast
                heading: 'Succes', // Optional heading to be shown on the toast
                icon: 'success', // Type of toast icon
                showHideTransition: 'fade', // fade, slide or plain
                allowToastClose: true, // Boolean value true or false
                hideAfter: 5000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time

                position: 'top-right', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values


                textAlign: 'left',  // Text alignment i.e. left, right or center
                loader: true,  // Whether to show loader or not. True by default
                loaderBg: '#9EC600',  // Background color of the toast loader

        });
}
function toast_error(texte){
        $.toast({
                text: texte, // Text that is to be shown in the toast
                heading: 'Erreur', // Optional heading to be shown on the toast
                icon: 'error', // Type of toast icon
                showHideTransition: 'fade', // fade, slide or plain
                allowToastClose: true, // Boolean value true or false
                hideAfter: 5000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time

                position: 'top-right', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values


                textAlign: 'left',  // Text alignment i.e. left, right or center
                loader: true,  // Whether to show loader or not. True by default
                loaderBg: '#9EC600',  // Background color of the toast loader

        });
}


</script>
