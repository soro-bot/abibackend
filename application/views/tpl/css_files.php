<?php
/**
 * User: Abou KONATE
 * Date: 01/08/2020
 */ 
?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $page_title ; ?></title>
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="<?php echo dist_url(); ?>wizard/style.css" class="ace-main-stylesheet" id="main-ace-style" />
<link rel="stylesheet" href="<?php echo bowers_url(); ?>bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo bowers_url(); ?>font-awesome/css/font-awesome.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo bowers_url(); ?>DataTables/datatables.min.css">
<!-- page specific plugin styles -->
<link rel="stylesheet" href="<?php echo dist_url(); ?>confirms/confirms.css">
<!-- text fonts -->
<link rel="stylesheet" href="<?php echo dist_url(); ?>css/fonts.googleapis.com.css" />
<!-- ace styles -->
<link rel="stylesheet" href="<?php echo dist_url(); ?>css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<!-- Favicon icon -->
<link rel="shortcut icon" href="<?php echo base_url("assets/login_assets/img/logo.png"); ?>" style="width: 100%; height: 100%;"/>

<!-- Latest compiled and minified CSS -->

<!--[if lte IE 9]>
<link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
<![endif]-->
<link href="<?php echo login_css_url("jquery.toast")?>" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo dist_url(); ?>css/ace-skins.min.css" />
<link rel="stylesheet" href="<?php echo dist_url(); ?>css/ace-rtl.min.css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

<link href="<?php echo dist_url()?>css/custom.css" rel="stylesheet" type="text/css" />

<!--[if lte IE 9]>
 <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
<![endif]-->
<!-- inline styles related to this page -->
<!-- ace settings handler -->
<script src="<?php echo dist_url(); ?>js/ace-extra.min.js"></script>
<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
<!--[if lte IE 8]>
<script src="assets/js/html5shiv.min.js"></script>
<script src="assets/js/respond.min.js"></script>
<![endif]-->
