<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo login_js_url('jquery-3.2.1.min')?>" type="text/javascript"></script>
<script src="<?php echo login_js_url('bootstrap.min')?>" type="text/javascript"></script>
<script src="<?php echo login_js_url('jquery.toast')?>" type="text/javascript"></script>

<script src="<?php echo login_js_url('form-validation')?>" type="text/javascript"></script>
<script src="<?php echo login_plugins_url('confirms.js')?>" type="text/javascript"></script>

<!-- END CORE PLUGINS -->
<script>
$('#comfirms_1').on("click", function () {
   $( '.comfirms_1' ).find( 'select, textarea, input' ).each(function()
   {
      if( ! $( this ).prop( 'required' ))
      {

      }
      else
      {
          if ( ! $( this ).val() ) {
              fail = true;
              name = $( this ).attr( 'name' );
              fail_log += name + " is required \n";
          }
      }
  });

});

function toast_warning(texte){
        $.toast({
                text: texte, // Text that is to be shown in the toast
                heading: 'Attention', // Optional heading to be shown on the toast
                icon: 'warning', // Type of toast icon
                showHideTransition: 'fade', // fade, slide or plain
                allowToastClose: true, // Boolean value true or false
                hideAfter: 5000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time

                position: 'top-right', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values


                textAlign: 'left',  // Text alignment i.e. left, right or center
                loader: true,  // Whether to show loader or not. True by default
                loaderBg: '#9EC600',  // Background color of the toast loader

        });
}
function toast_information(texte){
        $.toast({
                text: "texte", // Text that is to be shown in the toast
                heading: 'Information', // Optional heading to be shown on the toast
                icon: 'info', // Type of toast icon
                showHideTransition: 'fade', // fade, slide or plain
                allowToastClose: true, // Boolean value true or false
                hideAfter: 5000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time

                position: 'top-right', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values


                textAlign: 'left',  // Text alignment i.e. left, right or center
                loader: true,  // Whether to show loader or not. True by default
                loaderBg: '#9EC600',  // Background color of the toast loader

        });
}
function toast_success(texte){
        $.toast({
                text: texte, // Text that is to be shown in the toast
                heading: 'Succes', // Optional heading to be shown on the toast
                icon: 'success', // Type of toast icon
                showHideTransition: 'fade', // fade, slide or plain
                allowToastClose: true, // Boolean value true or false
                hideAfter: 5000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time

                position: 'top-right', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values


                textAlign: 'left',  // Text alignment i.e. left, right or center
                loader: true,  // Whether to show loader or not. True by default
                loaderBg: '#9EC600',  // Background color of the toast loader

        });
}
function toast_error(texte){
        $.toast({
                text: texte, // Text that is to be shown in the toast
                heading: 'Erreur', // Optional heading to be shown on the toast
                icon: 'error', // Type of toast icon
                showHideTransition: 'fade', // fade, slide or plain
                allowToastClose: true, // Boolean value true or false
                hideAfter: 5000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time

                position: 'top-right', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values


                textAlign: 'left',  // Text alignment i.e. left, right or center
                loader: true,  // Whether to show loader or not. True by default
                loaderBg: '#9EC600',  // Background color of the toast loader

        });
}

</script>

<!-- END JAVASCRIPTS -->
