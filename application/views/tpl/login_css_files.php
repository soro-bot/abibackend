<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $page_title ; ?></title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Titillium+Web:300" rel="stylesheet">
<link href="<?php echo login_css_url("bootstrap.min")?>" rel="stylesheet" type="text/css" />
<link href="<?php echo login_css_url("font-awesome/css/font-awesome.min")?>" rel="stylesheet" type="text/css" />
<link href="<?php echo login_css_url("jquery.toast")?>" rel="stylesheet" type="text/css" />

<!-- Librairie -->
<link href="<?php echo login_css_url("jquery-filestyle.min")?>" rel="stylesheet" type="text/css" />
<link href="<?php echo login_css_url("style")?>" rel="stylesheet" type="text/css" />
<link href="<?php echo login_css_url("jquery-ui")?>" rel="stylesheet" type="text/css" />

<!-- Widzard et logo -->
<link rel="stylesheet" href="<?php echo login_plugins_url('confirms.css'); ?>">
<link rel="shortcut icon" href="<?php echo base_url("assets/login_assets/img/logo.png"); ?>" style="width: 100%; height: 100%;"/>


