<?php
/**
 * User: Abou KONATE
 * Date: 01/08/2020
 */ 
?>
<div id="navbar" class="navbar navbar-default ace-save-state navbar-fixed-top" style="background: #ffb848;">
<div class="navbar-container ace-save-state" id="navbar-container">
<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
<span class="sr-only">Toggle sidebar</span>

<span class="icon-bar"></span>

<span class="icon-bar"></span>

<span class="icon-bar"></span>
</button>

<div class="navbar-header pull-left">
<a href="<?php echo site_url('Dashboard');?>" class="navbar-brand">
    <?php if($this->session->userdata('ent_raison')) { ?>
    <small>
        <?php if($this->session->userdata('fk_pays')) { ?>
        <img src="<?php echo img_url(); ?>gallery/<?php echo $this->session->userdata('fk_pays'); ?>.png" alt="<?php echo $this->session->userdata('fk_pays'); ?>" width="25" height="18"/>
        <?php }else{ ?>
        <i class="fa fa-bank"></i>
        <?php }; ?>
        <?php echo($this->session->userdata('ent_raison')); ?>
    </small>
    <?php }else{ ?>
    <small>
        <?php if($this->session->userdata('fk_pays')) { ?>
        <img src="<?php echo img_url(); ?>gallery/<?php echo $this->session->userdata('fk_pays'); ?>.png" alt="<?php echo $this->session->userdata('fk_pays'); ?>" width="25" height="18"/>
        <?php }else{ ?>
        <i class="fa fa-bank"></i>
        <?php }; ?>
        Atlantic Business International
    </small>
    <?php }; ?>
</a>
</div>

<div class="navbar-buttons navbar-header pull-right" role="navigation">
    <ul class="nav ace-nav">

        <li class="light-orange dropdown-modal" style="background-color: #999999 !important;">
            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                <img class="nav-user-photo" src="<?php echo img_url(); ?>avatars/avatar2.png" alt="<?php echo($this->session->userdata('nom_user')); ?>" />
                <span class="user-info">
                    <small><?php echo($this->lang->line('bienvenue_text')); ?>,</small>
                    <?php echo($this->session->userdata('nom_user')); ?>&nbsp;
              <?php echo($this->session->userdata('prenoms_user')); ?>
                </span>

                <i class="ace-icon fa fa-caret-down"></i>
            </a>

            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">

                <li>
                    <a href="<?php echo site_url('Profil');?>">
                        <i class="ace-icon fa fa-user"></i>
                        <?php echo($this->lang->line('profil_text')); ?>
                    </a>
                </li>

                <li class="divider"></li>

                <li>
                    <a href="<?php echo site_url('Deconnexion');?>">
                        <i class="ace-icon fa fa-power-off"></i>
                        <?php echo($this->lang->line('menu_logout')); ?>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>
</div><!-- /.navbar-container -->
</div>