<!DOCTYPE html>
<html lang="fr">
<head>
<?php $this->load->view('tpl/css_files'); ?>
</head>

<body class="skin-2 no-skin">
        
        <!-- Navbar -->
          <?php $this->load->view('tpl/header'); ?>
        <!-- /.navbar -->

        <div class="main-container ace-save-state" id="main-container">
            <script type="text/javascript">
                try{ace.settings.loadState('main-container')}catch(e){}
            </script>

            <?php 
              $data['page'] = $page;
              $this->load->view('tpl/sidebar', $data);
            ?>

            <div class="main-content">
                <div class="main-content-inner">
                    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="ace-icon fa fa-home home-icon"></i>
                                <a href="#">Accueil - Home</a>
                            </li>
                            <li class="active"><?php echo $page_title ?></li>
                        </ul><!-- /.breadcrumb -->

                        <div class="nav-search" id="nav-search" style="display: none;">
                            <form class="form-search">
                                <span class="input-icon">
                                    <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                                    <i class="ace-icon fa fa-search nav-search-icon"></i>
                                </span>
                            </form>
                        </div><!-- /.nav-search -->
                    </div>

                    <div class="page-content">

                        <?php $this->load->view('tpl/setting'); ?>


                        <div class="row">
                            <div class="col-xs-12">
                                <!-- PAGE CONTENT BEGINS -->
                                <!--div class="alert alert-block text-center">
                                    <form class="form-inline" action="<?php echo site_url('Transactions/rechercher');?>" method="post" role="form">
                                        <div class="table-toolbar margin-bottom-25" align="center" style="margin-bottom: 25px;">
                                            <div class="form-body">
                                                <span class="form-group"><?php echo $this->lang->line('text_periode'); ?> :</span>
                                                <div class="form-group">
                                                    <div class="input-group date">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="date" class="form-control"  name="date_min"
                                                               value="<?php echo $this->session->userdata('date_min'); ?>" placeholder="Début" required="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="input-group date">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="date" class="form-control" value="<?php echo $this->session->userdata('date_max'); ?>" name="date_max" placeholder="Final" required="">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="input-group date">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-home"></i>
                                                        </div>
                                                        <select class="form-control input-sm " style="width: 100%" id="statut" name="statut">
                                                            <option value="0" <?php if($this->session->userdata('statut') == '0') echo "selected=true"; ?>> <?php echo($this->lang->line('label_tous')); ?> </option>
                                                            <option value="S" <?php if($this->session->userdata('statut') == 'S') echo "selected=true"; ?>><?php echo($this->lang->line('label_succes')); ?></option>
                                                            <option value="E" <?php if($this->session->userdata('statut') == 'E') echo "selected=true"; ?>><?php echo($this->lang->line('label_echec')); ?></option>
                                                            <option value="P" <?php if($this->session->userdata('statut') == 'P') echo "selected=true"; ?>><?php echo($this->lang->line('label_pending')); ?></option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <input class="form-control btn-warning" type="submit" name="search"  value="<?php echo($this->lang->line('valider_btn')); ?>">
                                            </div>

                                    </form>
                                </div-->

                                <div class="hr hr32 hr-dotted"></div>

                                <div class="row">
                                    <table id="table" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
											<th>N°Quittance Sinistre</th>
											<th>Bénéficiaire</th>
											<th>Sinistre</th>
											<th>Mobile</th>
											<th>Libellé opération</th>
											<th>Montant</th>
											<th>Reference</th>
											<th>Statut</th>
											<th>Date traitement</th>
                                        </tr>
                                        </thead>
                                        <tbody>
											<?php foreach ($demandes as $row) : ?>
												<tr>
													<td><?php echo $row->quittance_sinistre ?></td>
													<td><?php echo $row->beneficiaire ?></td>
													<td><?php echo $row->numero_sinistre ?></td>
													<td><?php echo $row->mobile_demande ?></td>
													<td><?php echo $row->details_demande ?></td>
													<td><?php echo $row->montant_demande ?></td>
													<td><?php echo $row->reference_traitement ?></td>
													<td><?php if ($row->statut_demande == "T") : ?>
														<?php echo "<span style='color: blue'>Traitement en cours</span>" ?>
													<?php elseif ($row->statut_demande == "P") : ?>
														<?php echo "<span style='color: blue'>En attente de traitement</span>" ?>
													<?php elseif ($row->statut_demande == "S") : ?>
														<?php echo "<span style='color: green'>Succès</span>" ?>
													<?php elseif ($row->statut_demande == "E") : ?>
														<?php echo "<span style='color: red'>Echec</span> (". $row->details_traitement.")" ?>
													<?php endif; ?></td>
													<td><?php echo $row->date_traitement ?></td>
												</tr>
											<?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- PAGE CONTENT ENDS -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.page-content -->

                    <?php $this->load->view('tpl/footer'); ?>

                    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
                    </a>
                </div><!-- /.main-container -->

                <!-- basic scripts -->
                <?php $this->load->view('tpl/js_files'); ?>
                <script type="text/javascript">


                    $("#button").click(function()
                    {
                        document.location.href = '<?php echo site_url('Transactions/exportFormat') ; ?>';
                    });

                    var table;
                    $(document).ready(function() {


                        $('#import_form').on('submit', function(event){
                            event.preventDefault();
                            $.ajax({
                                url:"<?php echo base_url(); ?>index.php/Remboursements/import",
                                method:"POST",
                                data:new FormData(this),
                                contentType:false,
                                cache:false,
                                processData:false,
                                success:function(data){
                                    $('#file').val('');
                                    //load_data();
                                    alert("Données inmportée avec succès");
                                    location.href = "<?php echo site_url("Remboursements"); ?>"

                                }
                            })
                        });



                        //datatables
                        table = $('#table').DataTable({
                            "ordering": false,
                            "processing": true, //Feature control the processing indicator.
                            "serverSide": false, //Feature control DataTables' server-side processing mode.
                            "dom":  "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>" +
                            "<'row'<'col-sm-12'tr>>" +
                            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                            "buttons": [
                                { extend: 'excel', className: 'btn-default', text: 'EXPORT EXCEL'},
                                { extend: 'pdf', className: 'btn-default', text: 'EXPORT PDF' },
                            ],
                            "columnDefs": [ { orderable: false, targets: [0] } ],
                            "language" : {
                                "sEmptyTable":     "<?php echo $this->lang->line('sEmptyTable'); ?>",
                                "sInfo":           "<?php echo $this->lang->line('sInfo'); ?>",
                                "sInfoEmpty":      "<?php echo $this->lang->line('sInfoEmpty'); ?>",
                                "sInfoFiltered":   "<?php echo $this->lang->line('sInfoFiltered'); ?>",
                                "sInfoPostFix":    "",
                                "sInfoThousands":  ",",
                                "sLengthMenu":     "<?php echo $this->lang->line('sLengthMenu'); ?>",
                                "sLoadingRecords": "<?php echo $this->lang->line('sLoadingRecords'); ?>",
                                "sProcessing":     "<?php echo $this->lang->line('sProcessing'); ?>",
                                "sSearch":         "<?php echo $this->lang->line('sSearch'); ?>",
                                "sZeroRecords":    "<?php echo $this->lang->line('sZeroRecords'); ?>",
                                "oPaginate": {
                                    "sFirst":    "<?php echo $this->lang->line('sFirst'); ?>",
                                    "sLast":     "<?php echo $this->lang->line('sLast'); ?>",
                                    "sNext":     "<?php echo $this->lang->line('sNext'); ?>",
                                    "sPrevious": "<?php echo $this->lang->line('sPrevious'); ?>"
                                },
                                "oAria": {
                                    "sSortAscending":  "<?php echo $this->lang->line('sSortAscending'); ?>",
                                    "sSortDescending": "<?php echo $this->lang->line('sSortDescending'); ?>"
                                },
                                "select": {
                                    "rows": {
                                        "_": "<?php echo $this->lang->line('_'); ?>",
                                        "0": "<?php echo $this->lang->line('0'); ?>",
                                        "1": "<?php echo $this->lang->line('1'); ?>"
                                    }
                                }
                            }
                        });

                    });
                </script>


</body>
</html>
