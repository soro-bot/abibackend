<!DOCTYPE html>
<html>
<head>
   <?php $this->load->view('tpl/css_files'); ?>
</head>
<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('tpl/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php 
  $data['page'] = "trace_actions";
  $this->load->view('tpl/sidebar', $data); 
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Trace actions
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('Dashboard');?>"><i class="fa fa-dashboard"></i> Accueil</a></li>
        <li class="active">Trace actions</li>
      </ol>
    </section>
  <!-- Main content -->
  <section class="content">
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
        <div class="box">
          <div class="box-header">
            <h3 class="box-title" style="display: none;">Data Table With Full Features</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          <form class="form-inline" action="<?php echo site_url('Trace_actions/rechercher_actions');?>" method="post" role="form">
          <div class="table-toolbar margin-bottom-25" align="center" style="margin-bottom: 25px;">
            <div class="form-body">
            <span class="form-group">Période :</span>
                 <div class="form-group">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="date" class="form-control"  name="date_min" 
                      value="<?php echo $this->session->userdata('date_min'); ?>" placeholder="Début" required="">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="date" class="form-control" value="<?php echo $this->session->userdata('date_max'); ?>" name="date_max" placeholder="Final" required="">
                    </div>
                </div>
               
            <input class="form-control btn-primary" type="submit" name="search"  value="Valider">
          </div>
          </form>
          </div>

        <div class="box-body table-responsive"> 
          <table id="table" class="table table-bordered table-striped" border="1" cellpadding="3">
          <thead>
             <tr>
              <th>Groupe</th>
              <th>Actions</th>
              <th>Descriptions</th>
              <th>Utilisateurs</th>
              <th>Date</th>
              </tr>
            </thead>
            <tbody>
  
            </tbody>
          </table>
        </div><!-- /.box-body -->


        </div>
        <!-- /.box-body -->
      </div>
      <!-- right col -->
    </div>
  <!-- /.row (main row) -->
  </section>
  <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
<?php $this->load->view('tpl/footer'); ?>
<!-- ./wrapper -->
<!-- jQuery 3 -->
<?php $this->load->view('tpl/js_files'); ?>
<script type="text/javascript">

var table;
$(document).ready(function() {
    //datatables
    table = $('#table').DataTable({ 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "columnDefs": [ { orderable: false, targets: [0] } ],
        "dom":  "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
          "buttons": [
          { extend: 'excel', className: 'btn-default', text: 'EXPORT EXCEL'},
          { extend: 'pdf', className: 'btn-default', text: 'EXPORT PDF' },
          ],
        "ordering": false,
        "language" : {
          "sEmptyTable":     "<?php echo $this->lang->line('sEmptyTable'); ?>",
          "sInfo":           "<?php echo $this->lang->line('sInfo'); ?>",
          "sInfoEmpty":      "<?php echo $this->lang->line('sInfoEmpty'); ?>",
          "sInfoFiltered":   "<?php echo $this->lang->line('sInfoFiltered'); ?>",
          "sInfoPostFix":    "",
          "sInfoThousands":  ",",
          "sLengthMenu":     "<?php echo $this->lang->line('sLengthMenu'); ?>",
          "sLoadingRecords": "<?php echo $this->lang->line('sLoadingRecords'); ?>",
          "sProcessing":     "<?php echo $this->lang->line('sProcessing'); ?>",
          "sSearch":         "<?php echo $this->lang->line('sSearch'); ?>",
          "sZeroRecords":    "<?php echo $this->lang->line('sZeroRecords'); ?>",
          "oPaginate": {
            "sFirst":    "<?php echo $this->lang->line('sFirst'); ?>",
            "sLast":     "<?php echo $this->lang->line('sLast'); ?>",
            "sNext":     "<?php echo $this->lang->line('sNext'); ?>",
            "sPrevious": "<?php echo $this->lang->line('sPrevious'); ?>"
          },
          "oAria": {
            "sSortAscending":  "<?php echo $this->lang->line('sSortAscending'); ?>",
            "sSortDescending": "<?php echo $this->lang->line('sSortDescending'); ?>"
          },
          "select": {
                  "rows": {
                    "_": "<?php echo $this->lang->line('_'); ?>",
                    "0": "<?php echo $this->lang->line('0'); ?>",
                    "1": "<?php echo $this->lang->line('1'); ?>"
                  }  
          }
        },
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('Trace_actions/ajax_list')?>",
            "type": "POST",
        },
    });

});

</script>

</body>
</html>
