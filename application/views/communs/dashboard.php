<!DOCTYPE html>
<html lang="fr">
<head>
<?php $this->load->view('tpl/css_files'); ?>
</head>

<body class="skin-2 no-skin">
        
        <!-- Navbar -->
          <?php $this->load->view('tpl/header'); ?>
        <!-- /.navbar -->

        <div class="main-container ace-save-state" id="main-container">
            <script type="text/javascript">
                try{ace.settings.loadState('main-container')}catch(e){}
            </script>

            <?php 
              $data['page'] = "dashboard";
              $this->load->view('tpl/sidebar', $data); 
            ?>

            <div class="main-content">
                <div class="main-content-inner">
                    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="ace-icon fa fa-home home-icon"></i>
                                <a href="#">Accueil - Home</a>
                            </li>
                            <li class="active"><?php echo($this->lang->line('dashboard')); ?></li>
                        </ul><!-- /.breadcrumb -->

                        <div class="nav-search" id="nav-search" style="display: none;">
                            <form class="form-search">
                                <span class="input-icon">
                                    <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                                    <i class="ace-icon fa fa-search nav-search-icon"></i>
                                </span>
                            </form>
                        </div><!-- /.nav-search -->
                    </div>

                    <div class="page-content">

                         <?php $this->load->view('tpl/setting'); ?>

                        <div class="page-header" style="display: none;">
                            <h1>
                                Dashboard
                                <small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    overview &amp; stats
                                </small>
                            </h1>
                        </div><!-- /.page-header -->

                        <div class="row">
                            <div class="col-xs-12">
                                <!-- PAGE CONTENT BEGINS -->
                                <div class="alert alert-block text-center">
                                <form class="form-inline" action="<?php echo site_url('Dashboard/rechercher');?>" method="post" role="form">
                                  <div class="table-toolbar margin-bottom-25" align="center" style="margin-bottom: 25px;">
                                    <div class="form-body">
                                    <span class="form-group"><?php echo $this->lang->line('text_periode'); ?> :</span>
                                         <div class="form-group">
                                            <div class="input-group date">
                                              <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                              </div>
                                              <input type="date" class="form-control"  name="date_min" 
                                              value="<?php echo $this->session->userdata('date_min'); ?>" placeholder="Début" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group date">
                                              <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                              </div>
                                            <input type="date" class="form-control" value="<?php echo $this->session->userdata('date_max'); ?>" name="date_max" placeholder="Final" required="">
                                            </div>
                                        </div>
                                        <?php $role_fk = $this->session->userdata('role_fk'); if($role_fk == 1) : ?>
                                        <div class="form-group">
                                            <div class="input-group date">
                                              <div class="input-group-addon">
                                                <i class="fa fa-home"></i>
                                              </div>

                                                <select class="select2 form-control" name="entreprise">
                                                    <option value=""><?php echo($this->lang->line('text_select')); ?></option>
                                                    <?php foreach ($liste_entreprises as $ent) : ?>
                                                        <option value="<?php echo $ent->id_ent ; ?>" <?php if($ent->id_ent == $this->session->userdata('entreprise')) echo "selected=true"; ?>><?php echo $ent->ent_raison; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php endif; ?>

                                    <input class="form-control btn-warning" type="submit" name="search"  value="<?php echo($this->lang->line('valider_btn')); ?>">
                                  </div>
                                  </form>
                                </div>

                                <div class="hr hr32 hr-dotted"></div>

                                 <div class="row">
                                  <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="infobox infobox-red" style="width: 100% !important;">
                                    <div class="infobox-icon">
                                        <i class="ace-icon fa fa-google-wallet"></i>
                                    </div>

                                    <div class="infobox-data" style="text-align: center;">
                                        <span class="successbox-data-number" style="font-size: 30px;">
                                        <?php 
                                          if ($statsGlobal == NULL){echo '0';}
                                          else{echo number_format((int)$statsGlobal->total_trans, 0, '.', ' ');}
                                        ?> 
                                        </span>
                                        <div class="infobox-content" style="font-size: 16px;"><?php echo $this->lang->line('text_nbre_glob'); ?></div>
                                    </div>
                                    </div>
                                  </div><!-- ./col -->       
                                   <div class="col-lg-3 col-xs-6">
                                      <!-- small box -->
                                      <div class="infobox infobox-red" style="width: 100% !important;">
                                        <div class="infobox-icon">
                                            <i class="ace-icon fa fa-line-chart"></i>
                                        </div>

                                        <div class="infobox-data" style="text-align: center;">
                                            <span class="successbox-data-number" style="font-size: 30px;">
                                              <?php 
                                                if ($statsGlobal == NULL){echo '0';}
                                                else{echo number_format((int)$statsGlobal->montant_trans, 0, '.', ' ');}
                                              ?>
                                            </span>
                                            <div class="infobox-content" style="font-size: 16px;"><?php echo $this->lang->line('text_chiffre_glob'); ?></div>
                                        </div>
                                        <div class="badge badge-warning">
                                            F CFA
                                        </div>
                                    </div>
                                  </div><!-- ./col --> 
                                  <div class="col-lg-3 col-xs-6">
                                      <!-- small box -->
                                    <div class="infobox infobox-green" style="width: 100% !important;">
                                        <div class="infobox-icon">
                                            <i class="ace-icon fa fa-sort-numeric-asc"></i>
                                        </div>

                                        <div class="infobox-data" style="text-align: center;">
                                            <span class="successbox-data-number" style="font-size: 30px;">
                                              <?php 
                                                if ($primeGlobal == NULL){echo '0';}
                                                else{echo number_format((int)$primeGlobal->total_trans, 0, '.', ' ');}
                                              ?>
                                            </span>
                                            <div class="infobox-content" style="font-size: 16px;"><?php echo $this->lang->line('text_nbre_encaisse'); ?></div>
                                        </div>
                                       
                                    </div>
                                  </div><!-- ./col -->       
                                   <div class="col-lg-3 col-xs-6">
                                      <!-- small box -->
                                      <div class="infobox infobox-green" style="width: 100% !important;">
                                        <div class="infobox-icon">
                                            <i class="ace-icon fa fa-money"></i>
                                        </div>

                                        <div class="infobox-data" style="text-align: center;">
                                            <span class="successbox-data-number" style="font-size: 30px;">
                                              <?php 
                                                if ($primeGlobal == NULL){echo '0';}
                                                else{echo number_format((int)$primeGlobal->montant_trans, 0, '.', ' ');}
                                              ?>
                                             </span>
                                            <div class="infobox-content" style="font-size: 16px;"><?php echo $this->lang->line('text_chiffre_encaisse'); ?></div>
                                        </div>
                                        <div class="badge badge-warning">
                                            F CFA
                                        </div>
                                    </div>
                                  </div><!-- ./col -->                          
                                </div>

                                <div class="hr hr32 hr-dotted"></div>

                                <div class="row">
                                <div class="col-md-12">
                                  <div class="box">
                                    <div class="box-header with-border">
                                      <h3 class="box-title"><?php echo $this->lang->line('label_rapport_chiffre_encaisse'); ?></h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-footer" style="color: #f87114; font-size: 16px;">
                                      <div class="row">
                                        <!-- /.col -->
                                        <div class="col-sm-4 col-xs-6">
                                          <div class="description-block border-right">
                                            <span class="description-percentage text-yellow"></span>
                                            <h5 class="description-header"><i class="fa fa-sort-numeric-asc"></i> </h5>
                                            <span class="description-text"><?php echo $this->lang->line('label_nombre_encaisse'); ?> :</span>
                                          </div>
                                          <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-sm-2 col-xs-6">
                                          <div class="description-block border-right">
                                            <span class="description-percentage text-blue"></span>
                                            <h5 class="description-header">
                                              <?php 
                                                if ($primeGlobal == NULL){echo '0';}
                                                else{echo number_format((int)$primeGlobal->total_trans, 0, '.', ' ');}
                                              ?>
                                            </h5>
                                            <span class="description-text text-blue"><?php echo $this->lang->line('text_periode'); ?></span>
                                          </div>
                                          <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-sm-2 col-xs-6">
                                          <div class="description-block border-right">
                                            <span class="description-percentage text-yellow"></span>
                                            <h5 class="description-header">
                                              <?php 
                                                if ($todayGlobal == NULL){echo '0';}
                                                else{echo number_format((int)$todayGlobal->total_trans, 0, '.', ' ');}
                                              ?>
                                            </h5>
                                            <span class="description-text text-yellow"><?php echo $this->lang->line('today'); ?></span>
                                          </div>
                                          <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-sm-2 col-xs-6">
                                          <div class="description-block border-right">
                                            <span class="description-percentage text-green"></span>
                                            <h5 class="description-header">
                                              <?php 
                                                if ($weekGlobal == NULL){echo '0';}
                                                else{echo number_format((int)$weekGlobal->total_trans, 0, '.', ' ');}
                                              ?>
                                            </h5>
                                            <span class="description-text text-green"><?php echo $this->lang->line('thisweek'); ?></span>
                                          </div>
                                          <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-sm-2 col-xs-6">
                                          <div class="description-block">
                                            <span class="description-percentage text-red"></span>
                                            <h5 class="description-header">
                                              <?php 
                                                if ($monthGlobal == NULL){echo '0';}
                                                else{echo number_format((int)$monthGlobal->total_trans, 0, '.', ' ');}
                                              ?>
                                            </h5>
                                            <span class="description-text text-red"><?php echo $this->lang->line('thismonth'); ?></span>
                                          </div>
                                          <!-- /.description-block -->
                                        </div>
                                      </div>
                                      <!-- /.row -->
                                    </div>
                                    <!-- ./box-body -->
                                    <div class="box-footer" style="color: green; font-size: 16px; margin-top: 20px;">
                                      <div class="row">
                                        <div class="col-sm-4 col-xs-6">
                                          <div class="description-block border-right">
                                            <span class="description-percentage text-yellow"></span>
                                            <h5 class="description-header"><i class="fa fa-money"></i> </h5>
                                            <span class="description-text"><?php echo $this->lang->line('label_montant_encaisse'); ?> : </span>
                                          </div>
                                          <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-sm-2 col-xs-6">
                                          <div class="description-block border-right">
                                            <span class="description-percentage text-blue"></span>
                                            <h5 class="description-header">
                                              <?php 
                                                if ($primeGlobal == NULL){echo '0';}
                                                else{echo number_format((int)$primeGlobal->montant_trans, 0, '.', ' ');}
                                              ?>
                                            </h5>
                                            <span class="description-text text-blue"><?php echo $this->lang->line('text_periode'); ?></span>
                                          </div>
                                          <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-sm-2 col-xs-6">
                                          <div class="description-block border-right">
                                            <span class="description-percentage text-yellow"></span>
                                            <h5 class="description-header">
                                              <?php 
                                                if ($todayGlobal == NULL){echo '0';}
                                                else{echo number_format((int)$todayGlobal->montant_trans, 0, '.', ' ');}
                                              ?>
                                            </h5>
                                            <span class="description-text text-yellow"><?php echo $this->lang->line('today'); ?></span>
                                          </div>
                                          <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-sm-2 col-xs-6">
                                          <div class="description-block border-right">
                                            <span class="description-percentage text-green"></span>
                                            <h5 class="description-header">
                                              <?php 
                                                if ($weekGlobal == NULL){echo '0';}
                                                else{echo number_format((int)$weekGlobal->montant_trans, 0, '.', ' ');}
                                              ?>
                                            </h5>
                                            <span class="description-text text-green"><?php echo $this->lang->line('thisweek'); ?></span>
                                          </div>
                                          <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-sm-2 col-xs-6">
                                          <div class="description-block">
                                            <span class="description-percentage text-red"></span>
                                            <h5 class="description-header">
                                              <?php 
                                                if ($monthGlobal == NULL){echo '0';}
                                                else{echo number_format((int)$monthGlobal->montant_trans, 0, '.', ' ');}
                                              ?>
                                            </h5>
                                            <span class="description-text text-red"><?php echo $this->lang->line('thismonth'); ?></span>
                                          </div>
                                          <!-- /.description-block -->
                                        </div>
                                      </div>
                                      <!-- /.row -->
                                    </div>
                                    <!-- /.box-footer -->
                                  </div>
                                  <!-- /.box -->
                                </div>
                                <!-- /.col -->
                              </div>

                                <!-- PAGE CONTENT ENDS -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.page-content -->

            <?php $this->load->view('tpl/footer'); ?>

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

    <!-- basic scripts -->
    <?php $this->load->view('tpl/js_files'); ?>
    </body>
</html>
