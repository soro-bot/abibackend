<!DOCTYPE html>
<html lang="fr">
<head>
<?php $this->load->view('tpl/css_files'); ?>
</head>

<body class="skin-2 no-skin">
        
        <!-- Navbar -->
          <?php $this->load->view('tpl/header'); ?>
        <!-- /.navbar -->

        <div class="main-container ace-save-state" id="main-container">
            <script type="text/javascript">
                try{ace.settings.loadState('main-container')}catch(e){}
            </script>

            <?php 
              $data['page'] = "initier_transaction";
              $this->load->view('tpl/sidebar', $data); 
            ?>

            <div class="main-content">
                <div class="main-content-inner">
                    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="ace-icon fa fa-home home-icon"></i>
                                <a href="#">Accueil - Home</a>
                            </li>
                            <li class="active"><?php echo($this->lang->line('transactions')); ?></li>
                        </ul><!-- /.breadcrumb -->

                        <div class="nav-search" id="nav-search" style="display: none;">
                            <form class="form-search">
                                <span class="input-icon">
                                    <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                                    <i class="ace-icon fa fa-search nav-search-icon"></i>
                                </span>
                            </form>
                        </div><!-- /.nav-search -->
                    </div>

                    <div class="page-content">

                         <?php $this->load->view('tpl/setting'); ?>

							<div class="tab-block" style="margin-left: auto; margin-right: auto; margin-top: 50px !important; width: 500px !important;">
								<div class="buttonload" style="text-align: center; display: none;">
									<i class="fa fa-refresh fa-spin"></i> Chargement ...
								</div>
								<!--  onSubmit="return confirm('Voulez Vous Faire Ce Paiement ?')" -->
								<!-- <?php echo site_url('Accueil/payment') ?> -->
								<!-- <form id="regForm" action="https://secure.sycapay.net/checkresponsive" enctype="multipart/form-data"
									  method="post" role="form" style="display:none;"> -->
								<form id="regForm" action="https://secure.sycapay.net/checkresponsive" enctype="multipart/form-data"
									  method="post" role="form">
									<div class="form-body" style="position: center;">
										<!-- One "tab" for each step in the form: -->


									<div class="tab" style="padding: 10px;">

										<div class="form-row">
											<div class="form-group">
												<label class="col-sm-4 control-label">Mobile :</label>
												<div class="col-sm-8 input-group">
													<div class="input-group-addon">
														<i class="fa fa-phone"></i>
													</div>
													<input type="text" class="form-control" placeholder="Mobile" required id="mobile" name="mobile" maxlength="13" value="225"/>
												</div><!-- /.input group -->
											</div>
										</div>

									</div>


										<!-- <div class="tab"> -->
										<div class="tab">
											<ul class="nav nav-tabs nav-justified">
												<li class="active"><a data-toggle="tab" style="border-radius: 6px 0 0 0;" href="#vie">VIE</a></li>
												<li><a data-toggle="tab" href="#iard" style="border-radius: 0 6px 0 0;">IARD</a></li>
											</ul>

											<div class="tab-content" style="">
												<div id="vie" class="tab-pane fade in active" style="padding-top: 1px;">
													<center><?php echo "Liste des impayés VIE"; ?></center>
													<div class="form-row">
														<div class="scroll">
															<div class="row row-primes hide">
																<div class="col col-md-1 check">
																	<input type="checkbox" />
																</div>
																<div class="col col-md-7 title">
																	<h4>Accident corporel</h4>
																	<span>(Souscription : <b>200 000F</b>)</span>
																	<h5>N° police</h5>
																</div>
																<div class="col col-md-4 montant">
																	<p><span>15 000</span> <sup>FCFA</sup></p>
																	<p>nbr mois <select></select></p>
																</div>
															</div>
														</div>

														<table class="table table-bordered" style="display: none;">
															<thead>
															<tr>
																<th scope="col">#</th>
																<th scope="col"><?php echo $this->lang->line('produit_label'); ?></th>
																<th scope="col"><?php echo $this->lang->line('montant_label'); ?></th>
																<th scope="col"><?php echo "Durée"; ?></th>
															</tr>
															</thead>
															<tbody class="tr-bim bim1">

															</tbody>
														</table>
													</div>
												</div>
												<div id="iard" class="tab-pane fade" style="padding-top: 1px;">
													<center><?php echo "Liste des impayés IARD"; ?></center>
													<div class="form-row">

														<div class="scroll">
															<div class="row row-primes hide">
																<div class="col col-md-1 check">
																	<input type="checkbox" />
																</div>
																<div class="col col-md-7 title">
																	<h4>Accident corporel</h4>
																	<h5>N° police</h5>
																</div>
																<div class="col col-md-4 montant">
																	<p>15 000 <sup>FCFA</sup></p>
																</div>
															</div>
															<div class="row row-primes">
																<div class="col col-md-1 check">
																	<input type="checkbox" />
																</div>
																<div class="col col-md-7 title">
																	<h4>Accident corporel</h4>
																	<h5>N° police</h5>
																</div>
																<div class="col col-md-4 montant">
																	<p>15 000 <sup>FCFA</sup></p>
																</div>
															</div>
															<div class="row row-primes">
																<div class="col col-md-1 check">
																	<input type="checkbox" />
																</div>
																<div class="col col-md-7 title">
																	<h4>Accident corporel</h4>
																	<h5>N° police</h5>
																</div>
																<div class="col col-md-4 montant">
																	<p>15 000 <sup>FCFA</sup></p>
																</div>
															</div>
														</div>

														<table class="table table-bordered hide">
															<thead>
															<tr>
																<th scope="col">#</th>
																<th scope="col"><?php echo $this->lang->line('produit_label'); ?></th>
																<th scope="col"><?php echo $this->lang->line('montant_label'); ?></th>
																<th scope="col"><?php echo "Durée"; ?></th>
															</tr>
															</thead>
															<tbody class="tr-bim bim2">

															</tbody>
														</table>
													</div>
												</div>
											</div>

										</div>

										<div class="tab">
											<h3><center><?php echo "Resumé règlement"; ?></center></h3>
											<div class="form-row">
												<div class="form-group">
													<label class="col-sm-4 control-label">Nom</label>
													<div class="col-sm-8 input-group">
														<h4><b id="nom"></b></h4>
													</div><!-- /.input group -->
												</div>
											</div>
											<div class="form-row">
												<div class="form-group">
													<label
															class="col-sm-6 control-label"><?php echo "Police(s) à régler" ?></label>
													<div class="col-sm-8 input-group">
													</div><!-- /.input group -->
												</div>
											</div>
											<div class="form-row">
												<div class="form-group liste_primes" id="liste_primes_vie">
													<div class="scroll">
														<div class="row row-primes">
															<div class="col col-md-1 check">
															</div>
															<div class="col col-md-7 title">
																<h4>Accident corporel</h4>
																<h5>N° police</h5>
															</div>
															<div class="col col-md-4 montant">
																<p>15 000 <sup>FCFA</sup></p>
															</div>
														</div>
														<div class="row row-primes">
															<div class="col col-md-1 check">
															</div>
															<div class="col col-md-7 title">
																<h4>Accident corporel</h4>
																<h5>N° police</h5>
															</div>
															<div class="col col-md-4 montant">
																<p>15 000 <sup>FCFA</sup></p>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group liste_primes" id="liste_primes_iard">
													<div class="scroll">
														<div class="row row-primes">
															<div class="col col-md-1 check">
															</div>
															<div class="col col-md-7 title">
																<h4>Accident corporel</h4>
																<h5>N° police</h5>
															</div>
															<div class="col col-md-4 montant">
																<p>15 000 <sup>FCFA</sup></p>
															</div>
														</div>
														<div class="row row-primes">
															<div class="col col-md-1 check">
															</div>
															<div class="col col-md-7 title">
																<h4>Accident corporel</h4>
																<h5>N° police</h5>
															</div>
															<div class="col col-md-4 montant">
																<p>15 000 <sup>FCFA</sup></p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-8 col-md-offset-4">
													<div class="form-row" style="border-top: 1px solid black; padding-top: 10px">
														<label class="col-sm-4 control-label" style="text-align: right;"><?php echo "Total"; ?></label>
														<div class="col-sm-8">
															<p style="border-bottom: 0; text-align: right"><span id="total">150 000</span> <sup>FCFA</sup></p>
														</div><!-- /.input group -->
													</div>
												</div><!-- /.input group -->
											</div>
											<div class="row">
												<div class="col-sm-8 col-md-offset-4">
													<div class="form-row">
														<label class="col-sm-4 control-label" style="text-align: right;"><?php echo "Frais"; ?></label>
														<div class="col-sm-8">
															<p style="border-bottom: 0; text-align: right"><span id="frais">150 000</span> <sup>FCFA</sup></p>
														</div><!-- /.input group -->
													</div>
												</div><!-- /.input group -->
											</div>
											<div class="row" style="margin-bottom: 20px">
												<div class="form-group">
													<div class="col-sm-10 col-md-offset-2">
														<div class="form-row" style="border-top: 1px solid black; padding-top: 10px">
															<label class="col-sm-6 control-label" style="text-align: right;"><?php echo "Montant à régler"; ?></label>
															<div class="col-sm-6">
																<p style="border-bottom: 0; padding-top: 10px; text-align: right"><span id="montant_total">150 000</span> <sup>FCFA</sup></p>
															</div><!-- /.input group -->
														</div>
													</div><!-- /.input group -->
												</div>
											</div>

											<!-- /.Information de SYCAPAY -->
											<input type="hidden" name="token" id="token">
											<input type="hidden" name="code_int_concat" id="code_int_concat">
											<input type="hidden" name="num_police_concat" id="num_police_concat">
											<input type="hidden" name="nombre_mois_concat" id="nombre_mois_concat">
											<input type="hidden" name="num_quittance_concat" id="num_quittance_concat">
											<input type="hidden" name="num_avenant_concat" id="num_avenant_concat">
											<input type="hidden" name="mont" id="mont_concat">
											<input type="hidden" name="type_concat" id="type_concat">
											<input type="hidden" name="assure" id="assure">
											<input type="hidden" name="amount" class="total">
											<input type="hidden" name="currency" value="XOF">
											<input type="hidden" name="telephone" value="<?php echo $this->session->userdata('mobile_assure') ?>">
											<input type="hidden" name="name" value="<?php echo $this->session->userdata('nom_assure') ?>">
											<input type="hidden" name="urlnotif" value="<?php echo site_url('Transactions/getRetours') ?>">
											<input type="hidden" name="numcommande" id="commande">
											<input type="hidden" name="merchandid" value="C_6033A4C37BA52">
											<!-- <input type="hidden" name="typpaie" value="payement"> -->
										</div>

										<div id="foot_nologin" class="">
											<div style="overflow:auto; background-color: rgba(255,255,255,0.68); padding-top: 20px;">
												<div style="text-align:center;">
													<button type="button" class="btn btn-default" id="prevBtn" onclick="nextPrev(-1)">Précédent
													</button>
													<button type="button" class="myChargement btn btn-primary" id="nextBtn" onclick="nextPrev(1);">Next</button>
												</div>
											</div>

											<!-- Circles which indicates the steps of the form: -->
											<div style="text-align:center; background-color: rgba(255,255,255,0.68); padding-top: 20px;">
												<span class="step" style="background: #EA9617;"></span>
												<span class="step" style="background: #EA9617;"></span>
												<span class="step" style="background: #EA9617;"></span>
											</div>
										</div>

									</div>
								</form>



								<div id="myOperatorsCheckBox" class="modal fade" role="dialog">
									<div class="modal-dialog">
										<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title text-center"><b style="color: red;">CHOISIR UN MOYEN DE PAIEMENT</b></h4>
											</div>
											<form id="modePaiementFormulaire" action="" method="post" role="form">
												<input type="hidden" name="token" required="" id="tokenCard" />
												<input type="hidden" name="numpayeur" id="numpayeurCard" />
												<input type="hidden" name="amount" required="" id="amountCard" />
												<input type="hidden" name="commande" required="" id="commandeCard" />
												<input type="hidden" name="urls" value="<?php echo site_url('Transactions/getRetoursPaiements') ?>" />
												<input type="hidden" name="urlc" value="<?php echo site_url('Transactions/getRetoursPaiements') ?>" />
												<input type="hidden" name="telephone" value="0151405603" />
												<input type="hidden" name="name" value="SYCA" />
												<input type="hidden" name="pname" value="ABI" />
												<input type="hidden" name="emailpayeur" value="sales@sycapay.com" />
												<input type="hidden" name="merchandid" value="C_6033A4C37BA52" required="" />
												<input type="hidden" name="currency" value="CFA"/>
												<input type="hidden" name="typpaie" value="payement"/>

												<div class="modal-body">
													<div class="form-group">
														<label for="text" class="col-sm-1 control-label"><input type="radio" name="optradioVilain" value="MO" checked></label>
														<div class="col-sm-11">
															<img height="25" width="25" src="<?php echo img_url()."MO.png" ?>"/> MOBILE MONEY (ORANGE, MTNT, MOOV)
														</div>
													</div>
													<div class="form-group" style="margin-top: 50px;">
														<label for="text" class="col-sm-1 control-label"><input type="radio" name="optradioVilain" value="VI"></label>
														<div class="col-sm-11">
															<img height="25" width="25" src="<?php echo img_url()."VI.png" ?>"/> VISA (CARTE VISA)
														</div>
													</div>
													<div class="form-group" style="margin-top: 100px;">
														<label for="text" class="col-sm-1 control-label"><input type="radio" name="optradioVilain" value="MC"></label>
														<div class="col-sm-11">
															<img height="25" width="25" src="<?php echo img_url()."MC.png" ?>"/> MASTER CARTE
														</div>
													</div>
												</div>
												<div class="modal-footer" style="text-align: center; margin-top: 25px;">
													<a class="btn btn-default" data-dismiss="modal" class="form-control" >Annuler</a>
													<button id="confirmerModePaiement" type="button" class="btn btn-primary">Valider</button>
												</div>
											</form>
										</div>
									</div>
								</div>

							</div>
							<!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->

                        </div><!-- /.row -->
                    </div><!-- /.page-content -->
                </div>

            <?php $this->load->view('tpl/footer'); ?>
            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

<!-- basic scripts -->
<?php $this->load->view('tpl/js_files'); ?>
<script type="text/javascript">
	var currentTab;


	jQuery(document).ready(function() {
  <?php
      if ($this->session->flashdata("success")){
          echo "toast_success('".$this->session->flashdata("success")."');";
      }

      if ($this->session->flashdata("error")){
          echo "toast_error('".$this->session->flashdata("error")."');";
      }
  ?>

		$(document).on('click', "#confirmerModePaiement", function() {
			// $('#myOperatorsCheckBox').hide();
			$('#myOperatorsCheckBox').modal('hide');
			var modePaiementChoose = $("input[name='optradioVilain']:checked").val();

			var commande = $("#commande").val();
			var ent_id = $("#ent_id").val();
			var pays_id = $("#pays_id").val();
			var assure = $("#assure").val();
			var token = $("#token").val();
			var type_concat = $("#type_concat").val();
			var code_int_concat = $("#code_int_concat").val();
			var num_police_concat = $("#num_police_concat").val();
			var nombre_mois_concat = $("#nombre_mois_concat").val();
			var num_avenant_concat = $("#num_avenant_concat").val();
			var num_quittance_concat = $("#num_quittance_concat").val();
			var total = $("#montant_total").text();
			var mont_concat = $("#mont_concat").text();
			var telephone = $($('input[name=optradio]:checked', '#regForm')[0]).attr("data-telephone");
			var id_assure = $($('input[name=optradio]:checked', '#regForm')[0]).attr("data-id-assure");

			$("#modePaiementFormulaire").find("#telephone").val(telephone);
			$("#modePaiementFormulaire").find("#name").val(assure);
			$("#modePaiementFormulaire").find("#pname").val(assure);

			if(modePaiementChoose == 'MO')
			{
				var firstTime = true;
				var confirm_box = $.confirm({
					theme: 'material',
					boxWidth: '1500px',
					title: 'Numéro de Téléphone!',
					content: '' +
							'<form action="" class="formName" autocomplete="off">' +
							'<div class="form-group">' +
							'<label>Montant à payer</label>' +
							'<input type="text" id="total_a_payer" placeholder="Montant" value="'+total+'" class="form-control" /></div>' +
							'</div>' +
							'<div class="form-group">' +
							'<label>Saisir Numéro de Paiement</label>' +
							'<input type="text" placeholder="Téléphone" class="name form-control" required id="target" autocomplete="off" maxlength="10" value="<?php echo $this->session->userdata("mobile_assure"); ?>"/>' +
							'</div>' +
							'<div class="form-group" id="getOTP" style="display:none;">' +
							'<label><b>Prière générer un code temporaire au #144*8*2#.</b></label>' +
							'<input type="text" placeholder="Saisir OTP ICI" class="OTP form-control" autocomplete="off" maxlength="4"/>' +
							'</div>' +
							'</form>',
					buttons: {

						cancel:
								{
									text: 'Retour',
									btnClass: 'btn-default',
									function () {
										$.alert('Prière Reprendre SVP !!');
									},
								},
						formSubmit: {
							text: 'Valider',
							btnClass: 'btn-blue',
							action: function () {
								//console.log(this.text);
								var otpShowed = false;
								var OTP = this.$content.find('.OTP').val();
								var mobile = this.$content.find('#target').val();
								if(!mobile)
								{
									$.alert('Veuillez renseigner le numéro de téléphone !');
									return false;
								}
								else if (mobile.length != 8 && mobile.length != 10)
								{
									$.alert('Le numéro de téléphone doit être 08 ou 10 chiffres !');
									return false;
								}
								else
								{
									if (firstTime) {
										//Verifier le réseau et afficher l'OTP si orange la première fois
										$($(".jconfirm-buttons")[0]).prepend("<div id=\"buttonload\" style=\"text-align: center; display: none;\">\n" +
												"            <i class=\"fa fa-refresh fa-spin\"></i> Chargement ...\n" +
												"        </div>");
										$("#buttonload").show();
										$($(".jconfirm-buttons")[0]).find("button.btn-blue").hide();
										$.post("<?php echo site_url("Transactions/getCheckOperateurs") ?>", {mobile: mobile})
												.done(function (data) {
													console.log(data);
													if (data != "") {
														firstTime = false;
														$("#buttonload").hide();
														if (data === "OK") {
															otpShowed = true;
															$($(".jconfirm-buttons")[0]).find("#buttonload").hide();
															$($(".jconfirm-buttons")[0]).find("button.btn-blue").show();

															$('#getOTP').show();
															$('.retourOperateurs').show();
															return false;
														} else if (data === "NO") {
															$('#getOTP').hide();
															$('.retourOperateurs').show();
															otpShowed = false;
															confirm_box.close();
															processToPayment(num_avenant_concat, num_quittance_concat, nombre_mois_concat, null, mobile, token, code_int_concat, num_police_concat, commande, ent_id, pays_id, mont_concat, assure, type_concat, provider, telephone, id_assure);
														}

													} else {
														$.alert("Impossible de détecter l'opérateur !");
														$($(".jconfirm-buttons")[0]).find("#buttonload").hide();
														$($(".jconfirm-buttons")[0]).find("button.btn-blue").show();
														return false;
													}
												})
												.fail(function (error) {
													console.log(error);
												});
										return false;
									} else {
										// Proceder au paiement si ce n'est pas la première fois dans le cas d'orange
										if (OTP.length == 0) {
											$.alert('Le code d\'autorisation doit être saisi !');
											return false;
										} else {
											processToPayment(num_avenant_concat, num_quittance_concat, nombre_mois_concat, OTP, mobile, token, code_int_concat, num_police_concat, commande, ent_id, pays_id, mont_concat, assure, type_concat, provider, telephone, id_assure);
										}
									}
								}
							}
						}
					},
					onContentReady: function () {
						// bind to events
						var jc = this;
						this.$content.find('form').on('submit', function (e) {
							// if the user submits the form by pressing enter in the field.
							e.preventDefault();
							jc.$$formSubmit.trigger('click'); // reference the button and click it
						});
					}
				});

			}
			else
			{
				var alerte = $.confirm({
					boxWidth: '800px',
					boxHeight: '800px',
					title: "Paiement en cours",
					content: 'Veuillez patienter votre paiement est en cours...',
					buttons: {
						logoutUser: false,
						cancel: false,
						confirm: false
					}
				});
				//$("#foot_nologin").addClass("hide");
				var provider = "";
				if(modePaiementChoose == 'VI')
				{
					provider = "VISA";
				}
				else
				{
					provider = "MASTERCARD";
				}
				$.ajax({
					url: "<?php echo site_url('Transactions/paiement_cb')?>",
					type: "POST",
					data: {num_avenant_concat: num_avenant_concat, num_quittance_concat: num_quittance_concat, nombre_mois_concat: nombre_mois_concat, token: token, code_int_concat: code_int_concat, num_police_concat: num_police_concat, commande: commande, ent_id: ent_id, pays_id: pays_id,montant: mont_concat, assure: assure, type_concat: type_concat, provider: provider, telephone: telephone, id_assure: id_assure},
					success: function (datas) {
						try {
							alerte.close();
						} catch (e) {
							console.log(e);
						}

						if (datas !== "")
						{

							if(modePaiementChoose == 'VI')
							{
								$("#modePaiementFormulaire").attr('action', 'https://secure.sycapay.com/checkvisadir');
							}
							else
							{
								$("#modePaiementFormulaire").attr('action', 'https://secure.sycapay.com/checkmasterdir');
							}

							$( "#modePaiementFormulaire" ).submit();

						}
						else
						{
							$.alert('Paiement impossible - Reprendre SVP !');
							currentTab = 2;
							showTab(currentTab);
						}
					}
				});
			}
		});

		$('.carousel').carousel({interval: 20000});

		(function($) {
			$.fn.inputFilter = function(inputFilter) {
				return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
					if (inputFilter(this.value)) {
						this.oldValue = this.value;
						this.oldSelectionStart = this.selectionStart;
						this.oldSelectionEnd = this.selectionEnd;
					} else if (this.hasOwnProperty("oldValue")) {
						this.value = this.oldValue;
						this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
					} else {
						this.value = "";
					}
				});
			};
		}(jQuery));

		$(document).on('keydown', function (e) {
			if (e.keyCode == 13 && $('#mobile').is(":focus") && $('#mobile').val().length < 4) {
				e.preventDefault();
			}
		});

		$(document).on('change', "#pays_id", function (e) {
			$("#mobile").val($(this).find("option:selected").attr("data-indicatif"));
		});

//$('.myRadio').click(function() {
		var pinId ;
		$("#mobile").inputFilter(function(value) {
			return /^\d*$/.test(value);    // Allow digits only, using a RegExp
		});

		$(document).on("change", ".optradio", function () {

			var prime = $(this).attr("data-label");
			var montant = $(this).val();
			var code = $(this).attr("data-id");

			var all = $(".optradio:checked");
			console.log(all);


			var type = $(this).attr("data-type");

			var change = false;

			$("#liste_primes_vie").find("div.scroll").empty();
			$("#liste_primes_iard").find("div.scroll").empty();
			$("#liste_primes_iard").fadeIn();
			$("#liste_primes_vie").fadeIn();

			$.each(all, function (index, value) {
				var type_new = $(this).attr("data-type");

				if (type != type_new){
					remove_all(type);
				}
				console.log(value);

			});

			all = $(".optradio:checked");

			$.each(all, function (index, value) {
				var type_new = $(this).attr("data-type");
				var prime = $(this).attr("data-label");
				var montant = $(this).val();

				var code = $(this).attr("data-id");
				var assure = $(this).attr("data-assure");
				var codeInterv = $(this).attr("data-code-inter");

				if (type_new == "VIE"){

					var nb = $($(this).closest("div.row").find("input[type='number']")[0]).val();
					if (nb == '0')
					{
						var nb = 1;
					}

					$($("#liste_primes_vie").find("div.scroll")[0]).append("" +
							'<div class="row row-primes" style="margin-left: 20px; height: auto important!; width: auto important!;">' +
							'<div class="col col-md-5 title">' +
							'   <h4>' + prime + '</h4>' +
							'   <h5>' + code + ' - ' + codeInterv + '</h5>' +
							'</div>' +
							'<div class="col col-md-3 title">' +
							'   <h5> '+ nb +' x ' + Math.floor(parseInt(montant) / parseInt(nb)) + '</h5>' +
							'</div>' +
							'<div class="col col-md-4 montant">' +
							'   <p>' + numStr(montant) + ' <sup>FCFA</sup></p>' +
							'</div>' +
							'</div><hr>' +
							""
					);

					$("#liste_primes_iard").hide();
				}

				if (type_new == "IARD"){

					var nb = 1;

					$($("#liste_primes_iard").find("div.scroll")[0]).append("" +
							'<div class="row row-primes" style="margin-left: 20px; height: auto important!; width: auto important!;">' +
							'<div class="col col-md-5 title">' +
							'   <h4>' + prime + '</h4>' +
							'   <h5>' + code + ' - ' + codeInterv + '</h5>' +
							'</div>' +
							'<div class="col col-md-3 title">' +
							'   <h5> '+ nb +' x ' + Math.floor(parseInt(montant) / parseInt(nb)) + '</h5>' +
							'</div>' +
							'<div class="col col-md-4 montant">' +
							'   <p>' + numStr(montant) + ' <sup>FCFA</sup></p>' +
							'</div>' +
							'</div><hr>' +
							""
					);

					$("#liste_primes_vie").hide();
				}

				$("#nom").text(assure);
			});

			var pays_id = $("#pays_id").val();

		});

		function remove_all (type){
			if (type == "VIE"){
				$("#iard").find('.optradio:checked').each(function(i){
					$(this).prop('checked', false);
				});
			}
			if (type == "IARD"){
				$("#vie").find('.optradio:checked').each(function(i){
					$(this).prop('checked', false);
				});
			}
		}


		$(document).on("change", ".slider", function () {

			var nb_mois = parseInt($(this).val());
			var montant = parseInt($(this).attr("data-montant"));


			var mt_total = nb_mois * montant;


			$($(this).parent().parent().find(".mt_tot")[0]).text(numStr(mt_total));

			$($(this).closest("div.row").find("input[type=checkbox]")[0]).val(mt_total);

		});

	// Current tab is set to be the first tab (0)
	currentTab = 0;
	// Display the current tab
	showTab(currentTab);

});


	function processToPayment(num_avenant_concat, num_quittance_concat, nombre_mois_concat, OTP, mobile, token, code_int_concat, num_police_concat, commande, ent_id, pays_id, mont_concat, assure, type_concat, provider, telephone, id_assure){
		var alerte = $.confirm({
			boxWidth: '800px',
			boxHeight: '800px',
			title: "Paiement en cours",
			content: 'Veuillez patienter votre paiement est en cours...',
			buttons: {
				logoutUser: false,
				cancel: false,
				confirm: false
			}
		});
		//$("#foot_nologin").addClass("hide");
		var provider = "Mobile Money";
		$.ajax({
			url: "<?php echo site_url('Transactions/paiement')?>",
			type: "POST",
			data: {num_avenant_concat: num_avenant_concat, num_quittance_concat: num_quittance_concat, nombre_mois_concat: nombre_mois_concat, OTP: OTP, mobile: mobile, token: token, code_int_concat: code_int_concat, num_police_concat: num_police_concat, commande: commande, ent_id: ent_id, pays_id: pays_id,montant: mont_concat, assure: assure, type_concat: type_concat, provider: provider, telephone: telephone, id_assure: id_assure},
			success: function (datas) {
				try {
					alerte.close();
				} catch (e) {
					console.log(e);
				}
				console.log(datas);
				if (datas !== "")
				{
					datas = JSON.parse(datas);
					if (datas.code == 0) {
						//$("#foot_nologin").removeClass("hide");
						var code = datas.transactionId;
						//console.log(datas);
						//$.alert(datas.message);
						$.confirm({
							boxWidth: '800px',
							boxHeight: '800px',
							title: datas.message,
							content: 'Votre paiement sera annulé dans 2 minutes ou cliquez sur attendre.',
							autoClose: 'logoutUser|120000',
							buttons: {
								logoutUser: {
									text: 'Annuler le paiement !',
									btnClass: 'btn-danger',
									action: function () {

										document.location.href = '<?php echo site_url('Transactions/getAnnulations') . '/'?>' + commande;
									}
								},
								cancel:
										{
											text: 'Attendre le paiement !',
											btnClass: 'btn-success',
											function() {
												$.alert('Prière confirmer le paiement sur votre téléphone SVP !');
											},
										}
							}
						});
						var setIntervalId = setInterval(function () {
							$.ajax({
								url: "<?php echo site_url('Transactions/getStatus')?>",
								type: "POST",
								data: {code: code},
								success: function (rep) {
									var code = datas.code;
									if (rep === "PE") {
										//$.alert('Veuillez patientez SVP !');
										//return false;
										var commande = $("#commande").val();
										console.log(commande);
										//$.confirm({
										//   boxWidth: '800px',
										//   boxHeight: '800px',
										//   title: datas.message,
										//    content: 'Votre paiement sera annulé dans 10 secondes ou cliquez sur attendre.',
										//    autoClose: 'logoutUser|10000',
										//    buttons: {
										//        logoutUser: {
										//            text: 'Annuler le paiement !',
										//            btnClass: 'btn-danger',
										//            action: function () {
										//
										//                document.location.href = '<?php //echo site_url('Accueil/getAnnulations').'/'?>//'+commande;
										//            }
										//        },
										//        cancel:
										//        {
										//            text: 'Attendre le paiement !',
										//            btnClass: 'btn-success',
										//            function () {
										//                $.alert('Prière confirmer le paiement sur votre téléphone SVP !');
										//            },
										//        }
										//    }
										//});
									} else {
										var alerte = $.confirm({
											boxWidth: '800px',
											boxHeight: '800px',
											title: "Succès",
											content: 'Félicitations, votre paiement s\'est bien déroulé',
											buttons: {
												confirm:
												   {
													   text: 'Compris',
													   btnClass: 'btn-success',
													   function () {
														   document.location.reload();
													   },
												   }
											}
										});
									}
								}
							});
						}, 10000);

					} else {
						$.alert(datas.message);
						currentTab = 2;
						showTab(currentTab);
					}
				}
				else
				{
					$.alert('Paiement impossible - Reprendre SVP !');
					currentTab = 2;
					showTab(currentTab);
				}
			}
		});
	}


	function nextPrev(n) {
	// This function will figure out which tab to display
	var x = document.getElementsByClassName("tab");
	// Exit the function if any field in the current tab is invalid:
	//if (n == 1 && !validateForm()) return false;
	// Hide the current tab:
	//x[currentTab].style.display = "none";

	// Increase or decrease the current tab by 1:
	currentTab = currentTab + n;
	//$(".tab").hide();

	// if you have reached the end of the form...

	//Afficher/masquer le bas prev/next

	if (currentTab >= x.length) {
		// ... the form gets submitted:
		//document.getElementById("regForm").submit();
		//return false;
		$.confirm({
			theme: 'material',
			title: 'CONFIRMATION !',
			content: 'Voulez vous faire cette action ?',
			buttons: {
				confirm: {
					text: '<?php echo $this->lang->line('confirm_modal_valider_btn'); ?>',
					btnClass: 'btn-blue',
					action: function () {

						//$('#myOperatorsCheckBox').show();

						var commande = $("#commande").val();
						var ent_id = $("#ent_id").val();
						var pays_id = $("#pays_id").val();
						var assure = $("#assure").val();
						var token = $("#token").val();
						var type_concat = $("#type_concat").val();
						var code_int_concat = $("#code_int_concat").val();
						var num_police_concat = $("#num_police_concat").val();
						var nombre_mois_concat = $("#nombre_mois_concat").val();
						var num_avenant_concat = $("#num_avenant_concat").val();
						var num_quittance_concat = $("#num_quittance_concat").val();
						var total = $("#montant_total").text();
						var mont_concat = $("#mont_concat").text();
						//var montant = parseInt(total);

						console.log(total);


						$("#tokenCard").val(token);
						$("#telephoneCard").val('+22555835666');
						$("#nameCard").val(assure);
						$("#pnameCard").val(assure);
						$("#emailpayeurCard").val('sales@sycapay.com');
						$("#numpayeurCard").val(commande);
						$("#amountCard").val(total);
						$("#commandeCard").val(commande);


						$('#myOperatorsCheckBox').modal('show');

					}
				},
				cancel: {
					text: '<?php echo $this->lang->line('confirm_modal_annuler_btn'); ?>',
					btnClass: 'btn-default',
					action: function () {
						//document.location.reload(true);
						//return false;
					}
				}
			}
		});

	} else if (currentTab == 1 && n !== -1) {
		var mobile = $('#mobile');
		$("#mob").text(mobile.val());
		if (mobile.val() == ""){
			currentTab=0;
			showTab(currentTab);
			mobile.addClass("invalid");
			alert("Le mobile n'a pas été renseigné !")
		} else if (mobile.val().length < 11 || mobile.val().length > 13){
			currentTab=0;
			showTab(currentTab);
			mobile.addClass("invalid");
			alert("Le mobile doit être de 10 chiffres !")
		} else {
			$("#nextBtn").prepend("<i class=\"fa fa-refresh fa-spin\"></i>");
			$("#nextBtn").attr("disabled", "true");
			$.post("<?php echo site_url("Transactions/get_primes") ?>", {mobile: mobile.val()})
					.done(function (data) {
						$("#nextBtn").removeAttr("disabled");
						data = JSON.parse(data);
						if (data != null && data.status == true) {
							$('#foot_nologin').show();

							$("#mob").text("+" + data.assure.mobile_assure);

							$("div#vie").find("div.form-row").find("div.scroll").empty();
							$("div#iard").find("div.form-row").find("div.scroll").empty();
							$(".tr-bim.bim1").empty();
							$(".tr-bim.bim2").empty();
							//console.log(data.vie[0]);
							$.each(data.vie, function (index, value) {
								$(".tr-bim.bim1").append('<tr><td>' + value.NUMEPOLI + '</td><td>' + value.LIBECATE + ' (VIE)</td><td>' + value.MONTGARA + '</td><td><input class="slider" type="range" min="1" max="10" value="1" data-montant="' + value.MONTGARA + '">(<span class="nb_mois">10 mois</span>)</td><td><input type="checkbox" data-id="' + value.NUMEPOLI + '" data-assure="' + value.NOM_ASSU + '" data-label="' + value.LIBECATE + '" data-type="VIE" value="' + value.MONTGARA + '" name="optradio" class="optradio"><a class="btn default btn-xs default" href="#" data-target="#myReduction" data-toggle="modal" title="Effectuer un paiement" data-id="' + value.NUMEPOLI + '""></a></td></tr>');
								$("div#vie").find("div.form-row").find("div.scroll").append('' +
										'<div class="row row-primes" style="height:auto !important; width: auto !important;">' +
										'<div class="col col-md-1 check">' +
										'<input type="checkbox" data-id="' + value.NUMEPOLI + '" data-assure="' + value.NOM_ASSU + '" data-label="' + value.LIBECATE + '" data-type="VIE" value="' + value.montant_tot + '" data-code-int="' + value.CODEINTE + '" data-num-police="' + value.NUMEPOLI + '" name="optradio" class="optradio" data-nombre-mois="' + value.NB_COTISATION + '" data-code-inter="' + value.CODEINTE + '">' +
										'</div>' +
										'<div class="col col-md-7 title">' +
										'<h4>' + value.LIBECATE + '</h4>' +
										'<span>(Souscription : <b>' + numStr(value.MONTGARA) + '</b> FCFA)</span>' +
										'<h5>' + value.NUMEPOLI + ' - ' + value.CODEINTE + '</h5>' +
										'</div>' +
										'<div class="col col-md-4 montant">' +
										'<p><span class="mt_tot">' + value.montant_tot + '</span> <sup>FCFA</sup></p>' +
										'<p>Nbre de mois à payer <input type="number" value="' + value.NB_COTISATION + '" min="1" class="slider getNombreMois" data-montant="' + value.MONTGARA + '" data-num-quittance="" data-num-avenant=""/></p>' +
										'</div>' +
										'</div>' +
										''
								);

								$("#nom").text(data.NOM_ASSU);


							});

							$.each(data.iard, function (index, value) {
								$(".tr-bim.bim2").append('<tr><td>' + value.NUMEPOLI + '</td><td>' + value.LIBTYPAV + ' (IARD)</td><td>' + value.PRIMTOTA + '</td><td><input class="slider" type="range" min="1" max="10" value="1" data-montant="' + value.PRIMTOTA + '">(<span class="nb_mois">10 mois</span>)</td><td><input type="checkbox" data-id="' + value.NUMEPOLI + '" data-assure="' + value.NOM_ASSU + '" data-label="' + value.LIBTYPAV + '" data-type="IARD" value="' + value.PRIMTOTA + '" name="optradio" class="optradio"><a class="btn default btn-xs default" href="#" data-target="#myReduction" data-toggle="modal" title="Effectuer un paiement" data-id="' + value.NUMEPOLI + '""></a></td></tr>');
								$("div#iard").find("div.form-row").find("div.scroll").append('' +
										'<div class="row row-primes" style="height:auto !important; width: auto !important;">' +
										'<div class="col col-md-1 check">' +
										'<input type="checkbox" data-id="' + value.NUMEPOLI + '" data-assure="' + value.NOM_ASSU + '" data-label="' + value.LIBTYPAV + '" data-type="IARD" value="' + value.PRIMTOTA + '" data-code-int="' + value.CODEINTE + '" data-num-police="' + value.NUMEPOLI + '" name="optradio" class="optradio" data-nombre-mois="' + value.RELIQUAT + '" data-code-inter="' + value.CODEINTE + '" data-num-quittance="' + value.NUMEQUIT + '" data-num-avenant="' + value.NUMEAVEN + '">' +
										'</div>' +
										'<div class="col col-md-7 title">' +
										'<h4>' + value.LIBTYPAV + '</h4>' +
										'<h5 style="text-decoration:none !important;color:black !important;"><b>Auto - Toyota Yaris - 307HL01</h5>' +
										'<h5>' + value.NUMEPOLI + ' - ' + value.CODEINTE + '</h5>' +
										'</div>' +
										'<div class="col col-md-4 montant">' +
										'<h4><span class="mt_tot">' + numStr(value.PRIMTOTA) + '</span> <sup>FCFA</sup></h4>' +
										// '<p><span class="mt_tot">' + value.PRIMTOTA + '</span> <sup>FCFA</sup></p>' +
										'<h5>' + value.DATEEFFE + ' - ' + value.DATEECHE + '</h5>' +
										'</div>' +
										'</div>' +
										''
								);

								$("#nom").text(data.NOM_ASSU);

							});

							$(".slider").change();
							showTab(currentTab);

						} else {
							//currentTab = 0;
							//nextPrev(-1);
							currentTab = 0;
							showTab(currentTab);
							$.alert("Aucune prime retrouvée");
						}
					})
					.fail(function (error) {
						currentTab = 0;
						//nextPrev(-1);
						$('.buttonload').hide();

						showTab(currentTab);
						$.alert("Aucune prime retrouvée !");
					});
		}
	} else if (currentTab == 2 && n !== -1){
		var all = $(".optradio:checked");
		if (all.length == 0){
			currentTab=1;
			//$("#otp").addClass("invalid");
			$.alert("Veuillez selectionner au moins une prime");
			return false;
		} else {
			// Do something interesting here
			var code = $('input[name=optradio]:checked', '#regForm').attr("data-id");
			var label = $('input[name=optradio]:checked', '#regForm').attr("data-label");
			var type = $('input[name=optradio]:checked', '#regForm').attr("data-type");
			var assure = $('input[name=optradio]:checked', '#regForm').attr("data-assure");
			var code_int = $('input[name=optradio]:checked', '#regForm').attr("data-code-int");
			var num_police = $('input[name=optradio]:checked', '#regForm').attr("data-num-police");
			var prime_montant = 0;
			var nb = 0;
			var mont_concat = "";
			var type_concat = "";
			var code_int_concat = "";
			var num_police_concat = "";
			var num_police_concat = "";
			var nombre_mois_concat = "";
			var num_quittance_concat = "";
			var num_avenant_concat = "";

			$('input[name=optradio]:checked').each(function (i) {
				nb++;
				prime_montant += parseInt($(this).val());
				mont_concat += $(this).val() + "|";
				code_int_concat += $(this).attr("data-code-int") + "|";
				num_police_concat += $(this).attr("data-num-police") + "|";
				type_concat += $(this).attr("data-type") + "|";
				//nombre_mois_concat += $(this).attr("data-nombre-mois") + "|";
				nombre_mois_concat += $($(this).closest("div.row").find("input[type='number']")[0]).val() + "|";
				num_quittance_concat += $(this).attr("data-num-quittance") + "|";
				num_avenant_concat += $(this).attr("data-num-avenant") + "|";
			});

			mont_concat = mont_concat.slice(0, -1);
			type_concat = type_concat.slice(0, -1);
			code_int_concat = code_int_concat.slice(0, -1);
			num_police_concat = num_police_concat.slice(0, -1);
			nombre_mois_concat = nombre_mois_concat.slice(0, -1);
			num_avenant_concat = num_avenant_concat.slice(0, -1);
			num_quittance_concat = num_quittance_concat.slice(0, -1);

			//var frais = parseInt(100);
			var frais = parseInt(0);
			var total = parseInt(prime_montant) + parseInt(frais);
			var montant_total = parseInt(total);

			$("#total").text(prime_montant);
			$("#frais").text(frais);
			$("#montant_total").text(numStr(total));

			$.post("<?php echo site_url("Transactions/getTokens2") ?>", {
				chiffre: montant_total,
				types: type_concat,
				nb_primes: nb
			})
					.done(function (data) {
						$('.buttonload').hide();

						data = JSON.parse(data);
						console.log(data);
						if (data) {
							//var json = JSON.parse(data);
							$(".id").val(code);
							$("#token").val(data.token);
							$("#commande").val(data.commande);
							$("#frais").val(frais);
							$("#label").val(label);
							$("#mont_concat").text(mont_concat);
							$("#assure").val(assure);
							$("#type_concat").val(type_concat);
							$("#code_int_concat").val(code_int_concat);
							$("#num_police_concat").val(num_police_concat);
							$("#nombre_mois_concat").val(nombre_mois_concat);
							$("#num_quittance_concat").val(num_quittance_concat);
							$("#num_avenant_concat").val(num_avenant_concat);
							$(".total").val(total);
							$("#montant").val(prime_montant);

							showTab(currentTab);

						}
						else
						{
							$.alert('Token impossible');
							location.href = "<?php echo site_url('Accueil/')?>";
						}

					})
					.fail(function (error) {
						$.alert('Token impossible');
						$('.buttonload').hide();
					});
		}
	} else {
		showTab(currentTab);
	}


	// Otherwise, display the correct tab:
	//showTab(currentTab);
}

function showTab(n) {
	// This function will display the specified tab of the form...
	$(".tab").hide();
	var x = document.getElementsByClassName("tab");
	x[n].style.display = "block";

	if (n == (x.length - 1)) {


		//document.getElementById("nextBtn").innerHTML = "Confirmer";
		document.getElementById("nextBtn").innerHTML = "<?php echo $this->lang->line('payer_btn'); ?>";
	} else {
		//document.getElementById("nextBtn").innerHTML = "Suivant";
		document.getElementById("nextBtn").innerHTML = "<?php echo $this->lang->line('next_btn'); ?>";
	}

	if (n == 0){
		$(".tab").closest(".tab-block").css("margin-top", "50px");
		document.getElementById("nextBtn").innerHTML = "Demander prime";
	} else if (n == 1) {
		//$(".tab").closest(".tab-block").css("margin-top", "-30px")
		$(".tab").closest(".tab-block").css("margin-top", "50px");
		document.getElementById("nextBtn").innerHTML = "Suivant";
	} else if (n == 2) {
		//$(".tab").closest(".tab-block").css("margin-top", "-30px")
		$(".tab").closest(".tab-block").css("margin-top", "50px");
		document.getElementById("nextBtn").innerHTML = "Payer";
	} else {
		//$(".tab").closest(".tab-block").css("margin-top", "0px")
		$(".tab").closest(".tab-block").css("margin-top", "50px");
		//$(".tab").closest(".tab-block").css("margin-top", "80px")
	}
	//... and fix the Previous/Next buttons:
	if (n == 0) {
		document.getElementById("prevBtn").style.display = "none";
	} else {

		document.getElementById("prevBtn").style.display = "inline";
	}
	//... and run a function that will display the correct step indicator:
	fixStepIndicator(n)
}

function validateForm() {
	// This function deals with validation of the form fields
	var x, y, i, valid = true;
	x = document.getElementsByClassName("tab");
	y = x[currentTab].getElementsByTagName("input");
	// A loop that checks every input field in the current tab:
	for (i = 0; i < y.length; i++) {
		// If a field is empty...
		if (y[i].value == "") {
			// add an "invalid" class to the field:
			y[i].className += " invalid";
			// and set the current valid status to false
			valid = false;
		}
	}
	// If the valid status is true, mark the step as finished and valid:

	return valid; // return the valid status
}

function fixStepIndicator(n) {
	// This function removes the "active" class of all steps...
	var i, x = document.getElementsByClassName("step");
	for (i = 0; i < x.length; i++) {
		x[i].className = x[i].className.replace(" active", "");
	}
	//... and adds the "active" class on the current step:
	x[n].className += " active";
}


function numStr(a, b) {
	a = '' + a;
	b = b || ' ';
	var c = '',
			d = 0;
	while (a.match(/^0[0-9]/)) {
		a = a.substr(1);
	}
	for (var i = a.length-1; i >= 0; i--) {
		c = (d != 0 && d % 3 == 0) ? a[i] + b + c : a[i] + c;
		d++;
	}
	return c;
}

var table;

$("#demande").click(function () {

        
        var mobile = $("#mobile").val();
        var type = $("input[name='typedeprimes']:checked").val();
        
        $.post("<?php echo site_url('Transactions/get_primes')?>",{type:type, mobile:mobile})
            .done(function(datas) {

                datas = JSON.parse(datas);
                if(jQuery.isEmptyObject(datas.vie) !== true || jQuery.isEmptyObject(datas.iard) !== true)
                {   
                    $("#demande").hide();
                    $("#comfirms_1").fadeIn();
                    $("#choix").fadeIn();
                    if (type == 'V') 
                    {
                        $.each(datas.vie, function (index, value)
                        {
                            $( ".tr-bim" ).append('<tr><td>' + value.NUMEPOLI + ' - ' + value.CODEINTE + '</td><td>' + value.MONTGARA + '</td><td>' + value.LIBECATE + ' (VIE)</td><td><input type="radio" data-id="'+ value.NUMEPOLI +'" data-label="'+ value.LIBECATE +'"  data-id-assure="' + datas.id+ '" data-code-inter="' + value.CODEINTE+ '" data-telephone="' + value.TELEASSU+ '" data-assure="' + value.NOM_ASSU+ '" data-type="VIE" value="' + value.MONTGARA + '" name="optradio"></td></tr>');
                        });
                    }
                    else
                    {  
                         console.log(datas.iard);
                        $.each(datas.iard, function (index, value)
                        {
                            $( ".tr-bim" ).append('<tr><td>' + value.NUMEPOLI + ' - ' + value.CODEINTE + '</td><td>' + value.PRIMTOTA + '</td><td>' + value.LIBTYPAV + ' (IARD)</td><td><input type="radio" data-id="'+ value.NUMEPOLI +'" data-label="'+ value.LIBTYPAV +'" data-id-assure="' + datas.id+ '" data-code-inter="' + value.CODEINTE+ '" data-telephone="' + value.TELEASSU+ '" data-assure="' + value.NOM_ASSU+ '" data-type="IARD" value="' + value.PRIMTOTA + '" name="optradio"></td></tr>');
                        });
                    }

                }
                else
                {   
                    $.alert('Aucune prime pour ce numéro !');
                    location.reload();
                }
            })
        });


$("#comfirms_1").click(function () {

    var code_prime = $('input[name=optradio]:checked', '#regForm').attr("data-id");
    var label = $('input[name=optradio]:checked', '#regForm').attr("data-label");
    var type = $('input[name=optradio]:checked', '#regForm').attr("data-type");
    var prime_montant = $('input[name=optradio]:checked', '#regForm').val();

    var code_inter = $('input[name=optradio]:checked', '#regForm').attr("data-code-inter");
    var telephone = $('input[name=optradio]:checked', '#regForm').attr("data-telephone");
    var id_assure = $('input[name=optradio]:checked', '#regForm').attr("data-id-assure");

    //var frais = prime_montant*0.03;
    var frais = parseInt(0);
    var total = parseInt(prime_montant)+parseInt(frais);
    var chiffre = parseInt(total);

    console.log(code_prime, id_assure, code_inter, code_prime, telephone, type, label);
    

    $.post("<?php echo site_url("Transactions/getTokens") ?>", {code_prime : code_prime, id_assure : id_assure, code_inter : code_inter, telephone : telephone, chiffre : chiffre, type : type, label : label})
        .done(function (data) {

            data = JSON.parse(data);
            if(data)
            {
                console.log(data.token);
                $(".id").val(code_prime);
                $("#token").val(data.token);
                $("#commande").val(data.commande);
                $("#frais").val(frais);
                $("#label").val(label);
                $(".total").val(chiffre);
                $("#montant").val(prime_montant);
            }
            else
            {
                $.alert("Paiement imposible, veuillez reprendre SVP !");
                return false;
            }
        })
        .fail(function (error) {
            $.alert("Paiement imposible, veuillez reprendre SVP !");
            return false;
        });

    });

</script>


</body>
</html>
