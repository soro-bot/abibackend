<!DOCTYPE html>
<html lang="fr">
<head>
<?php $this->load->view('tpl/css_files'); ?>
</head>

<body class="skin-2 no-skin">
        
        <!-- Navbar -->
          <?php $this->load->view('tpl/header'); ?>
        <!-- /.navbar -->

        <div class="main-container ace-save-state" id="main-container">
            <script type="text/javascript">
                try{ace.settings.loadState('main-container')}catch(e){}
            </script>

            <?php 
              $data['page'] = "users";
              $this->load->view('tpl/sidebar', $data); 
            ?>

            <div class="main-content">
                <div class="main-content-inner">
                    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="ace-icon fa fa-home home-icon"></i>
                                <a href="#">Accueil - Home</a>
                            </li>
                            <li class="active"><?php echo($this->lang->line('utilisateurs')); ?></li>
                        </ul><!-- /.breadcrumb -->

                        <div class="nav-search" id="nav-search" style="display: none;">
                            <form class="form-search">
                                <span class="input-icon">
                                    <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                                    <i class="ace-icon fa fa-search nav-search-icon"></i>
                                </span>
                            </form>
                        </div><!-- /.nav-search -->
                    </div>

                    <div class="page-content">

                         <?php $this->load->view('tpl/setting'); ?>

                        <div class="page-header" <?php if(empty($affiche_users)) echo 'style="display : none !important;"'; ?>>
                            <h1>
                              <a href="<?php echo site_url('Users/addUsers');?>" role="button" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo($this->lang->line('ajouter_btn')); ?></a>
                            </h1>
                        </div><!-- /.page-header -->

                        <?php if ($affiche_users) { ?>
                        <div class="row">
                          <table id="table" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                              <th><?php echo($this->lang->line('text_nom')); ?></th>
                              <th><?php echo($this->lang->line('text_prenom')); ?></th>
                              <th><?php echo($this->lang->line('login_label')); ?></th>
                              <th><?php echo($this->lang->line('text_privilege')); ?></th>
                              <th><?php echo($this->lang->line('etat_label')); ?></th>
                              <th><?php echo($this->lang->line('text_create')); ?></th>
                              <th>Actions</th>
                          </tr>
                          </thead>
                          <tbody>
                              
                          </tbody>
                          </table>
                          <!-- PAGE CONTENT ENDS -->
                          </div><!-- /.col -->
                          <?php } ; ?>

                        <?php if ($modifier_users) { ?>
                        <div class="row">
                          <div class="col-md-offset-3 col-md-5">
                          <?php foreach ($modifier_users as $usr) : ?>
                          <form class="comfirms_2" class="form-horizontal" action="<?php echo site_url('Users/updateUsers')?>" enctype="multipart/form-data" method="post">
                            <div class="form-body">
                            <input type="hidden" class="form-control" required name="id_user" value="<?php echo $usr->id_user; ?>"/>
                            <!-- Date dd/mm/yyyy -->
                            <div class="form-group">
                                <label><?php echo($this->lang->line('text_nom')); ?> :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                         <i class="fa fa-user"></i>
                                    </div>
                                  <input type="text" class="form-control" placeholder="<?php echo($this->lang->line('text_nom')); ?>" required name="nom_user" value="<?php echo $usr->nom_user; ?>"/>
                                </div><!-- /.input group -->
                            </div><!-- /.form group -->

                            <div class="form-group">
                                <label><?php echo($this->lang->line('text_prenom')); ?> :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                         <i class="fa fa-user"></i>
                                    </div>
                                  <input type="text" class="form-control" placeholder="<?php echo($this->lang->line('text_prenom')); ?>" required name="prenoms_user" value="<?php echo $usr->prenoms_user; ?>"/>
                                </div><!-- /.input group -->
                            </div>

                            <div class="form-group">
                                <label><?php echo($this->lang->line('login_label')); ?> :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                         <i class="fa fa-at"></i>
                                    </div>
                                  <input type="email" class="form-control" placeholder="<?php echo($this->lang->line('login_label')); ?>" readonly name="login_user" value="<?php echo $usr->login_user; ?>"/>
                                </div><!-- /.input group -->
                            </div>

                            <div class="form-group">
                                <label><?php echo($this->lang->line('mdp_label')); ?> :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                         <i class="fa fa-lock"></i>
                                    </div>
                                  <input type="text" class="form-control" placeholder="<?php echo($this->lang->line('mdp_label')); ?>" name="password" value=""/>
                                </div><!-- /.input group -->
                            </div>
                            <!-- phone mask -->
                            <div class="form-group">
                              <label><?php echo($this->lang->line('text_privilege')); ?> :</label>
                              <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-users"></i>
                                </div>
                                <select class="select2 form-control" name="role_fk" id="privilege">
                                  <?php foreach ($liste_roles as $roles) : ?>
                                  <option value="<?php echo $roles->id_role ; ?>" <?php if($roles->id_role == $usr->role_fk) echo "selected=true"; ?>><?php echo $roles->libelle_role; ?></option>
                                   <?php endforeach; ?>   
                               </select>
                              </div>
                            </div>


								<!-- <div class="form-group">
									<label><?php echo($this->lang->line('text_prime')); ?> :</label>
									<div class="input-group">
										<div class="input-group-addon">
											<i class="menu-icon fa fa-money"></i>
										</div>
										<select class="select2 form-control" name="role_fk" required="" id="prime">
											<?php foreach ($liste_primes as $primes) : ?>
												<option value="<?php echo $primes->id_prime ; ?>" <?php if($primes->id_prime == $usr->role_fk) echo "selected=true"; ?>><?php echo $primes->libelle_prime; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div> -->
								<div class="form-group" style="display: none;" id="rowAgence">
                              <label><?php echo($this->lang->line('agences')); ?> :</label>
                              <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-bank"></i>
                                </div>
                                <select class="select2 form-control" name="agences_fk">
                                  <option value=""><?php echo($this->lang->line('text_select')); ?></option>
                                  <?php foreach ($liste_agences as $agenx) : ?>
                                  <option value="<?php echo $agenx->id_agence ; ?>"><?php echo $agenx->nom_agence; ?></option>
                                   <?php endforeach; ?>   
                               </select>
                              </div>
                            </div> 
                            <div class="form-group" style="display: none;" id="rowEntreprise">
                              <label><?php echo($this->lang->line('entreprise_name')); ?> :</label>
                              <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-home"></i>
                                </div>
                                <select class="select2 form-control" name="fk_ent">
                                  <option value=""><?php echo($this->lang->line('text_select')); ?></option>
                                  <?php foreach ($liste_entreprises as $ent) : ?>
                                  <option value="<?php echo $ent->id_ent ; ?>" <?php if($ent->id_ent == $usr->fk_ent) echo "selected=true"; ?>><?php echo $ent->ent_raison; ?></option>
                                   <?php endforeach; ?>   
                               </select>
                              </div>
                            </div> 
                            </div>
                            <div class="form-group">
                            <input id="comfirms_2" style="float:right" class="btn btn-primary" type="submit" class="form-control" value="<?php echo $this->lang->line('modifier_btn'); ?>"/>
                            <a style="float:left" class="btn btn-default" href="<?php echo site_url('Users');?>" class="form-control" ><?php echo $this->lang->line('annuler_btn'); ?></a>
                            </div>                                          
                           </form>
                           <?php endforeach;?>
                        </div>
                        <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                        <?php } ; ?>

                        <?php if ($ajouter_users) { ?>
                        <div class="row">
                        <div class="col-md-offset-3 col-md-5">
                          <form class="comfirms_1" class="form-horizontal" action="<?php echo site_url('Users/insertUsers')?>" enctype="multipart/form-data" method="post">
                            <div class="form-body">
                            <!-- Date dd/mm/yyyy -->
                            <div class="form-group">
                                <label><?php echo($this->lang->line('text_nom')); ?> :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                         <i class="fa fa-user"></i>
                                    </div>
                                  <input type="text" class="form-control" placeholder="<?php echo($this->lang->line('text_nom')); ?>" required name="nom_user"/>
                                </div><!-- /.input group -->
                            </div><!-- /.form group -->

                            <div class="form-group">
                                <label><?php echo($this->lang->line('text_prenom')); ?> :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                         <i class="fa fa-user"></i>
                                    </div>
                                  <input type="text" class="form-control" placeholder="<?php echo($this->lang->line('text_prenom')); ?>" required name="prenoms_user"/>
                                </div><!-- /.input group -->
                            </div>

                            <div class="form-group">
                                <label><?php echo($this->lang->line('login_label')); ?> :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                         <i class="fa fa-at"></i>
                                    </div>
                                  <input type="email" class="form-control" placeholder="<?php echo($this->lang->line('login_label')); ?>" required name="login_user"/>
                                </div><!-- /.input group -->
                            </div>

<!--                            <div class="form-group">-->
<!--                                <label>--><?php //echo($this->lang->line('mdp_label')); ?><!-- :</label>-->
<!--                                <div class="input-group">-->
<!--                                    <div class="input-group-addon">-->
<!--                                         <i class="fa fa-lock"></i>-->
<!--                                    </div>-->
<!--                                  <input type="text" class="form-control" placeholder="--><?php //echo($this->lang->line('mdp_label')); ?><!--" required name="password" value="demo"/>-->
<!--                                </div><-- /.input group -->
<!--                            </div>-->
                            <!-- phone mask -->
                            <div class="form-group">
                              <label><?php echo($this->lang->line('text_privilege')); ?> :</label>
                              <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-users"></i>
                                </div>
                                <select class="select2 form-control" name="role_fk" required="" id="privilege">
                                  <?php foreach ($liste_roles as $roles) : ?>
                                  <option value="<?php echo $roles->id_role ; ?>"><?php echo $roles->libelle_role; ?></option>
                                   <?php endforeach; ?>   
                               </select>
                              </div>
                            </div>
								<div class="form-group">
									<label><?php echo($this->lang->line('text_prime')); ?> :</label>
									<div class="input-group">
										<div class="input-group-addon">
											<i class="menu-icon fa fa-money"></i>
										</div>
										<select class="select2 form-control"  required="" id="prime">
											<?php foreach ($liste_primes as $primes) : ?>
												<option value="<?php echo $primes->id_prime ; ?>"><?php echo $primes->libelle_prime; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="form-group" style="display: none;" id="rowAgence">
                              <label><?php echo($this->lang->line('agences')); ?> :</label>
                              <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-bank"></i>
                                </div>
                                <select class="select2 form-control" name="agences_fk">
                                  <option value=""><?php echo($this->lang->line('text_select')); ?></option>
                                  <?php foreach ($liste_agences as $agenx) : ?>
                                  <option value="<?php echo $agenx->id_agence ; ?>"><?php echo $agenx->nom_agence; ?></option>
                                   <?php endforeach; ?>   
                               </select>
                              </div>
                            </div>
                            <div class="form-group" style="display: none;" id="rowEntreprise">
                              <div class="input-group">
                                  <input type="hidden" name="fk_ent" value="<?php echo $this->session->userdata('fk_ent') ?>" />
                              </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <input id="comfirms_1" style="float:right" class="btn btn-primary" type="submit" class="form-control" value="<?php echo $this->lang->line('valider_btn'); ?>"/>
                            <a style="float:left" class="btn btn-default" href="<?php echo site_url('Users');?>" class="form-control" ><?php echo $this->lang->line('annuler_btn'); ?></a>
                            </div>                                          
                           </form>
                        </div>
                        <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                        <?php } ; ?>

                        </div><!-- /.row -->
                    </div><!-- /.page-content -->
                </div>

            <?php $this->load->view('tpl/footer'); ?>
            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

<!-- basic scripts -->
<?php $this->load->view('tpl/js_files'); ?>
<script type="text/javascript">
$(function() {

});

//jQuery(document).ready(function() {
//  <?php
//      if ($this->session->flashdata("success")){
//          echo "toast_success('".$this->session->flashdata("success")."');";
//      }
//
//      if ($this->session->flashdata("error")){
//          echo "toast_error('".$this->session->flashdata("error")."');";
//      }
//  ?>
//});

var table;
$(document).ready(function() {
    //datatables
    table = $('#table').DataTable({
        "ordering": false, 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "dom":  "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
          "buttons": [
          { extend: 'excel', className: 'btn-default', text: 'EXPORT EXCEL'},
          { extend: 'pdf', className: 'btn-default', text: 'EXPORT PDF' },
          ],
        "columnDefs": [ { orderable: false, targets: [0] } ],
        "language" : {
          "sEmptyTable":     "<?php echo $this->lang->line('sEmptyTable'); ?>",
          "sInfo":           "<?php echo $this->lang->line('sInfo'); ?>",
          "sInfoEmpty":      "<?php echo $this->lang->line('sInfoEmpty'); ?>",
          "sInfoFiltered":   "<?php echo $this->lang->line('sInfoFiltered'); ?>",
          "sInfoPostFix":    "",
          "sInfoThousands":  ",",
          "sLengthMenu":     "<?php echo $this->lang->line('sLengthMenu'); ?>",
          "sLoadingRecords": "<?php echo $this->lang->line('sLoadingRecords'); ?>",
          "sProcessing":     "<?php echo $this->lang->line('sProcessing'); ?>",
          "sSearch":         "<?php echo $this->lang->line('sSearch'); ?>",
          "sZeroRecords":    "<?php echo $this->lang->line('sZeroRecords'); ?>",
          "oPaginate": {
            "sFirst":    "<?php echo $this->lang->line('sFirst'); ?>",
            "sLast":     "<?php echo $this->lang->line('sLast'); ?>",
            "sNext":     "<?php echo $this->lang->line('sNext'); ?>",
            "sPrevious": "<?php echo $this->lang->line('sPrevious'); ?>"
          },
          "oAria": {
            "sSortAscending":  "<?php echo $this->lang->line('sSortAscending'); ?>",
            "sSortDescending": "<?php echo $this->lang->line('sSortDescending'); ?>"
          },
          "select": {
                  "rows": {
                    "_": "<?php echo $this->lang->line('_'); ?>",
                    "0": "<?php echo $this->lang->line('0'); ?>",
                    "1": "<?php echo $this->lang->line('1'); ?>"
                  }  
          }
        },
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('Users/ajax_list')?>",
            "type": "POST",
        },
    });

});
</script>


</body>
</html>
