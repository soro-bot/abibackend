<!DOCTYPE html>
<html lang="fr">
<head>
<?php $this->load->view('tpl/css_files'); ?>
</head>

<body class="skin-2 no-skin">
        
        <!-- Navbar -->
          <?php $this->load->view('tpl/header'); ?>
        <!-- /.navbar -->

        <div class="main-container ace-save-state" id="main-container">
            <script type="text/javascript">
                try{ace.settings.loadState('main-container')}catch(e){}
            </script>

            <?php 
              $data['page'] = "agence";
              $this->load->view('tpl/sidebar', $data); 
            ?>

            <div class="main-content">
                <div class="main-content-inner">
                    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="ace-icon fa fa-home home-icon"></i>
                                <a href="#">Accueil - Home</a>
                            </li>
                            <li class="active"><?php echo($this->lang->line('agences')); ?></li>
                        </ul><!-- /.breadcrumb -->

                        <div class="nav-search" id="nav-search" style="display: none;">
                            <form class="form-search">
                                <span class="input-icon">
                                    <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                                    <i class="ace-icon fa fa-search nav-search-icon"></i>
                                </span>
                            </form>
                        </div><!-- /.nav-search -->
                    </div>

                    <div class="page-content">

                         <?php $this->load->view('tpl/setting'); ?>

                        <div class="page-header" <?php if(empty($affiche_agence)) echo 'style="display : none !important;"'; ?>>
                            <h1>
                              <a href="<?php echo site_url('Agence/addAgences');?>" role="button" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo($this->lang->line('ajouter_btn')); ?></a>
                            </h1>
                        </div><!-- /.page-header -->

                        <?php if ($affiche_agence) { ?>
                        <div class="row">
                          <table id="table" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                              <th>Agence</th>
                              <th>Localisation</th>
                              <th><?php echo($this->lang->line('etat_label')); ?></th>
                              <th><?php echo($this->lang->line('text_create')); ?></th>
                              <th>Actions</th>
                          </tr>
                          </thead>
                          <tbody>
                              
                          </tbody>
                          </table>
                          <!-- PAGE CONTENT ENDS -->
                          </div><!-- /.col -->
                          <?php } ; ?>

                        <?php if ($modifier_agence) { ?>
                        <div class="row">
                          <div class="col-md-offset-3 col-md-5">
                          <?php foreach ($modifier_agence as $agenx) : ?>
                          <form class="comfirms_2" class="form-horizontal" action="<?php echo site_url('Agence/updateAgences')?>" enctype="multipart/form-data" method="post">
                            <div class="form-body">
                            <input type="hidden" class="form-control" required name="id_agence" value="<?php echo $agenx->id_agence; ?>"/>
                            <!-- Date dd/mm/yyyy -->
                            <div class="form-group">
                                <label><?php echo($this->lang->line('agence_name')); ?> :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                         <i class="fa fa-bank"></i>
                                    </div>
                                  <input type="text" class="form-control" placeholder="<?php echo($this->lang->line('agence_name')); ?>" value="<?php echo $agenx->nom_agence; ?>" required name="nom_agence"/>
                                </div><!-- /.input group -->
                            </div><!-- /.form group -->

                            <div class="form-group">
                                <label><?php echo($this->lang->line('agence_local')); ?> :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                         <i class="fa fa-map"></i>
                                    </div>
                                  <input type="text" class="form-control" placeholder="<?php echo($this->lang->line('agence_local')); ?>" value="<?php echo $agenx->localisation_agence; ?>" required name="localisation_agence"/>
                                </div><!-- /.input group -->
                            </div>
                            <!-- phone mask -->
                            <div class="form-group">
                              <label><?php echo($this->lang->line('entreprise_name')); ?> :</label>
                              <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-home"></i>
                                </div>
                                <select class="select2 form-control" name="entreprise_fk" required="">
                                  <?php foreach ($liste_entreprises as $ent) : ?>
                                  <option value="<?php echo $ent->id_ent ; ?>"  <?php if($ent->id_ent == $agenx->entreprise_fk) echo "selected=true"; ?>><?php echo $ent->ent_raison; ?></option>
                                  <?php endforeach; ?>   
                               </select>
                              </div>
                            </div> 
                            </div>
                            <div class="form-group">
                            <input id="comfirms_2" style="float:right" class="btn btn-primary" type="submit" class="form-control" value="<?php echo $this->lang->line('modifier_btn'); ?>"/>
                            <a style="float:left" class="btn btn-default" href="<?php echo site_url('Agence');?>" class="form-control" ><?php echo $this->lang->line('annuler_btn'); ?></a>
                            </div>                                          
                           </form>
                           <?php endforeach;?>
                        </div>
                        <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                        <?php } ; ?>

                        <?php if ($ajouter_agence) { ?>
                        <div class="row">
                        <div class="col-md-offset-3 col-md-5">
                          <form class="comfirms_1" class="form-horizontal" action="<?php echo site_url('Agence/insertAgences')?>" enctype="multipart/form-data" method="post">
                            <div class="form-body">
                            <!-- Date dd/mm/yyyy -->
                            <div class="form-group">
                                <label><?php echo($this->lang->line('agence_name')); ?> :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                         <i class="fa fa-bank"></i>
                                    </div>
                                  <input type="text" class="form-control" placeholder="<?php echo($this->lang->line('agence_name')); ?>" required name="nom_agence"/>
                                </div><!-- /.input group -->
                            </div><!-- /.form group -->

                            <div class="form-group">
                                <label><?php echo($this->lang->line('agence_local')); ?> :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                         <i class="fa fa-map"></i>
                                    </div>
                                  <input type="text" class="form-control" placeholder="<?php echo($this->lang->line('agence_local')); ?>" required name="localisation_agence"/>
                                </div><!-- /.input group -->
                            </div>
                            <!-- phone mask -->
                            <div class="form-group">
                              <label><?php echo($this->lang->line('entreprise_name')); ?> :</label>
                              <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-home"></i>
                                </div>
                                  <select class="selectpicker select2 form-control" id="entreprise_fk" required="">
                                      <?php
                                      foreach($liste_entreprises as $ent)
                                          echo '<option data-pays="'.$ent->pays_id.'" data-content="'.$ent->ent_raison.'  (<img class=\' \' width=\'25\' src=\''.img_url()."flags/".$ent->pays_id.".png".'\' />)" value="'.$ent->id_ent.'">'.$ent->ent_raison.' ('.$ent->pays_id.')</option>';

                                      ?>
                                  </select>
                              </div>
                            </div> 
                            </div>
                            <div class="form-group">
                            <input id="comfirms_1" style="float:right" class="btn btn-primary" type="submit" class="form-control" value="<?php echo $this->lang->line('valider_btn'); ?>"/>
                            <a style="float:left" class="btn btn-default" href="<?php echo site_url('Agence');?>" class="form-control" ><?php echo $this->lang->line('annuler_btn'); ?></a>
                            </div>                                          
                           </form>
                        </div>
                        <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                        <?php } ; ?>

                        </div><!-- /.row -->
                    </div><!-- /.page-content -->
                </div>

            <?php $this->load->view('tpl/footer'); ?>
            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

<!-- basic scripts -->
<?php $this->load->view('tpl/js_files'); ?>
<script type="text/javascript">
jQuery(document).ready(function() {
  <?php
      if ($this->session->flashdata("success")){
          echo "toast_success('".$this->session->flashdata("success")."');";
      }

      if ($this->session->flashdata("error")){
          echo "toast_error('".$this->session->flashdata("error")."');";
      }
  ?>
});

var table;
$(document).ready(function() {
    //datatables
    table = $('#table').DataTable({
        "ordering": false, 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "dom":  "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
          "buttons": [
          { extend: 'excel', className: 'btn-default', text: 'EXPORT EXCEL'},
          { extend: 'pdf', className: 'btn-default', text: 'EXPORT PDF' },
          ],
        "columnDefs": [ { orderable: false, targets: [0] } ],
        "language" : {
          "sEmptyTable":     "Aucune donnée disponible dans le tableau",
          "sInfo":           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
          "sInfoEmpty":      "Affichage de l'élément 0 à 0 sur 0 élément",
          "sInfoFiltered":   "(filtré à partir de _MAX_ éléments au total)",
          "sInfoPostFix":    "",
          "sInfoThousands":  ",",
          "sLengthMenu":     "Afficher _MENU_ éléments",
          "sLoadingRecords": "Chargement...",
          "sProcessing":     "Traitement...",
          "sSearch":         "Rechercher :",
          "sZeroRecords":    "Aucun élément correspondant trouvé",
          "oPaginate": {
            "sFirst":    "Premier",
            "sLast":     "Dernier",
            "sNext":     "Suivant",
            "sPrevious": "Précédent"
          },
          "oAria": {
            "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
            "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
          },
          "select": {
                  "rows": {
                    "_": "%d lignes sélectionnées",
                    "0": "Aucune ligne sélectionnée",
                    "1": "1 ligne sélectionnée"
                  }  
          }
        },
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('Agence/ajax_list')?>",
            "type": "POST",
        },
    });

});
</script>


</body>
</html>
