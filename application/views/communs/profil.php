<!DOCTYPE html>
<html lang="fr">
<head>
<?php $this->load->view('tpl/css_files'); ?>
</head>

<body class="skin-2 no-skin">
        
        <!-- Navbar -->
          <?php $this->load->view('tpl/header'); ?>
        <!-- /.navbar -->

        <div class="main-container ace-save-state" id="main-container">
            <script type="text/javascript">
                try{ace.settings.loadState('main-container')}catch(e){}
            </script>

            <?php 
              $data['page'] = "profil";
              $this->load->view('tpl/sidebar', $data); 
            ?>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="ace-icon fa fa-home home-icon"></i>
                                <a href="#">Accueil - Home</a>
                            </li>
                            <li class="active"><?php echo($this->lang->line('mon_compte')); ?></li>
                        </ul><!-- /.breadcrumb -->

                        <div class="nav-search" id="nav-search" style="display: none;">
                            <form class="form-search">
                                <span class="input-icon">
                                    <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                                    <i class="ace-icon fa fa-search nav-search-icon"></i>
                                </span>
                            </form>
                        </div><!-- /.nav-search -->
                    </div>

					<div class="page-content">
						 <?php $this->load->view('tpl/setting'); ?>

						<div class="page-header">
							<h1 style="text-align: center;">
								<?php echo($this->lang->line('mon_compte')); ?>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div>
									<div id="user-profile-1" class="user-profile row">
										<div class="col-xs-12 col-sm-3 center">
											<div>
												<span class="profile-picture">
													<img id="avatar" class="editable img-responsive" alt="<?php echo($this->session->userdata('nom_user')); ?>" src="<?php echo img_url(); ?>avatars/avatar2.png" />
												</span>

												<div class="space-4"></div>

												<div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
													<div class="inline position-relative">
														<a href="#" class="user-title-label dropdown-toggle" data-toggle="dropdown">
															<i class="ace-icon fa fa-circle light-green"></i>
															&nbsp;
															<span class="white"><?php echo $myPerso->nom_user; ?>&nbsp;<?php echo $myPerso->prenoms_user; ?></span>
														</a>

														<ul class="align-left dropdown-menu dropdown-caret dropdown-lighter">
															<li class="dropdown-header" style="display: none;"> Change Status </li>
															<li>
																<a href="<?php echo site_url('Deconnexion');?>">
																	<i class="ace-icon fa fa-circle red"></i>
																	<span class="red"><?php echo($this->lang->line('menu_logout')); ?></span>
																</a>
															</li>
														</ul>
													</div>
												</div>
											</div>

											<div class="space-6"></div>

											<div class="profile-contact-info">
												<div class="profile-contact-links center">
													<a data-target="#myReduction" data-toggle="modal" id="#btn_approvision" data-id="<?php echo $myPerso->id_user; ?>" class="btn btn-link">
														<i class="ace-icon fa fa-lock bigger-120 red"></i>
														<?php echo($this->lang->line('mdp_label')); ?>
													</a>
												</div>

												<div class="space-6"></div>
											</div>

											<div class="hr hr12 dotted"></div>
										</div>

										<div class="col-xs-12 col-sm-9">

											<div class="profile-user-info profile-user-info-striped">
												<form class="comfirms_1"  action="<?php echo site_url('Profil/updateProfil')?>" method="post" role="form">
												<input type="hidden" name="id_user" required="" value="<?php echo $myPerso->id_user; ?>" id="profilID">
												<div class="profile-info-row">
													<div class="profile-info-name"> <?php echo($this->lang->line('text_nom')); ?> </div>

													<div class="profile-info-value">
														<input type="text" name="nom_user" required="" value="<?php echo $myPerso->nom_user; ?>" class="form-control editable" id="nom_user">
														<!-- <span class="editable" id="nom"><?php echo $myPerso->nom_user; ?></span> -->
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> <?php echo($this->lang->line('text_prenom')); ?> </div>

													<div class="profile-info-value">
														<!-- <span class="editable" id="prenom"><?php echo $myPerso->prenoms_user; ?></span> -->
														<input type="text" name="prenoms_user" required="" value="<?php echo $myPerso->prenoms_user; ?>" class="form-control editable" id="prenoms_user">
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> <?php echo($this->lang->line('login_label')); ?> </div>

													<div class="profile-info-value">
														<span class="editable" id="email"><?php echo $myPerso->login_user; ?></span>
													</div>
												</div>


												<div class="profile-info-row">
													<div class="profile-info-name"> <?php echo($this->lang->line('text_create')); ?> </div>

													<div class="profile-info-value">
														<span class="editable" id="signup"><?php echo $myPerso->date_create_user; ?></span>
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"><?php echo($this->lang->line('text_privilege')); ?></div>

													<div class="profile-info-value">
														<span class="editable" id="login"><?php echo $myPerso->libelle_role; ?></span>
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"></div>

													<div class="profile-info-value center">
														<input id="comfirms_1" class="btn btn-warning" type="submit" name="modifier"  value="<?php echo($this->lang->line('modifier_btn')); ?>">
													</div>
												</div>
												</form>
											</div>

											<div class="space-20"></div>

											<div class="row">
										    <legend style="text-align: center;"><?php echo($this->lang->line('traces_text')); ?></legend>
											<table id="table" class="table table-bordered table-striped">
					                          <thead>
					                          <tr>
					                              <th>Actions</th>
					                              <th><?php echo($this->lang->line('login_label')); ?></th>
					                              <th>Description</th>
					                              <th>Date</th>
					                          </tr>
					                          </thead>
					                          <tbody>
					                              
					                          </tbody>
					                          </table>
										</div>
							
										</div>
									</div>
								</div>

								<div class="modal fade" id="myReduction" tabindex="1" role="dialog" aria-labelledby="my_modalLabel-1">
							          <div class="modal-dialog">
							            <!-- Modal content-->
							            <div class="modal-content">
							              <div class="modal-header">
							                <button type="button" class="close" data-dismiss="modal">&times;</button>
							                <h4 class="modal-title text-center"><b style="color: #f87114;"><?php echo $this->lang->line('label_modifier_password'); ?></b></h4>
							              </div>
							              <form class="comfirms_2"  action="<?php echo site_url('Profil/updatePassword')?>" method="post" role="form">
							              <input type="hidden" name="id_user" id="idProfil" value="" />
							              <div class="modal-body">
							                <div class="form-group"> 
							                  <label for="text" class="col-sm-4 control-label"><?php echo $this->lang->line('last_password'); ?></label> 
							                  <div class="col-sm-8"> 
							                  <input type="password" class="form-control" name="lastPassword" placeholder="<?php echo $this->lang->line('last_password'); ?>" /> 
							                  </div> 
							                </div>
							                <div class="form-group" style="margin-top: 50px;"> 
							                  <label for="text" class="col-sm-4 control-label"><?php echo $this->lang->line('mdp_label'); ?></label> 
							                  <div class="col-sm-8"> 
							                  <input type="password" class="form-control" name="password" placeholder="<?php echo $this->lang->line('mdp_label'); ?>" id="password"/> 
							                  </div> 
							                </div> 
							                <div class="form-group" style="margin-top: 100px;"> 
							                  <label for="text" class="col-sm-4 control-label"><?php echo $this->lang->line('ressaisir_mdp_label'); ?></label> 
							                  <div class="col-sm-8"> 
							                  <input type="password" class="form-control" name="rpassword" placeholder="<?php echo $this->lang->line('ressaisir_mdp_label'); ?>" id="rpassword" onKeyup="checkPass()"/> 
							                  </div> 
							                </div> 

							                <div class="form-group" style="margin-top: 150px;"> 
							                  <b style="color: red; text-align: center;"><div id="divInfo"></div></b>
							                </div>   
							               
							              </div>
							              <div class="modal-footer" style="text-align: center; ">
							                <a class="btn btn-default" data-dismiss="modal" class="form-control" ><?php echo $this->lang->line('annuler_btn'); ?></a>
							                <button id="comfirms_2" type="button" class="btn btn-primary"><?php echo $this->lang->line('modifier_btn'); ?></button>
							              </div>
							             </form>
							            </div>
							          </div>
							        </div>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php $this->load->view('tpl/footer'); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->
		<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<?php $this->load->view('tpl/js_files'); ?>
<script type="text/javascript">
function checkPass()
{
  var password = document.getElementById("password").value;
  var rpassword = document.getElementById("rpassword").value;
  var divInfo = document.getElementById("divInfo");
   
  if(password == rpassword & password.length >= 6)
  {
      divInfo.innerHTML = "Mot de passe corrects / Password are goods !";
  }
  else if(password == rpassword & password.length < 6)
  {
      divInfo.innerHTML = "Longueur mot de passe < 06 / Size password < 06 !";
  }
  else
  {
      divInfo.innerHTML = "Mot De Passe différents / Password are differents !";
  }
}

$(document).on('click', "[data-id]", function() {
    var idProfil = $(this).attr('data-id');
    $("#idProfil").val(idProfil);
    $('#myReduction').modal('show');
});

jQuery(document).ready(function() {
  <?php
      if ($this->session->flashdata("success")){
          echo "toast_success('".$this->session->flashdata("success")."');";
      }

      if ($this->session->flashdata("error")){
          echo "toast_error('".$this->session->flashdata("error")."');";
      }
  ?>
});

var table;
$(document).ready(function() {
    //datatables
    table = $('#table').DataTable({
        "ordering": false, 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "dom":  "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
          "buttons": [
          { extend: 'excel', className: 'btn-default', text: 'EXPORT EXCEL'},
          { extend: 'pdf', className: 'btn-default', text: 'EXPORT PDF' },
          ],
        "columnDefs": [ { orderable: false, targets: [0] } ],
        "language" : {
          "sEmptyTable":     "<?php echo $this->lang->line('sEmptyTable'); ?>",
          "sInfo":           "<?php echo $this->lang->line('sInfo'); ?>",
          "sInfoEmpty":      "<?php echo $this->lang->line('sInfoEmpty'); ?>",
          "sInfoFiltered":   "<?php echo $this->lang->line('sInfoFiltered'); ?>",
          "sInfoPostFix":    "",
          "sInfoThousands":  ",",
          "sLengthMenu":     "<?php echo $this->lang->line('sLengthMenu'); ?>",
          "sLoadingRecords": "<?php echo $this->lang->line('sLoadingRecords'); ?>",
          "sProcessing":     "<?php echo $this->lang->line('sProcessing'); ?>",
          "sSearch":         "<?php echo $this->lang->line('sSearch'); ?>",
          "sZeroRecords":    "<?php echo $this->lang->line('sZeroRecords'); ?>",
          "oPaginate": {
            "sFirst":    "<?php echo $this->lang->line('sFirst'); ?>",
            "sLast":     "<?php echo $this->lang->line('sLast'); ?>",
            "sNext":     "<?php echo $this->lang->line('sNext'); ?>",
            "sPrevious": "<?php echo $this->lang->line('sPrevious'); ?>"
          },
          "oAria": {
            "sSortAscending":  "<?php echo $this->lang->line('sSortAscending'); ?>",
            "sSortDescending": "<?php echo $this->lang->line('sSortDescending'); ?>"
          },
          "select": {
                  "rows": {
                    "_": "<?php echo $this->lang->line('_'); ?>",
                    "0": "<?php echo $this->lang->line('0'); ?>",
                    "1": "<?php echo $this->lang->line('1'); ?>"
                  }  
          }
        },
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('Profil/ajax_list')?>",
            "type": "POST",
        },
    });

});
</script>
	</body>
</html>
