<!DOCTYPE html>
<html lang="fr">
<head>
    <?php $this->load->view('tpl/login_css_files'); ?>
</head>
<body>

    <div class="modal fade" id="modal_confirm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel3"
         aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×</button>
                    <h3 class="modal-title" id="myModalLabel"><?php echo $this->lang->line('confirm_modal_titre'); ?></h3>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <!-- BEGIN HEADER -->
        <div class="header mb30">
            <div class="menu-bar">
                <div class="row" style="margin: 0">
                    <div class="col-sm-5 col-md-offset-1 logo">
                        <!-- <img src="<?php echo img_url("logo_white.png") ?>"/> -->
                    </div>
                    <div class="col-sm-5 col-sm-offset-1" style="text-align: right">
                        <select style="width: auto; float: right; margin-top: 10px" class="form-control" onchange="javascript:window.location.href='<?php echo site_url(); ?>/LanguageSwitcher/switchLang/'+this.value;">
                            <option value="francais" <?php if($this->session->userdata('site_lang') == "francais") echo 'selected="selected"'; ?>><?php echo $this->lang->line('francais'); ?></option>
                            <option value="english" <?php if($this->session->userdata('site_lang') == "english") echo 'selected="selected"'; ?>><?php echo $this->lang->line('anglais'); ?></option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <!-- END HEADER -->

        <div class="tab-block" style="margin-right: 100px;">
        <form class="" id="comfirms_form"  action="<?php echo site_url('Accueil/updatePassword')?>" method="post" role="form" autocomplete="off" style="background-color: #faebd766">
        <legend style="font-size: 25px; text-align: center;"><b><?php echo $this->lang->line('label_modifier_password'); ?></b></legend>
        <?php if($query) : ?>
        <div class="form-body" style="position: center;">
        <!-- One "tab" for each step in the form: -->
          <input type="hidden" class="form-control" required name="id_user" value="<?php echo $query->id_user; ?>"/>
          <input type="hidden" name="envoyer" value="envoyer" />
          <div class="form-row">
            <div class="form-group">
              <label class="col-sm-4 control-label"><?php echo $this->lang->line('mdp_label'); ?></label>
              <div class="col-sm-8 input-group">
                  <div class="input-group-addon">
                       <i class="fa fa-lock"></i>
                  </div>
                 <input type="password" class="form-control" placeholder="<?php echo $this->lang->line('mdp_label'); ?>" required name="password" id="password" autocomplete="false" onpaste="return false"/>
              </div><!-- /.input group -->
            </div>
          </div>
          <div class="form-row">
            <div class="form-group">
              <label class="col-sm-4 control-label"><?php echo $this->lang->line('ressaisir_mdp_label'); ?></label>
              <div class="col-sm-8 input-group">
                  <div class="input-group-addon">
                       <i class="fa fa-lock"></i>
                  </div>
                <input type="password" class="form-control" placeholder="<?php echo $this->lang->line('ressaisir_mdp_label'); ?>" required name="rpassword" id="rpassword" autocomplete="false" onpaste="return false"/>
              </div><!-- /.input group -->
            </div>
          </div>
          <div class="row error hide" style="color: red;">

			  <div class="col-sm-offset-4 col-sm-8">
				  <span class="">Le mot de passe doit:</span><br><span class="">* Avoir 8 charactères minimum</span><br><span>* Contenir au moins 1 majuscule</span><br><span>* Contenir au moins 1 minuscule</span><br><span>* Contenir au moins 1 chiffre</span><br><span>* Contenir au moins 1 car. spécial</span><br>
			  </div><!-- /.input group -->
          </div>
			<br>
          <div class="form-row text-center" style="padding-bottom: 10px;">
            <a class="btn btn-default" href="<?php echo site_url('Accueil')?>"><i class="fa fa-home"></i> <?php echo $this->lang->line('annuler_btn'); ?></a>
            <button type="button" id="comfirms_1" class="btn btn-warning"><?php echo $this->lang->line('modifier_btn'); ?> <i class="fa fa-thumbs-up"></i></button>
          </div>
        </div>
        <?php endif;?>
        </form>
        </div>

    </div>
<div class="md-overlay"></div>
<?php $this->load->view('tpl/login_js_files'); ?>
<script type="text/javascript">
function checkPass()
{
    var password = document.getElementById("password").value;
    var rpassword = document.getElementById("rpassword").value;
    //var div_comp = document.getElementById("divcomp");
     
    if(password == rpassword && password.length >= 6)
    {
        //divcomp.innerHTML = "Les Mots De Passe Sont Corrects";
        <?php
            echo "toast_success('Mot de passe corrects / Password are goods !');";
        ?>
    }
    else if(password == rpassword && password.length < 6)
    {
        //divcomp.innerHTML = "Les Mot De Passe Doivent Avoir Une Longueur Supérieure ou égale à 6 Caractères!";
        <?php
            echo "toast_error('Longueur mot de passe < 06 / Size password < 06 !');";
        ?>
    }
    else
    {
        //divcomp.innerHTML = "Les Mot De Passe Ne Sont Pas Identiques!";
        <?php
            echo "toast_error('Mot De Passe différents / Password are differents !');";
        ?>
    }
}

$("#comfirms_1").click(function (e) {
	e.preventDefault();

	var password = document.getElementById("password").value;
	var rpassword = document.getElementById("rpassword").value;
	//var div_comp = document.getElementById("divcomp");
	let strongPassword = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})');
	console.log(strongPassword);
	console.log(password);
	console.log(rpassword);
	console.log(password !== rpassword);

	if(password === rpassword && strongPassword.test(password))
	{
		$("#comfirms_form").submit();
		//divcomp.innerHTML = "Les Mots De Passe Sont Corrects";
		// toast_success('Mot de passe corrects / Password are goods !');
	}
	else if(password !== rpassword)
	{
		//divcomp.innerHTML = "Les Mot De Passe Doivent Avoir Une Longueur Supérieure ou égale à 6 Caractères!";
		<?php
		echo "toast_error('Les mots de passes ne sont pas conformes ');";
		?>
	}
	else
	{
		$(".error").toggleClass("hide");
		//divcomp.innerHTML = "Les Mot De Passe Ne Sont Pas Identiques!";
		<?php
		echo "toast_error('Mot De Passe différents / Password are differents !');";
		?>
	}


})

//$('#comfirms_1').confirm({
//	theme: 'material',
//	title: 'CONFIRMATION !',
//	content: '<?php //echo $this->lang->line('confirm_modal_text'); ?>//',
//	buttons: {
//		cancel: {
//			text: '<?php //echo $this->lang->line('confirm_modal_annuler_btn'); ?>//',
//			btnClass: 'btn-default',
//		},
//		confirm: {
//			text: '<?php //echo $this->lang->line('confirm_modal_valider_btn'); ?>//',
//			btnClass: 'btn-blue',
//			action: function () {
//				$(".comfirms_1").submit();
//			}
//		},
//	},
//
//});

jQuery(document).ready(function() {
  <?php
      if ($this->session->flashdata("success")){
          echo "toast_success('".$this->session->flashdata("success")."');";
      }

      if ($this->session->flashdata("error")){
          echo "toast_error('".$this->session->flashdata("error")."');";
      }
  ?>
});

    
</script>
<!--    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
</body>
</html>
