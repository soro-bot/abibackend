<?php
   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
   
  class Dashboard_model extends CI_Model {

    protected $table_transactions = "transactions";

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('UTC');
    }

    public function getTotalTrans($date_min, $date_max, $ent_fk = null)
    { 
        $role_fk = $this->session->userdata('role_fk');
        $fk_ent = $this->session->userdata('fk_ent');
        $fk_pays = $this->session->userdata('fk_pays');

        if ($role_fk == 1) {
            if ($ent_fk){
                return $this->db->select('count(transactions.id_trans) as total_trans, sum(transactions.montant_trans) as montant_trans')
                    ->from($this->table_transactions)
                    ->where('transactions.date_create_trans >=', $date_min)
                    ->where('transactions.date_create_trans <=', $date_max)
                    ->where('transactions.ent_fk =', $ent_fk)
                    ->get()
                    ->row();
            } else {
                return $this->db->select('count(transactions.id_trans) as total_trans, sum(transactions.montant_trans) as montant_trans')
                    ->from($this->table_transactions)
                    ->where('transactions.date_create_trans >=', $date_min)
                    ->where('transactions.date_create_trans <=', $date_max)
                    ->get()
                    ->row();
            }
        }
        //elseif ($role_fk == 2 OR $role_fk == 3) {
        //   return $this->db->select('count(transactions.id_trans) as total_trans, sum(transactions.montant_trans) as montant_trans')
        //                  ->from($this->table_transactions)
        //                  ->where('transactions.pays_fk', $fk_pays)
        //                  ->where('transactions.date_create_trans >=', $date_min)
        //                  ->where('transactions.date_create_trans <=', $date_max)
        //                  ->get()
        //                  ->row();
        //}
        else
        {
            return $this->db->select('count(transactions.id_trans) as total_trans, sum(transactions.montant_trans) as montant_trans')
                          ->from($this->table_transactions)
                          ->where('transactions.ent_fk', $fk_ent)
                          ->where('transactions.date_create_trans >=', $date_min)
                          ->where('transactions.date_create_trans <=', $date_max)
                          ->get()
                          ->row();
        }
    }

    public function getTotalPrimes($date_min, $date_max, $ent_fk = null)
    { 
        $role_fk = $this->session->userdata('role_fk');
        $fk_ent = $this->session->userdata('fk_ent');
        $fk_pays = $this->session->userdata('fk_pays');

        if ($role_fk == 1) {
            if ($ent_fk){
                return $this->db->select('count(transactions.id_trans) as total_trans, sum(transactions.montant_trans) as montant_trans')
                    ->from($this->table_transactions)
                    ->where('transactions.date_create_trans >=', $date_min)
                    ->where('transactions.date_create_trans <=', $date_max)
                    ->where('transactions.ent_fk =', $ent_fk)
                    ->where('transactions.statut_trans', 'S')
                    ->get()
                    ->row();
            } else {
                return $this->db->select('count(transactions.id_trans) as total_trans, sum(transactions.montant_trans) as montant_trans')
                    ->from($this->table_transactions)
                    ->where('transactions.date_create_trans >=', $date_min)
                    ->where('transactions.date_create_trans <=', $date_max)
                    ->where('transactions.statut_trans', 'S')
                    ->get()
                    ->row();
            }
        }
        elseif ($role_fk == 2 OR $role_fk == 3) {
           return $this->db->select('count(transactions.id_trans) as total_trans, sum(transactions.montant_trans) as montant_trans')
                          ->from($this->table_transactions)
                          ->where('transactions.statut_trans', 'S')
                          ->where('transactions.pays_fk', $fk_pays)
                          ->where('transactions.date_create_trans >=', $date_min)
                          ->where('transactions.date_create_trans <=', $date_max)
                          ->get()
                          ->row();
        }
        else
        {
            return $this->db->select('count(transactions.id_trans) as total_trans, sum(transactions.montant_trans) as montant_trans')
                          ->from($this->table_transactions)
                          ->where('transactions.statut_trans', 'S')
                          ->where('transactions.ent_fk', $fk_ent)
                          ->where('transactions.date_create_trans >=', $date_min)
                          ->where('transactions.date_create_trans <=', $date_max)
                          ->get()
                          ->row();
        }
    }
      
     
  }
?>