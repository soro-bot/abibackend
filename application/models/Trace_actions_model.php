<?php
defined ('BASEPATH') OR exit ('No  direct   script   access   allowed');
//CREER LE CONSTRUCTEUR DU MODEL

	class Trace_actions_model extends CI_Model {
	
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('UTC');
    }
    
	protected $table_trace ="trace";

	public function getActions($utilisateurs, $date_min, $date_max)
	{		
			$id_user = $this->session->userdata('id_user'); 
			$role_fk = $this->session->userdata('role_fk');
	        $fk_pays = $this->session->userdata('fk_pays');
		    $query = $this->db->select('*')
			 				 ->from($this->table_trace)
			 				 ->join('user', 'user.id_user = trace.user_fk', 'left')
			 				 ->join('entreprise', 'entreprise.id_ent = user.fk_ent', 'left')
			 				 ->where('trace.trace_date >=', $date_min)
			 				 ->where('trace.trace_date <=', $date_max);

			if($utilisateurs != '0')
			{
			 	$this->db->where('trace.user_fk', $utilisateurs);
			}
			else
			{
		    	if ($role_fk == 2 OR $role_fk == 3)
		    	{	
		    		$this->db->where('entreprise.pays_id', $fk_pays);
		    	}
		    	if ($user_role !== 1)
		    	{
		    		$this->db->where('trace.user_fk', $id_user);
		    	}
			}
						 
			$query = $this->db->order_by("trace.trace_date","desc")
		                     ->get();
		        return $query->result();
	}

		var $column_order = array('trace_action', 'prenoms_user', 'trace_complement', 'nom_user', 'trace_date'); 
		//set column field database for datatable orderable
    	var $column_search = array('trace_action', 'prenoms_user', 'trace_complement', 'nom_user', 'trace_date'); 
    	//set column field database for datatable searchable 
    	var $order = array('trace.trace_date' => 'desc'); // default order 
	

		public function _get_datatables_query($utilisateurs, $date_min, $date_max)
		{ 	
			$id_user = $this->session->userdata('id_user'); 
			$role_fk = $this->session->userdata('role_fk');
	        $fk_pays = $this->session->userdata('fk_pays');
     		$this->db->select('*')
	 				 ->from($this->table_trace)
	 				 ->join('user', 'user.id_user = trace.user_fk', 'left')
	 				 ->join('entreprise', 'entreprise.id_ent = user.fk_ent', 'left')
	 				 ->where('trace.trace_date >=', $date_min)
	 				 ->where('trace.trace_date <=', $date_max);

				if($utilisateurs != '0')
				{
				 	$this->db->where('trace.user_fk', $utilisateurs);
				}
				else
				{
			    	if ($role_fk == 2 OR $role_fk == 3)
			    	{	
			    		$this->db->where('entreprise.pays_id', $fk_pays);
			    	}
			    	if ($user_role !== 1)
			    	{
			    		$this->db->where('trace.user_fk', $id_user);
			    	}
				}

			 $i = 0;
      
	        foreach ($this->column_search as $item) // loop column 
	        {
		          if($_POST['search']['value']) // if datatable send POST for search
		          {
		             
		            if($i===0) // first loop
		            {
		              $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
		              $this->db->like($item, $_POST['search']['value']);
		            }
		            else
		            {
		              $this->db->or_like($item, $_POST['search']['value']);
		            }

		            if(count($this->column_search) - 1 == $i) //last loop
		              $this->db->group_end(); //close bracket
		          }
		          $i++;		
			}

			if(isset($_POST['order'])) // here order processing
	        {
	          $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	          $order = $this->order;
	          $this->db->order_by(key($order), $order[key($order)]);
	        }
		}

		function get_datatables($utilisateurs, $date_min, $date_max)
        {
          $this->_get_datatables_query($utilisateurs, $date_min, $date_max);
          if($_POST['length'] != -1)
          $this->db->limit($_POST['length'], $_POST['start']);
          $query = $this->db->get();
          return $query->result();
        }

        function count_filtered($utilisateurs, $date_min, $date_max)
        {
          $this->_get_datatables_query($utilisateurs, $date_min, $date_max);
          $query = $this->db->get();
          return $query->num_rows();
        }

        public function count_all($utilisateurs, $date_min, $date_max)
        {
            			$id_user = $this->session->userdata('id_user'); 
			$role_fk = $this->session->userdata('role_fk');
	        $fk_pays = $this->session->userdata('fk_pays');
     		$this->db->select('*')
	 				 ->from($this->table_trace)
	 				 ->join('user', 'user.id_user = trace.user_fk', 'left')
	 				 ->join('entreprise', 'entreprise.id_ent = user.fk_ent', 'left')
	 				 ->where('trace.trace_date >=', $date_min)
	 				 ->where('trace.trace_date <=', $date_max);

				if($utilisateurs != '0')
				{
				 	$this->db->where('trace.user_fk', $utilisateurs);
				}
				else
				{
			    	if ($role_fk == 2 OR $role_fk == 3)
			    	{	
			    		$this->db->where('entreprise.pays_id', $fk_pays);
			    	}
			    	if ($user_role !== 1)
			    	{
			    		$this->db->where('trace.user_fk', $id_user);
			    	}
				}

          	 return $this->db->count_all_results();
        }
		

}

?>