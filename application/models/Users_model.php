<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users_model extends CI_Model
{
	protected $table_user = 'user';
	protected $table_affectation = 'affectation';
	protected $table_agence = 'agence';
	protected $table_roles = 'roles';
	protected $table_primes = 'primes';
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("UTC");
	}

	public function getUsers()
	{	
		$role_fk = $this->session->userdata('role_fk');
        $fk_ent = $this->session->userdata('fk_ent');
        $fk_pays = $this->session->userdata('fk_pays');
        $prime_fk = $this->session->userdata('prime_fk'); 

        if ($role_fk == 1) 
        {
		  $query = $this->db->select("*")
							->from($this->table_user)
							->join('entreprise', 'entreprise.id_ent = user.fk_ent', 'left')
							->join('roles', "roles.id_role = user.role_fk")
							->where('user.id_user <>', '1')
			  				->where('id_role =', 2)
							->order_by("user.date_create_user","desc")
							->get();
		  return $query->result();
		}
		elseif ($role_fk == 2 OR $role_fk == 3) {
			$query = $this->db->select("*")
							->from($this->table_user)
							->join('entreprise', 'entreprise.id_ent = user.fk_ent', 'left')
							->join('roles', "roles.id_role = user.role_fk")
							->where('entreprise.pays_id', $fk_pays)
							->where('user.id_user !=', '1')
							->order_by("user.date_create_user","desc")
							->get();
		  return $query->result();
		}
		else
		{
			$query = $this->db->select("*")
							->from($this->table_user)
							->join('entreprise', 'entreprise.id_ent = user.fk_ent', 'left')
							->join('roles', "roles.id_role = user.role_fk")
							->where('user.fk_ent', $fk_ent)
							->where('user.id_user !=', '1')
							->order_by("user.date_create_user","desc")
							->get();
		  return $query->result();
		}		
	}

	public function getActifUsers()
	{	
		$role_fk = $this->session->userdata('role_fk');
        $fk_ent = $this->session->userdata('fk_ent');
        $fk_pays = $this->session->userdata('fk_pays');

        if ($role_fk == 1) 
        {
		  $query = $this->db->select("*")
							->from($this->table_user)
							->join('entreprise', 'entreprise.id_ent = user.fk_ent', 'left')
							->join('roles', "roles.id_role = user.role_fk")
							->where("user.user_etat", "A")
							->where('user.id_user !=', '1')
							->order_by("user.date_create_user","desc")
							->get();
		  return $query->result();
		}
		elseif ($role_fk == 2 OR $role_fk == 3) {
			$query = $this->db->select("*")
							->from($this->table_user)
							->join('entreprise', 'entreprise.id_ent = user.fk_ent', 'left')
							->join('roles', "roles.id_role = user.role_fk")
							->where('user.id_user !=', '1')
							->where('entreprise.pays_id', $fk_pays)
							->where("user.user_etat", "A")
							->order_by("user.date_create_user","desc")
							->get();
		  return $query->result();
		}
		else
		{
			$query = $this->db->select("*")
							->from($this->table_user)
							->join('entreprise', 'entreprise.id_ent = user.fk_ent', 'left')
							->join('roles', "roles.id_role = user.role_fk")
							->where('user.fk_ent', $fk_ent)
							->where('user.id_user !=', '1')
							->where("user.user_etat", "A")
							->order_by("user.date_create_user","desc")
							->get();
		  return $query->result();
		}		
	}

	var $column_order = array('nom_user', 'prenoms_user','login_user','date_create_user', 'ent_raison'); //set column field database for datatable orderable
    var $column_search = array('nom_user', 'prenoms_user','login_user','date_create_user', 'ent_raison'); //set column field database for datatable searchable 
    var $order = array('user.date_create_user' => 'desc'); // default order 

	private function _get_datatables_query()
    {
        $role_fk = $this->session->userdata('role_fk');
        $fk_ent = $this->session->userdata('fk_ent');
        $fk_pays = $this->session->userdata('fk_pays');

        if ($role_fk == 1) 
        {
	         $this->db->select("*")
						->from($this->table_user)
						->join('entreprise', 'entreprise.id_ent = user.fk_ent', 'left')
						->join('roles', "roles.id_role = user.role_fk")
						->where('user.id_user !=', '1');
		}
		//elseif ($role_fk == 2 OR $role_fk == 3) {
		// 	 $this->db->select("*")
		//				->from($this->table_user)
		//				->join('entreprise', 'entreprise.id_ent = user.fk_ent', 'left')
		//				->join('roles', "roles.id_role = user.role_fk")
		//				->where('user.id_user !=', '1')
		//				->where('entreprise.pays_id', $fk_pays);
		//}
		else
		{
		 	 $this->db->select("*")
						->from($this->table_user)
						->join('entreprise', 'entreprise.id_ent = user.fk_ent', 'left')
						->join('roles', "roles.id_role = user.role_fk")
						->where('user.fk_ent', $fk_ent)
						->where('user.role_fk NOT IN (1, 2) ');
						//->where('user.id_user !=', '1');
		}		

        $i = 0;
      
        foreach ($this->column_search as $item) // loop column 
        {
          if($_POST['search']['value']) // if datatable send POST for search
          {
            
            if($i===0) // first loop
            {
              $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
              $this->db->like($item, $_POST['search']['value']);
            }
            else
            {
              $this->db->or_like($item, $_POST['search']['value']);
            }

            if(count($this->column_search) - 1 == $i) //last loop
              $this->db->group_end(); //close bracket
          }
          $i++;
        }
      
      if(isset($_POST['order'])) // here order processing
      {
        $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      } 
      else if(isset($this->order))
      {
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
      }
    }

    function get_datatables()
    {
      $this->_get_datatables_query();
      if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();
      return $query->result();
    }

    function count_filtered()
    {
      $this->_get_datatables_query();
      $query = $this->db->get();
      return $query->num_rows();
    }

    public function count_all()
    {
        $role_fk = $this->session->userdata('role_fk');
        $fk_ent = $this->session->userdata('fk_ent');
        $fk_pays = $this->session->userdata('fk_pays');

        if ($role_fk == 1) 
        {
	         $this->db->select("*")
						->from($this->table_user)
						->join('entreprise', 'entreprise.id_ent = user.fk_ent', 'left')
						->join('roles', "roles.id_role = user.role_fk")
						->where('user.id_user !=', '1');
		}
		elseif ($role_fk == 2 OR $role_fk == 3) {
		 	 $this->db->select("*")
						->from($this->table_user)
						->join('entreprise', 'entreprise.id_ent = user.fk_ent', 'left')
						->join('roles', "roles.id_role = user.role_fk")
						->where('user.id_user !=', '1')
						->where('entreprise.pays_id', $fk_pays);
		}
		else
		{
		 	 $this->db->select("*")
						->from($this->table_user)
						->join('entreprise', 'entreprise.id_ent = user.fk_ent', 'left')
						->join('roles', "roles.id_role = user.role_fk")
						->where('user.fk_ent', $fk_ent)
						->where('user.id_user !=', '1');
		}		

     	return $this->db->count_all_results();
    }

    public function existeUsers($login_user)
	{
		return $this->db->select('*')
		 				->from($this->table_user)
		 				->where('login_user', $login_user)
						->get()
			            ->row();
	}

	public function existeAffecation($agences_fk, $users_fk)
	{
		$query = $this->db->select('*')
		 				->from($this->table_affectation)
		 				->where('agences_fk', $agences_fk)
		 				->where('users_fk', $users_fk)
		 				->where('etat_affectation', "A")
						->get()
			            ->row();

		if (empty($query)) 
		{
			$query = $this->db->select('*')
			 				->from($this->table_affectation)
			 				->where('users_fk', $users_fk)
			 				->where('etat_affectation', "A")
							->get()
				            ->row();
		}

		if ($query) 
		{
			return $query;
		}
		else
		{
			return NULL;
		}
	}

	public function getProfil($id_user)
    {
        $query = $this->db->select('*')
		 				 ->from($this->table_user)
		 				 ->join('roles', 'roles.id_role = user.role_fk', 'left')
		 				 ->where('user.id_user', $id_user)
						 ->get();
			return $query->result();
    }

    public function retirerAffecter($users_fk)
	{
        return $this->db->set('etat_affectation', "I")
			            ->where('users_fk', $users_fk)
			            ->update($this->table_affectation);
	}

    public function doAffectations($agences_fk, $users_fk)
	{			
			   $this->db->set('agences_fk', $agences_fk)
						->set('date_affectation', date("Y-m-d H:i:s"))
						->set('etat_affectation', "A")
						->set('users_fk', $users_fk)
						->insert($this->table_affectation);
		return $this->db->insert_id();
	}


	public function ajouterUsers($role_fk, $nom_user, $prenoms_user, $login_user, $fk_ent)
	{
		$code_user = uniqid();
			   $parent_id = $this->session->userdata('id_user');
			   $this->db->set('role_fk', $role_fk)
						->set('code_user', $code_user)
						->set('nom_user', $nom_user)
						->set('fk_ent', $fk_ent)
						->set('prenoms_user', $prenoms_user)
						->set('parent_id', $parent_id)
						->set('login_user', $login_user)
						->set('date_create_user', date("Y-m-d H:i:s"))
						->set('user_etat', "A")
						->insert($this->table_user);
		return $code_user;
	}

	public function modifierUsers($role_fk, $nom_user, $prenoms_user, $id_user, $password, $fk_ent)
	{	
		if ($password) {
			return $this->db->set('role_fk', $role_fk)
							->set('nom_user', $nom_user)
							->set('fk_ent', $fk_ent)
							->set('prenoms_user', $prenoms_user)
							->set('pass_user', md5($password))
							->where('id_user', $id_user)
							->update($this->table_user);
		}
		else
		{
			return $this->db->set('role_fk', $role_fk)
							->set('nom_user', $nom_user)
							->set('fk_ent', $fk_ent)
							->set('prenoms_user', $prenoms_user)
							->where('id_user', $id_user)
							->update($this->table_user);
		}
	}

	public function activerUsers($id_user)
	{
        return $this->db->set('user_etat', "A")
			            ->where('id_user', $id_user)
			            ->update($this->table_user);
	}

	public function desactiverUsers($id_user)
	{
        return $this->db->set('user_etat', "I")
			            ->where('id_user', $id_user)
			            ->update($this->table_user);
	}

	


}
