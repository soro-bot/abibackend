<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Transactions_model extends CI_Model
{
	protected $table_transactions = 'transactions';
	protected $table_agence = 'agence';

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('UTC');
	}

	public function getTransactions($statut, $type, $provider, $date_min, $date_max)
	{	
	  	$role_fk = $this->session->userdata('role_fk');
        $fk_ent = $this->session->userdata('fk_ent');
        $fk_pays = $this->session->userdata('fk_pays');


        if ($role_fk == 1) {
           $query =  $this->db->select('*')
                          ->from($this->table_transactions)
                          ->join('entreprise', 'entreprise.id_ent = transactions.ent_fk', 'left')
                          ->where('transactions.date_create_trans >=', $date_min)
                          ->where('transactions.date_create_trans <=', $date_max);
        }
        elseif ($role_fk == 2 OR $role_fk == 3) {
            $query = $this->db->select('*')
                          ->from($this->table_transactions)
                          ->join('entreprise', 'entreprise.id_ent = transactions.ent_fk', 'left')
                          ->where('transactions.pays_fk', $fk_pays)
                          ->where('transactions.date_create_trans >=', $date_min)
                          ->where('transactions.date_create_trans <=', $date_max);
        }
        else
        {
              $query = $this->db->select('*')
                          ->from($this->table_transactions)
                          ->join('entreprise', 'entreprise.id_ent = transactions.ent_fk', 'left')
                          ->where('transactions.ent_fk', $fk_ent)
                          ->where('transactions.date_create_trans >=', $date_min)
                          ->where('transactions.date_create_trans <=', $date_max);
        }

        if($statut != '0')
		{
		 	$this->db->where('transactions.statut_trans', $statut);
		}

        if($provider != '0')
		{
		 	$this->db->where('transactions.provider LIKE ', $provider);
		}

		if ($type) 
        {
			if ($type == "VIE")
			{
				$this->db->where('transactions.atlantis_ref LIKE "V\_%" ');
			}
			else
			{
				$this->db->where('transactions.atlantis_ref LIKE "I\_%" ');
			}
        }

		$query = $this->db->order_by("transactions.date_create_trans","desc")
							 ->get();
			return $query->result();

	}



	public function getPrimes($mobile)
	{
		$vie = null;
		$iard = null;

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => '197.159.217.53/abi/getListePolicesVies.php',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => 'TELEASSU='.$mobile,
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/x-www-form-urlencoded'
			),
		));

		$response = json_decode(curl_exec($curl));

		curl_close($curl);

		//var_dump($response);
		//exit();

		if (isset($response) && $response->code == 1){
			//var_dump($response->data);
			$vie = $response->data->polices;
		}

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => '197.159.217.53/abi/getListePolicesNonVies.php',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => 'TELEASSU='.$mobile,
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/x-www-form-urlencoded'
			),
		));

		$response = json_decode(curl_exec($curl));

		curl_close($curl);

		//var_dump($response);
		//exit();

		if (isset($response) && $response->code == 1){
			$iard = $response->data->quittances;
		}

		return array("vie" => $vie, "iard" => $iard);

	}


	public function is_existe($atlantis_ref)
	{
		return $this->db->select("*")
			->from("transactions")
			->where('atlantis_ref', $atlantis_ref)
			->get()
			->row();

	}

	public function isCommande($atlantis_ref)
	{
		return $this->db->select("*")
						->from($this->table_transactions)
						->where('atlantis_ref', $atlantis_ref)
						->get()
						->row();

	}

	public function modifierTermesListes($atlantis_ref, $NUMEENCA, $NUMEQUIT, $MOISTERM, $DATEEFFE, $DATEECHE)
	{		 
		 return $this->db->set('NUMEENCA', $NUMEENCA)
						 ->set('NUMEQUIT', $NUMEQUIT)
						 ->set('MOISTERM',  $MOISTERM)
						 ->set('DATEEFFE', $DATEEFFE)
						 ->set('DATEECHE',  $DATEECHE)
						 ->set('date_maj_statut', date("Y-m-d H:i:s"))
						 ->where('atlantis_ref', $atlantis_ref)
						 ->update($this->table_transactions);
	}

	public function insert($montant_trans, $atlantis_ref, $ent_fk, $pays_fk, $code_police, $id_assure, $code_inter, 
	$telephone, $assure_trans, $produit_trans)
	{	
		$id = $this->session->userdata('id_user');
		$this->db->set('montant_trans', $montant_trans)
					->set('atlantis_ref', $atlantis_ref)
					->set('ent_fk', $ent_fk)
					->set('pays_fk', $pays_fk)
					->set('assure_trans', $assure_trans)
					->set('produit_trans', $produit_trans)
					->set('assure_num_polices', $code_police)
					->set('assure_code_inte', $code_inter)
					->set('assureID', $id_assure)
					->set('assureMobile', $telephone)
					->set('usersBackID', $id)
					->set('statut_trans', 'P')
					->set('date_create_trans', date("Y-m-d H:i:s"))
					->insert($this->table_transactions);
		return $this->db->insert_id();
	}


	public function majStatusEchecs($atlantis_ref)
	{
		return $this->db->set('statut_trans',  'E')
			->set('date_maj_statut', date("Y-m-d H:i:s"))
			->where('atlantis_ref', $atlantis_ref)
			->update($this->table_transactions);
	}

	public function insertTrans($NUMEAVEN, $NUMEQUIT, $nombre_choisis, $assure_code_inte, $assure_num_polices,
								$montant_trans, $atlantis_ref, $ent_fk, $pays_fk, $assure, $type_concat, $provider, $id_assure, $telephone)
	{

		if ($type_concat !== 'VIE')
		{
			$nombre_choisis = NULL;
		}
		$id = $this->session->userdata('id_user');

		$this->db->set('montant_trans', $montant_trans)
			->set('atlantis_ref', $atlantis_ref)
			->set('nombre_choisis', $nombre_choisis)
			->set('NUMEAVEN', $NUMEAVEN)
			->set('NUMEQUIT', $NUMEQUIT)
			->set('ent_fk', $ent_fk)
			->set('assure_num_polices', $assure_num_polices)
			->set('assure_code_inte', $assure_code_inte)
			->set('assureID', $id_assure)
			->set('assureMobile', $telephone)
			->set('pays_fk', $pays_fk)
			->set('usersBackID', $id)
			->set('assure_trans', $assure)
			->set('produit_trans', $type_concat)
			->set('provider', $provider)
			->set('statut_trans', 'P')
			->set('date_create_trans', date("Y-m-d H:i:s"))
			->insert($this->table_transactions);
		return $this->db->insert_id();
	}

	public function getTransByRefPaie($referencePay)
	{
		date_default_timezone_set('UTC');
		$today = date("Y-m-d H:i:s");

		return $this->db->select("*")
			->from($this->table_transactions)
			->join("pays p", "pays_fk = p.pays_id", "left")
			->join("entreprise e", "ent_fk = e.id_ent", "left")
			->where('reference_syca', $referencePay)
			->or_where('atlantis_ref', $referencePay)
			->get()
			->row();
	}

	public function modifier($reference_syca, $atlantis_ref, $mobile_trans, $statut_trans)
	{
		if ($statut_trans == '0')
		{
			$statut_trans = 'S';
		}
		elseif ($statut_trans == '-100')
		{
			$statut_trans = 'P';
		}
		else
		{
			$statut_trans = 'E';
		}

		return $this->db->set('reference_syca', $reference_syca)
						->set('mobile_trans', $mobile_trans)
						->set('statut_trans',  $statut_trans)
						->set('date_maj_statut', date("Y-m-d H:i:s"))
						->where('atlantis_ref', $atlantis_ref)
						->update($this->table_transactions);
	}

	public function getReferenceSycapay($ReferenceSycapay)
	{
		$query = $this->db->select("*")
			->from($this->table_transactions)
			->join("pays p", "pays_fk = p.pays_id", "left")
			->join("entreprise e", "ent_fk = e.id_ent", "left")
			->where('transactions.reference_syca', $ReferenceSycapay)
			->get();
		return $query->result();
	}

	public function Guid($prefix)
	{
		do {
			$lettre = strtoupper(uniqid($prefix,FALSE));
		} while ($this->is_existe($lettre));

		return $lettre;
	}

	public function getTransExport($statut, $type, $date_min, $date_max)
	{	
	  	$role_fk = $this->session->userdata('role_fk');
        $fk_ent = $this->session->userdata('fk_ent');
        $fk_pays = $this->session->userdata('fk_pays');

        if ($role_fk == 1) {
           $query =  $this->db->select('*')
                          ->from($this->table_transactions)
                          ->join('entreprise', 'entreprise.id_ent = transactions.ent_fk', 'left')
                          ->where('transactions.date_create_trans >=', $date_min)
                          ->where('transactions.date_create_trans <=', $date_max);
        }
        elseif ($role_fk == 2 OR $role_fk == 3) {
            $query = $this->db->select('*')
                          ->from($this->table_transactions)
                          ->join('entreprise', 'entreprise.id_ent = transactions.ent_fk', 'left')
                          ->where('transactions.pays_fk', $fk_pays)
                          ->where('transactions.date_create_trans >=', $date_min)
                          ->where('transactions.date_create_trans <=', $date_max);
        }
        else
        {
              $query = $this->db->select('*')
                          ->from($this->table_transactions)
                          ->join('entreprise', 'entreprise.id_ent = transactions.ent_fk', 'left')
                          ->where('transactions.ent_fk', $fk_ent)
                          ->where('transactions.date_create_trans >=', $date_min)
                          ->where('transactions.date_create_trans <=', $date_max);
        }

        if($statut != '0')
		{
		 	$this->db->where('transactions.statut_trans', $statut);
		}

		if ($type) 
        {
			if ($type == "VIE")
			{
				$this->db->where('transactions.atlantis_ref LIKE "V\_%" ');
			}
			else
			{
				$this->db->where('transactions.atlantis_ref LIKE "I\_%" ');
			}
        }
		
		$query = $this->db->order_by("transactions.date_create_trans","desc")
	                      ->get();
	        return $query->result_array();
	}

	var $column_order = array('reference_syca', 'atlantis_ref','montant_trans','mobile_trans', 'date_create_trans'); //set column field database for datatable orderable
	var $column_search = array('reference_syca', 'atlantis_ref','montant_trans','mobile_trans', 'date_create_trans');
	var $order = array('date_create_trans' => 'desc'); // default order

	private function _get_datatables_query($statut, $type, $provider, $date_min, $date_max)
	{
		$role_fk = $this->session->userdata('role_fk');
        $fk_ent = $this->session->userdata('fk_ent');
        $fk_pays = $this->session->userdata('fk_pays');

        if ($role_fk == 1) {
            $this->db->select('*')
                          ->from($this->table_transactions)
                          ->join('entreprise', 'entreprise.id_ent = transactions.ent_fk', 'left')
                          ->where('transactions.date_create_trans >=', $date_min)
                          ->where('transactions.date_create_trans <=', $date_max);
        }
        elseif ($role_fk == 3)
        {
              $this->db->select('*')
                          ->from($this->table_transactions)
                          ->join('entreprise', 'entreprise.id_ent = transactions.ent_fk', 'left')
                          ->where('transactions.ent_fk', $fk_ent)
                          ->where('transactions.date_create_trans >=', $date_min)
                          ->where('transactions.date_create_trans <=', $date_max);
        }

        if($statut != '0')
		{
		 	$this->db->where('transactions.statut_trans', $statut);
		}

        if($provider != '0')
		{
		 	$this->db->where('transactions.provider LIKE', $provider);
		}

        if ($type) 
        {
			if ($type == "VIE")
			{
				$this->db->where('transactions.atlantis_ref LIKE "V\_%" ');
			}
			else
			{
				$this->db->where('transactions.atlantis_ref LIKE "I\_%" ');
			}
        }

		$i = 0;

		foreach ($this->column_search as $item) // loop column
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{

				if($i==0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}

		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}

	}

	function get_datatables($statut, $type, $provider, $date_min, $date_max)
	{
	  $this->_get_datatables_query($statut, $type, $provider, $date_min, $date_max);
      if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
      $query = $this->db->get();

      return $query->result();
	}

	function count_filtered($statut, $type, $provider, $date_min, $date_max)
	{
		$this->_get_datatables_query($statut, $type, $provider, $date_min, $date_max);
		$query = $this->db->count_all_results();

		return $query;
	}

	public function count_all($statut, $type, $provider, $date_min, $date_max)
	{

		$role_fk = $this->session->userdata('role_fk');
        $fk_ent = $this->session->userdata('fk_ent');
        $fk_pays = $this->session->userdata('fk_pays');

        if ($role_fk == 1) {
            $this->db->select('*')
                          ->from($this->table_transactions)
                          ->join('entreprise', 'entreprise.id_ent = transactions.ent_fk', 'left');
        }
        elseif ($role_fk == 2 OR $role_fk == 3) {
             $this->db->select('*')
                          ->from($this->table_transactions)
                          ->join('entreprise', 'entreprise.id_ent = transactions.ent_fk', 'left')
                          ->where('transactions.pays_fk', $fk_pays);
        }
        else
        {
              $this->db->select('*')
                          ->from($this->table_transactions)
                          ->join('entreprise', 'entreprise.id_ent = transactions.ent_fk', 'left')
                          ->where('transactions.ent_fk', $fk_ent);
        }

		return $this->db->count_all_results();

	}


}
