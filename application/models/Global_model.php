<?php
   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
   
	class Global_model extends CI_Model {

	    protected $table_trace = "trace";
		public function __construct()
	    {
	        parent::__construct();
	    	date_default_timezone_set('UTC');
	    }

	    public function pasDeCaractersBizares($replacement)
		{ 	
			 $responseReplace = '';
			 $searchReplace  = array('*', '=', '"', ';', '#', '/', '[', ']', '~');
			 if ($replacement) 
			 {	
			 	$responseReplace = str_replace($searchReplace, array(''), $replacement);
			 }

			 return $responseReplace; 		
		}

	    public function traces($trace_action, $description, $element_fk)
		{	
			$id_user = $this->session->userdata('id_user');
			$adresse_ip = $this->recuperer_ip();
			return $this->db->set('trace_date', date("Y-m-d H:i:s"))
						    ->set('trace_adresse_ip	', $adresse_ip)
						    ->set('element_fk', $element_fk)
						    ->set('trace_action', $trace_action)
						    ->set('trace_complement', $description)
						    ->set('user_fk', $id_user)
							->insert($this->table_trace);
		}

	    private function recuperer_ip()
		{
			if (isset($_SERVER['HTTP_CLIENT_IP']))
			{
				// IP si internet partagé
				return $_SERVER['HTTP_CLIENT_IP'];
			}
			elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			{
				// IP derrière un proxy
				return $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
			else
			{
				// Sinon : IP normale
				return (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
			}
		}
	  
	}
?>