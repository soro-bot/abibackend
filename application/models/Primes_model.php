<?php
   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
   
	class Primes_model extends CI_Model {

	    protected $table_primes = "primes";

	    public function __construct()
	    {
	        parent::__construct();
	    	date_default_timezone_set('UTC');
	        setlocale(LC_TIME, 'fra_fra');
	    }
		
	    public function getPrimes()
	    {
            $query = $this->db->select('*')
			 				 ->from($this->table_primes)
			 				 ->order_by("id_prime","desc")
							 ->get();
			 return $query->result();			
	    }

	   
	    public function getActivePrimes()
	    {
            $query = $this->db->select('*')
			 				 ->from($this->table_primes)
			 				 ->where('id_prime !=', "1")
			 				 ->where('etat_prime', "A")
			 				 ->order_by("id_prime","desc")
							 ->get();
			 return $query->result();			
	    }


	    public function getOthersPrimes()
	    {
            $query = $this->db->select('*')
			 				 ->from($this->table_primes)
			 				 ->where('id_prime NOT IN (1, 2)')
			 				 ->where('etat_prime', "A")
			 				 ->order_by("id_prime","desc")
							 ->get();
			 return $query->result();
	    }


	    public function getAdminPrimes()
	    {
            $query = $this->db->select('*')
			 				 ->from($this->table_primes)
			 				 ->where('id_prime =', "2")
			 				 ->where('etat_prime', "A")
			 				 ->order_by("id_prime","desc")
							 ->get();
			 return $query->result();
	    }






	  
	}
?>
