<?php
   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
   
	class Entreprise_model extends CI_Model {

	    protected $table_entreprise = "entreprise";

	    public function __construct()
	    {
	        parent::__construct();
	    	date_default_timezone_set('UTC');
	    }
		
	    public function getEntreprises()
	    {
            $query = $this->db->select('*')
			 				 ->from($this->table_entreprise)
			 				 ->order_by("ent_date_create","desc")
							 ->get();
			 return $query->result();		
	    }

	    public function getActiveEntreprises()
	    {
            $query = $this->db->select('*')
			 				 ->from($this->table_entreprise)
			 				 ->where('ent_etat', 'A')
			 				 ->order_by("ent_date_create","desc")
							 ->get();
			 return $query->result();		
	    }

	   







	  
	}
?>