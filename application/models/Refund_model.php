<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Refund_model extends CI_Model
{
	protected $table_demande = 'demandes';
	protected $table_lot = 'lots';


	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('UTC');
	}

	public function inserer_demande($numero, $montant, $details, $beneficiaire, $sinistre, $quittance, $lot_id){
		$today = date("Y-m-d H:i:s");
		$demande = $this->db->select("*")
			->from($this->table_demande)
			->where("quittance_sinistre", $quittance)
			->get()
			->row();

		if ($demande){
			$statut_demande = "E";
			$details_traitement = "Ce sinistre a déjà été resolu";
		} else {
			$statut_demande = "P";
		}

		return $this->db->set('mobile_demande', $numero)
			->set('montant_demande', $montant)
			->set('details_demande', $details)
			//->set('type_demande', $type_demande)
			->set('beneficiaire', $beneficiaire)
			->set('numero_sinistre', $sinistre)
			->set('quittance_sinistre', $quittance)
			->set('statut_demande', $statut_demande)
			->set('details_traitement', $details_traitement)
			->set('date_demande', $today)
			->set('id_lot', $lot_id)
			->insert($this->table_demande);
	}

	public function liste_demandes($lot){
		return $this->db->select("d.*")
			->from($this->table_demande." d")
			->where("d.id_lot", $lot)
			->get()
			->result();
	}

	public function list_dem($id_lot){
		return $this->db->select("d.*")
			->from($this->table_demande." d")
			->where("d.id_lot", $id_lot)
			->get()
			->result();
	}

	public function list_dem2($id_lot){
		return $this->db->select("d.*")
			->from($this->table_demande." d")
			->where("d.statut_demande", "P")
			->where("d.id_lot", $id_lot)
			->get()
			->result();
	}

	function select($type_demande,$status)
	{
		return $this->db->select("d.*")
			->from($this->table_demande." d")
			->where("d.type_demande LIKE", $type_demande)
			->where("d.statut_demande LIKE", $status)
			->get();
	}

	function traitement($id, $statut, $details, $ref)
	{
		$today = date("Y-m-d H:i:s");

		$this->db->set('statut_demande', $statut);
		$this->db->set('reference_traitement', $ref);
		$this->db->set('details_traitement', $details);
		$this->db->set('date_traitement', $today);
		$this->db->where('id_demande', $id);
		return $this->db->update($this->table_demande);

	}

	function verification ()
	{
		$id = $this->session->userdata("id_user");
		$this->db->select("d.*")
				->from($this->table_lot." d")
				->where("d.user_id LIKE", $id)
				->get()
				->num_rows();
	}

	public function verif_conf ($lot_id)
	{
		$id = $this->session->userdata("id_user");
		$code = "*".$id."*";
		return $this->db->select("d.*")
				 ->from($this->table_lot." d")
			     ->where("d.id_lot", $lot_id)
			 	 ->like('d.validation',$code)
				 ->get()
				 ->num_rows();
	}

	function validation($type)
	{
		$query =  $this->db->select("d.validation")
						->from($this->table_lot." d")
						->where('d.id_lot', $type)
						->get()
						->result();
		if (!is_null($query) || !empty($query)) {
			foreach ($query as $row)
			{
				$tab = explode("|", $row->validation);
				if(sizeof($tab) == 0){
					$res["taille"] = 1;
				} else {
					$res["taille"] = sizeof($tab);
				}
				$res["content"] = $row->validation;
			}
		} else {
			$res["taille"] = 0;
			$res["content"] = "";
		}

		return $res;
	}

	public function inserer($lot_id, $validation , $status){
		$today = date("Y-m-d H:i:s");
		if($status == "done") {
			$query = $this->db->set('validation', $validation)
								->set('status_lot', $status)
								->set('date_valid_2', $today)
								->where('id_lot', $lot_id)
								->update($this->table_lot);
		} else {
			$query = $this->db->set('validation', $validation)
								->set('date_valid_1', $today)
								->where('id_lot', $lot_id)
								->update($this->table_lot);
		}
		return $query;
	}

	public function valider_lot_1($lot_id, $id_user){
		$today = date("Y-m-d H:i:s");
		$query = $this->db->set('validation_1', $id_user)
							->set('statut_lot', "P2")
							->set('date_valid_1', $today)
							->where('id_lot', $lot_id)
							->update($this->table_lot);

		return $query;
	}

	public function refuser_lot_1($lot_id, $id_user){
		$today = date("Y-m-d H:i:s");
		$query = $this->db->set('validation_1', $id_user)
							->set('statut_lot', "R1")
							->set('date_valid_1', $today)
							->where('id_lot', $lot_id)
							->update($this->table_lot);

		return $query;
	}

	public function valider_lot_2($lot_id, $id_user, $statut){
		$today = date("Y-m-d H:i:s");
		$query = $this->db->set('validation_2', $id_user)
							->set('statut_lot', $statut)
							->set('date_valid_2', $today)
							->where('id_lot', $lot_id)
							->update($this->table_lot);

		return $query;
	}

	public function refuser_lot_2($lot_id, $id_user){
		$today = date("Y-m-d H:i:s");
		$query = $this->db->set('validation_2', $id_user)
							->set('statut_lot', "R2")
							->set('date_valid_2', $today)
							->where('id_lot', $lot_id)
							->update($this->table_lot);

		return $query;
	}

	public function vider()
	{
		return $this->db->truncate($this->table_lot);
	}

	/*
	 * Liste des Lots à afficher pour choix
	 */
	public function liste_lots($type){
		return $this->db->select("l.*")
				->from($this->table_lot." l")
				->where("l.type_lot",$type)
				->where('l.statut_lot', "P")
				->get()
				->result();
	}

	public function get_lot_by_id($id_lot, $ent_fk, $user_id){
		return $this->db->select("l.*")
				->from($this->table_lot." l")
				->where("l.id_lot",$id_lot)
				->where("l.user_id",$user_id)
				->where("l.user_id",$user_id)
				->where("l.ent_fk",$ent_fk)
				->where('l.statut_lot', "P1")
				->get()
				->row();
	}

	public function get_lot_by_id2($id_lot, $ent_fk, $user_id){
		return $this->db->select("l.*")
				->from($this->table_lot." l")
				->where("l.id_lot",$id_lot)
				->where("l.user_id",$user_id)
				->where("l.user_id",$user_id)
				->where("l.ent_fk",$ent_fk)
				->get()
				->row();
	}

	/*
	 * Insertion da la table lot
	 */
	public function insert_lot($lot_id, $nbre, $ent_fk){
		$today = date("Y-m-d H:i:s");
		return $this->db->set('id_lot',$lot_id)
						->set('date_ajout_lot',$today)
						->set('user_id', $this->session->userdata("id_user"))
						->set('nbre_remb', $nbre)
						->set('ent_fk', $ent_fk)
						->set('statut_lot', "P1")
						->set('etat_lot', "A")
						//->set('type_lot', $type)
						->insert($this->table_lot);
	}
	public function insert_lot2($lot_id, $nbre, $ent_fk){
		$today = date("Y-m-d H:i:s");
		return $this->db->set('id_lot',$lot_id)
						->set('date_ajout_lot',$today)
						->set('user_id', $this->session->userdata("id_user"))
						->set('nbre_remb', $nbre)
						->set('ent_fk', $ent_fk)
						->set('validation_1', $this->session->userdata("id_user"))
						->set('statut_lot', "P2")
						->set('date_valid_1', $today)
						->set('etat_lot', "A")
						//->set('type_lot', $type)
						->insert($this->table_lot);
	}

	var $column_order = array('id_lot', 'nbre_remb','date_ajout_lot','type_lot', 'statut_lot'); //set column field database for datatable orderable
	var $column_search = array('id_lot', 'nbre_remb','date_ajout_lot','type_lot', 'statut_lot');
	var $order = array('date_ajout_lot' => 'desc'); // default order

	private function _get_datatables_query($statut, $date_min, $date_max)
	{
		$id_user = $this->session->userdata('id_user');
		$role_fk = $this->session->userdata('role_fk');
		$fk_ent = $this->session->userdata('fk_ent');
		$fk_pays = $this->session->userdata('fk_pays');

		if ($role_fk == "3"){

			$this->db->select("l.*")
				->from($this->table_lot." l")
				->where("l.user_id",$id_user)
				->where('l.date_ajout_lot >=', $date_min)
				->where('l.date_ajout_lot <=', $date_max)
				->where('l.ent_fk =', $fk_ent);

		} else if($role_fk == "4") {

			$this->db->select("l.*")
				->from($this->table_lot." l")
				->where("l.validation_1 IS NOT NULL")
				->where("l.date_valid_1 IS NOT NULL")
				->where('l.date_ajout_lot >=', $date_min)
				->where('l.date_ajout_lot <=', $date_max)
				->where('l.ent_fk =', $fk_ent);

		}


		if($statut != '0')
		{
			$this->db->where('l.statut_lot', $statut);
		}


		$i = 0;

		foreach ($this->column_search as $item) // loop column
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{

				if($i==0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}

		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($statut, $date_min, $date_max)
	{
		$this->_get_datatables_query($statut, $date_min, $date_max);
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($statut, $date_min, $date_max)
	{
		$this->_get_datatables_query($statut, $date_min, $date_max);
		$query = $this->db->count_all_results();
		return $query;
	}

	public function count_all($statut, $date_min, $date_max)
	{


		$this->_get_datatables_query($statut, $date_min, $date_max);

		return $this->db->count_all_results();

	}

}
