<?php
   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Auth_model extends CI_Model {

	    protected $table_user = "user";
	    protected $table_assures = 'assures';
	    protected $table_agence = "agence";
	    protected $table_affectation = "affectation";

	    public function __construct()
	    {
	        parent::__construct();
	    	date_default_timezone_set('UTC');
	    }

	    public function isIdentifier($login_user, $mot_de_passe)
	    {	
	    	$pass_user = md5($mot_de_passe); 
            return $this->db->select('*')
			 				 ->from($this->table_user)
			 				 ->join('entreprise', 'entreprise.id_ent = user.fk_ent', 'left')
			 				 ->where('user.login_user', $login_user)
			 				 ->where('user.pass_user', $pass_user)
			 				 ->where('user.user_etat', 'A')
							 ->get()
			                 ->row();
	    }

	    public function isProfil($id_user)
	    {
            return $this->db->select('*')
			 				 ->from($this->table_user)
			 				 ->join('roles', 'roles.id_role = user.role_fk', 'left')
			 				 ->where('user.id_user', $id_user)
							 ->get()
			                 ->row();
	    }

	    public function isLogin($login_user)
	    {
            return $this->db->select('*')
			 				 ->from($this->table_user)
			 				 ->where('login_user', $login_user)
			 				 ->where('user_etat', 'A')
							 ->get()
			                 ->row();
	    }

	    public function isIds($id_user)
	    {
            return $this->db->select('*')
			 				 ->from($this->table_user)
			 				 ->join('entreprise', 'entreprise.id_ent = user.fk_ent', 'left')
			 				 ->where('user.id_user', $id_user)
			 				 ->where('user.user_etat', 'A')
							 ->get()
			                 ->row();
	    }

	    public function getUserByCode($code_user)
	    {
//	    	var_dump($code_user);
            return $this->db->select('*')
			 				 ->from($this->table_user)
			 				 ->join('entreprise', 'entreprise.id_ent = user.fk_ent', 'left')
			 				 ->where('SHA1(user.code_user)', $code_user)
			 				 ->where('user.pass_user IS NULL')
			 				 ->where('user.user_etat', 'A')
							 ->get()
			                 ->row();
	    }

	    public function changerPassword($id_user, $mot_de_passe)
	    {		
		    $password = md5($mot_de_passe); 
            return $this->db->set('pass_user', $password)
						    ->where('id_user', $id_user)
							->update($this->table_user);
	    }

 		public function modifierProfil($id_user, $nom_user, $prenoms_user)
	    {		
            return $this->db->set('nom_user', $nom_user)
            				->set('prenoms_user', $prenoms_user)
						    ->where('id_user', $id_user)
							->update($this->table_user);
	    }


		public function Guid($prefix)
		{
			//do {
				$lettre = strtoupper(uniqid($prefix,FALSE));
			//} while ($this->is_exist($lettre));

			return $lettre;
		}

		public function getAssure2($mobile)
		{
				  $ret = $this->db->select('*')
								->from($this->table_assures)
								->where('mobile_assure', $mobile)
								->get()
					  			->row();
		 	    return $ret;
		}
	   
	}
?>
