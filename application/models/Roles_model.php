<?php
   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
   
	class Roles_model extends CI_Model {

	    protected $table_roles = "roles";

	    public function __construct()
	    {
	        parent::__construct();
	    	date_default_timezone_set('UTC');
	        setlocale(LC_TIME, 'fra_fra');
	    }
		
	    public function getRoles()
	    {
            $query = $this->db->select('*')
			 				 ->from($this->table_roles)
			 				 ->order_by("id_role","desc")
							 ->get();
			 return $query->result();			
	    }

	   
	    public function getActiveRoles()
	    {
            $query = $this->db->select('*')
			 				 ->from($this->table_roles)
			 				 ->where('id_role !=', "1")
			 				 ->where('etat_role', "A")
			 				 ->order_by("id_role","desc")
							 ->get();
			 return $query->result();			
	    }


	    public function getOthersRoles()
	    {
            $query = $this->db->select('*')
			 				 ->from($this->table_roles)
			 				 ->where('id_role NOT IN (1, 2)')
			 				 ->where('etat_role', "A")
			 				 ->order_by("id_role","desc")
							 ->get();
			 return $query->result();
	    }


	    public function getAdminRoles()
	    {
            $query = $this->db->select('*')
			 				 ->from($this->table_roles)
			 				 ->where('id_role =', "2")
			 				 ->where('etat_role', "A")
			 				 ->order_by("id_role","desc")
							 ->get();
			 return $query->result();
	    }






	  
	}
?>