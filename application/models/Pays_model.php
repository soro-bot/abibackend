<?php
   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
   
	class Pays_model extends CI_Model {

	    protected $table_pays = "pays";

	    public function __construct()
	    {
	        parent::__construct();
	    	date_default_timezone_set('UTC');
	    }
		
	    public function getPays()
	    {
            $query = $this->db->select('*')
			 				 ->from($this->table_pays)
			 				 ->order_by("pays_id","desc")
							 ->get();
			 return $query->result();
	    }

	    public function getActifPays()
	    {
            $query = $this->db->select('*')
			 				 ->from($this->table_pays)
			 				 ->where('pays_etat', 'A')
			 				 ->order_by("pays_id","desc")
							 ->get();
			 return $query->result();
	    }

	  
	}
?>