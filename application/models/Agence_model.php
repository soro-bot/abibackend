<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Agence_model extends CI_Model
{
	protected $table_agence = 'agence';

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("UTC");
	}

	public function getAgences()
    {	
    	$role_fk = $this->session->userdata('role_fk');
        $fk_ent = $this->session->userdata('fk_ent');
        $fk_pays = $this->session->userdata('fk_pays');

        if ($role_fk == 1) {
        	$query = $this->db->select('*')
			 				 ->from($this->table_agence)
	                         ->join('entreprise', 'entreprise.id_ent = agence.entreprise_fk', 'left')
			 				 ->order_by("agence.date_create_agence","desc")
							 ->get();
			 	return $query->result();
        }
        elseif ($role_fk == 2 OR $role_fk == 3) {
        	$query = $this->db->select('*')
	                          ->from($this->table_agence)
	                          ->join('entreprise', 'entreprise.id_ent = agence.entreprise_fk', 'left')
	                          ->where('entreprise.pays_id', $fk_pays)
				 			  ->order_by("agence.date_create_agence","desc")
							  ->get();
				 return $query->result();
        }
        else
        {		
        	$query = $this->db->select('*')
	                          ->from($this->table_agence)
	                          ->join('entreprise', 'entreprise.id_ent = agence.entreprise_fk', 'left')
	                          ->where('agence.entreprise_fk', $fk_ent)
				 			  ->order_by("agence.date_create_agence","desc")
							  ->get();
				 return $query->result();
        }

        			
    }

    public function getActiveAgences()
    {	
    	$role_fk = $this->session->userdata('role_fk');
        $fk_ent = $this->session->userdata('fk_ent');
        $fk_pays = $this->session->userdata('fk_pays');

        if ($role_fk == 1) {
        	$query = $this->db->select('*')
			 				 ->from($this->table_agence)
	                         ->join('entreprise', 'entreprise.id_ent = agence.entreprise_fk', 'left')
	                         ->where('agence.etat_agence', "A")
			 				 ->order_by("agence.date_create_agence","desc")
							 ->get();
			 	return $query->result();
        }
        elseif ($role_fk == 2 OR $role_fk == 3) {
        	$query = $this->db->select('*')
	                          ->from($this->table_agence)
	                          ->join('entreprise', 'entreprise.id_ent = agence.entreprise_fk', 'left')
	                          ->where('entreprise.pays_id', $fk_pays)
	                          ->where('agence.etat_agence', "A")
				 			  ->order_by("agence.date_create_agence","desc")
							  ->get();
				 return $query->result();
        }
        else
        {		
        	$query = $this->db->select('*')
	                          ->from($this->table_agence)
	                          ->join('entreprise', 'entreprise.id_ent = agence.entreprise_fk', 'left')
	                          ->where('agence.entreprise_fk', $fk_ent)
	                          ->where('agence.etat_agence', "A")
				 			  ->order_by("agence.date_create_agence","desc")
							  ->get();
				 return $query->result();
        }
        			
    }

    var $column_order = array('nom_agence', 'localisation_agence','date_create_agence', 'ent_raison'); 
    //set column field database for datatable orderable
	var $column_search = array('nom_agence', 'localisation_agence','date_create_agence', 'ent_raison');
	var $order = array('date_create_agence' => 'desc'); // default order

	private function _get_datatables_query()
	{
		$role_fk = $this->session->userdata('role_fk');
        $fk_ent = $this->session->userdata('fk_ent');
        $fk_pays = $this->session->userdata('fk_pays');

        if ($role_fk == 1) {
            $this->db->select('*')
                          ->from($this->table_agence)
                          ->join('entreprise', 'entreprise.id_ent = agence.entreprise_fk', 'left');
        }
        elseif ($role_fk == 2 OR $role_fk == 3) {
             $this->db->select('*')
                          ->from($this->table_agence)
                          ->join('entreprise', 'entreprise.id_ent = agence.entreprise_fk', 'left')
                          ->where('entreprise.pays_id', $fk_pays);
        }
        else
        {		
    	     $this->db->select('*')
                      ->from($this->table_agence)
                      ->join('entreprise', 'entreprise.id_ent = agence.entreprise_fk', 'left')
                      ->where('agence.entreprise_fk', $fk_ent);
        }

		$i = 0;

		foreach ($this->column_search as $item) // loop column
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{

				if($i==0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}

		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->count_all_results();
		return $query;
	}

	public function count_all()
	{

		$role_fk = $this->session->userdata('role_fk');
        $fk_ent = $this->session->userdata('fk_ent');
        $fk_pays = $this->session->userdata('fk_pays');

        if ($role_fk == 1) {
            $this->db->select('*')
                          ->from($this->table_agence)
                          ->join('entreprise', 'entreprise.id_ent = agence.entreprise_fk', 'left');
        }
        elseif ($role_fk == 2 OR $role_fk == 3) {
             $this->db->select('*')
                          ->from($this->table_agence)
                          ->join('entreprise', 'entreprise.id_ent = agence.entreprise_fk', 'left')
                          ->where('entreprise.pays_id', $fk_pays);
        }
        else
        {		
    	     $this->db->select('*')
                      ->from($this->table_agence)
                      ->join('entreprise', 'entreprise.id_ent = agence.entreprise_fk', 'left')
                      ->where('agence.entreprise_fk', $fk_ent);
        }

		return $this->db->count_all_results();

	}

	public function isIdAgences($id_agence)
	{
		return $this->db->select('*')
		 				->from($this->table_agence)
		 				->where('id_agence', $id_agence)
						->get()
			            ->row();
	}

    public function existeAgences($nom_agence, $entreprise_fk)
	{
		return $this->db->select('*')
		 				->from($this->table_agence)
		 				->where('nom_agence', $nom_agence)
		 				->where('entreprise_fk', $entreprise_fk)
						->get()
			            ->row();
	}

	public function isAgences($id_agence)
	{
		$query = $this->db->select('*')
		 				->from($this->table_agence)
		 				->where('id_agence', $id_agence)
	                    ->get();
	       return $query->result();
	}

	public function ajouterAgences($nom_agence, $localisation_agence, $entreprise_fk)
	{
         	 $this->db->set('nom_agence', $nom_agence)
			            ->set('localisation_agence', $localisation_agence)
			            ->set('etat_agence', "A")
			            ->set('date_create_agence', date("Y-m-d H:i:s"))
			            ->set('entreprise_fk', $entreprise_fk)
			            ->insert($this->table_agence);
		return $this->db->insert_id();
	}

	public function modifierAgences($id_agence, $nom_agence, $localisation_agence, $entreprise_fk)
	{
        return $this->db->set('nom_agence', $nom_agence)
			            ->set('entreprise_fk', $entreprise_fk)
			            ->set('localisation_agence', $localisation_agence)
			            ->where('id_agence', $id_agence)
			            ->update($this->table_agence);
	}

    public function activerAgences($id_agence)
	{
        return $this->db->set('etat_agence', "A")
			            ->where('id_agence', $id_agence)
			            ->update($this->table_agence);
	}

	public function desactiverAgences($id_agence)
	{
        return $this->db->set('etat_agence', "I")
			            ->where('id_agence', $id_agence)
			            ->update($this->table_agence);
	}

}