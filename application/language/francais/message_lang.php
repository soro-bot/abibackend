<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2018, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 3.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

//lie page de connexion
$lang['Accueil_title'] = 'Atlantic || Bienvenu';
$lang['Copyright'] = 'Tous droits réservés par Atlantic Business International. 
Designé et développé par';
$lang['pays'] = 'Pays';
$lang['anglais'] = 'Anglais';
$lang['francais'] = 'Français';
$lang['confirm_modal_text'] = 'Confirmez-vous cette opération ?';
$lang['confirm_modal_annuler_btn'] = 'Modifier mes données';
$lang['label_modifier_password'] = 'Modifier mon mot de passe';
$lang['notif_auth'] = 'Authentification';
$lang['notif_creation'] = 'Bienvenu sur ABI';
$lang['notif_succes'] = 'Votre opération est effectuée avec succès';

//lie page de profil
$lang['text_nom'] = 'Nom';
$lang['text_prenom'] = 'Prénom(s)';
$lang['text_privilege'] = 'Privilège';
$lang['text_prime'] = 'Palet prime';
$lang['text_create'] = 'Date création';
$lang['last_password'] = 'Ancien mot de passe';

//Liste concernant les dates
$lang['today'] = "Aujourd'hui";
$lang['thisweek'] = "En semaine";
$lang['thismonth'] = "Ce mois-ci";

//Liste concernant le dashboard
$lang['text_nbre_glob'] = 'Nombre Global';
$lang['text_chiffre_glob'] = 'Chiffre Global';
$lang['text_nbre_encaisse'] = 'Nombre encaissé';
$lang['text_chiffre_encaisse'] = 'Primes encaissées';
$lang['label_rapport_chiffre_encaisse'] = 'RAPPORTS DES PRIMES ENCAISSEES';
$lang['label_nombre_encaisse'] = 'NOMBRE DE PRIMES';
$lang['label_montant_encaisse'] = 'MONTANTS DES PRIMES';

//Liste concernant les filtre
$lang['text_periode'] = 'Période';
$lang['text_select'] = 'Faire un choix';

//Liste des bouttons
$lang['confirm_modal_valider_btn'] = 'Oui, je confirme';
$lang['valider_btn'] = 'Valider';
$lang['annuler_btn'] = 'Annuler';
$lang['connecter_btn'] = 'Connexion';
$lang['deconnecter_btn'] = 'Déconnexion';
$lang['ajouter_btn'] = 'Ajouter';
$lang['modifier_btn'] = 'Modifier';

//Menu et sidebar 
$lang['dashboard'] = "Tableau de bord";
$lang['transactions'] = "Transactions";
$lang['agences'] = 'Agences';
$lang['utilisateurs'] = 'Utilisateurs';
$lang['menu_logout'] = "Déconnexion";

//Setting langue
$lang['choix_couleur'] = "Choisir couleur";
$lang['bloquer_navbar'] = "Fixer l'entête";
$lang['bloquer_sidebar'] = "Fixer le menu";
$lang['centrer_contenu'] = "Centrer le contenu";
$lang['afficher_submenu'] = "Sous menus au survol";

//Lie au dashboard
$lang['connexion_titre'] = 'Connexion';
$lang['login_label'] = 'Email';
$lang['mdp_label'] = 'Mot de passe';
$lang['ressaisir_mdp_label'] = 'Ressaisir mot de passe';
$lang['mdp_forget'] = 'Mot de passe oublié ?';
$lang['bienvenue_text'] = 'Bienvenu';
$lang['parametres'] = 'Paramètres';
$lang['mon_compte'] = 'Mon compte';
$lang['profil_text'] = 'Mon profil';
$lang['traces_text'] = 'Mon activité';

//liste des paiement
$lang['paiments_recus_text'] = 'Nbre de paiements';
$lang['primes_encaissees_text'] = 'Primes encaissées';
$lang['label_argent'] = 'Montant';
$lang['label_echec'] = 'Echec';
$lang['label_tous'] = 'Tous';
$lang['etat_label'] = 'Etat';
$lang['label_succes'] = 'Succès';
$lang['label_pending'] = 'En attente';
$lang['label_product'] = 'Produit';
$lang['label_activer'] = 'Actif';
$lang['label_desactiver'] = 'Inactif';

//Liste des agences
$lang['agence_name'] = 'Nom Agence';
$lang['agence_local'] = 'Localisation Agence';


//Liste des entreprise
$lang['entreprise_name'] = 'Nom Entreprise';
$lang['entreprise_local'] = 'Localisation Entreprise';

//Liste des info datatable
$lang['sEmptyTable'] = 'Aucune donnée disponible dans le tableau';
$lang['sInfo'] = "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments";
$lang['sInfoEmpty'] = "Affichage de l'élément 0 à 0 sur 0 élément";
$lang['sInfoFiltered'] = '(filtré à partir de _MAX_ éléments au total)';
$lang['sLengthMenu'] = "Afficher _MENU_ éléments";
$lang['sLoadingRecords'] = 'Chargement...';
$lang['sProcessing'] = "Traitement...";
$lang['sSearch'] = 'Rechercher :';
$lang['sZeroRecords'] = "Aucun élément correspondant trouvé";
$lang['sFirst'] = 'Premier';
$lang['sNext'] = "Dernier";
$lang['sPrevious'] = 'Précédent';
$lang['sSortAscending'] = ": activer pour trier la colonne par ordre croissant";
$lang['sSortDescending'] = ': activer pour trier la colonne par ordre décroissant';
$lang['_'] = "%d lignes sélectionnées";
$lang['0'] = 'Aucune ligne sélectionnée';
$lang['1'] = "1 ligne sélectionnée";

$lang['transactions_vie'] = "Transactions VIE";
$lang['transactions_iard'] = "Transactions IARD";
$lang['vie'] = "VIE";
$lang['iard'] = "IARD";
