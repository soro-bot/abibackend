<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2018, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 3.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

//lie page de connexion
$lang['Accueil_title'] = 'Atlantic || Welcome';
$lang['Copyright'] = 'All Rights Reserved by Atlantic Business International. Designed and Developed by';
$lang['pays'] = 'State';
$lang['anglais'] = 'English';
$lang['francais'] = 'French';
$lang['confirm_modal_text'] = 'Do you confirm this operation ?';
$lang['confirm_modal_annuler_btn'] = 'Edit my data';
$lang['label_modifier_password'] = 'Edit my password';
$lang['notif_auth'] = 'Authentification';
$lang['notif_creation'] = 'Welcome on ABI';
$lang['notif_succes'] = 'Your operation was successful';

//lie page de profil
$lang['text_nom'] = 'First Name';
$lang['text_prenom'] = 'Last Name';
$lang['text_privilege'] = 'Privilege';
$lang['text_create'] = 'Create date';
$lang['last_password'] = 'Old Password';

//Listre concernant les filtre
$lang['text_periode'] = 'Period';
$lang['text_select'] = 'Select please';

//Listre concernant le dashboard
$lang['text_nbre_glob'] = 'Global Number';
$lang['text_chiffre_glob'] = 'Global Amount';
$lang['text_nbre_encaisse'] = 'Number collected';
$lang['text_chiffre_encaisse'] = 'Amount collected';
$lang['label_rapport_chiffre_encaisse'] = 'AMOUNTS COLLECTED REPORTING';
$lang['label_nombre_encaisse'] = 'NUMBER COLLECTED';
$lang['label_montant_encaisse'] = 'AMOUNT COLLECTED';

//Liste concernant les dates
$lang['today'] = "Today";
$lang['thisweek'] = "This Week";
$lang['thismonth'] = "This Month";

//Liste des bouttons
$lang['confirm_modal_valider_btn'] = 'Yes I confirm';
$lang['connecter_btn'] = 'Log In';
$lang['valider_btn'] = 'Save';
$lang['deconnecter_btn'] = 'Sign out';
$lang['annuler_btn'] = 'Cancel';
$lang['ajouter_btn'] = 'Add';
$lang['modifier_btn'] = 'Update';

//Menu et sidebar 
$lang['dashboard'] = "Dashboard";
$lang['transactions'] = "Transactions";
$lang['agences'] = 'Agences';
$lang['utilisateurs'] = 'Users';
$lang['menu_logout'] = "Logout";

//Setting langue
$lang['choix_couleur'] = "Choose Skin";
$lang['bloquer_navbar'] = "Fixed Navbar";
$lang['bloquer_sidebar'] = "Fixed Sidebar";
$lang['centrer_contenu'] = "Inside Container";
$lang['afficher_submenu'] = "Submenu on Hover";

//Lie au dashboard
$lang['connexion_titre'] = 'Sign In';
$lang['login_label'] = 'Username';
$lang['mdp_label'] = 'Password';
$lang['ressaisir_mdp_label'] = 'Retape password';
$lang['mdp_forget'] = 'Forgot your password ?';
$lang['bienvenue_text'] = 'Welcome';
$lang['parametres'] = 'Settings';
$lang['mon_compte'] = 'My account';
$lang['profil_text'] = 'My profil';
$lang['traces_text'] = 'My activity';

//Liste des paiements
$lang['paiments_recus_text'] = 'Nber of payments';
$lang['primes_encaissees_text'] = 'Amount collected';
$lang['label_argent'] = 'Amount';
$lang['label_tous'] = 'All';
$lang['etat_label'] = 'Progress';
$lang['label_echec'] = 'Failed';
$lang['label_succes'] = 'Success';
$lang['label_pending'] = 'Pending';
$lang['label_product'] = 'Product';
$lang['label_activer'] = 'Enabled';
$lang['label_desactiver'] = 'Disable';


//Liste des agences
$lang['agence_name'] = 'Agence Name';
$lang['agence_local'] = 'Agence Localisation';

//Liste des entreprise
$lang['entreprise_name'] = 'Company Name';
$lang['entreprise_local'] = 'Company Localisation';

//Liste des info datatable
$lang['sEmptyTable'] = 'No data available in table';
$lang['sInfo'] = "Showing _START_ to _END_ of _TOTAL_ entries";
$lang['sInfoEmpty'] = 'Showing 0 to 0 of 0 entries';
$lang['sInfoFiltered'] = '(filtered from _MAX_ total entries)';
$lang['sLengthMenu'] = "Show _MENU_ entries";
$lang['sLoadingRecords'] = 'Loading...';
$lang['sProcessing'] = "Processing...";
$lang['sSearch'] = 'Search :';
$lang['sZeroRecords'] = "No matching records found";
$lang['sFirst'] = 'First';
$lang['sNext'] = "Next";
$lang['sPrevious'] = 'Last';
$lang['sSortAscending'] = ": activate to sort column ascending";
$lang['sSortDescending'] = ': activate to sort column descending';
$lang['_'] = "%d selected lines";
$lang['0'] = 'No row selected';
$lang['1'] = "1 selected line";
$lang['transactions_vie'] = "LIFE transactions";
$lang['transactions_iard'] = "NON-LIFE transactions";
$lang['vie'] = "LIFE";
$lang['iard'] = "NON-LIFE";