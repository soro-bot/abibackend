<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accueil extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url', 'assets', 'cookie'));
		$this->load->library(array('form_validation', 'mailjet'));
		$this->load->model('Auth_model', 'authModel');
		$this->load->model('Global_model', 'globalModel');

		$_GET = decontaminate_data($_GET);
		$_POST = decontaminate_data($_POST);
	}

	public function index()
	{
        $login_user = $this->session->userdata('login_user');
        if ($login_user != NULL) 
        {
            $this->login();
        }
        else
        {	
        	delete_cookie("ci_session"); 
            $data['page_title'] = $this->lang->line('Accueil_title');
			$this->load->view('login', $data);
        }
	}

	public function reloadPassword()
	{	
		if (isset($_POST['envoyer']))
		{
			//on vérifie si un email correspond 
			$login = $this->input->post('login');
			$data = $this->authModel->isLogin($login);

			if ($data) 
			{	
				$id_user = $data->id_user;

		        $base_path = base_url();
		        $html_message = base_url()."assets/emails/template-2.html";
		        $message = str_replace(array("user_id", "LINK_URL", "SITE_URL"), array($id_user, $base_path, site_url()), file_get_contents($html_message));

	            $mailto = $this->mailjet->emailing($login, $this->lang->line('mdp_label'), $message);

	            if ($mailto) 
	            {
	            	$this->session->set_flashdata('success', $this->lang->line("notif_succes"));
	            } 
	            else
	            {	            	
					$this->session->set_flashdata('error', 'Mailing impossible !');
	            }

				redirect("Accueil");
			}
			else
			{		
				$this->session->set_flashdata('error', 'Authentification impossible !');
				redirect("Accueil");
			}
		}
		else
		{
			$this->session->set_flashdata('error', 'Authentification impossible !');
			redirect(site_url('Accueil'));
		}
	}

	public function truncatePassword($idUser)
	{	
		if ($idUser == NULL)
		{	
			$this->session->set_flashdata('error', 'Authentification impossible !');
			redirect(site_url('Accueil'));
		}
		else
		{			
		    $datum = $this->authModel->isIds($idUser);
			if ($datum) 
			{	
				$data['query'] = $datum;
				$data['page_title'] = $this->lang->line('Accueil_title');
				$this->load->view('password', $data);	
			}
			else
			{	
				$this->session->set_flashdata('error', 'Authentification impossible !');
				redirect(site_url('Accueil'));
			}
		}
	}

	public function definePassword($code_user)
	{
		if ($code_user == NULL)
		{
			$this->session->set_flashdata('error', 'Authentification impossible !');
			redirect(site_url('Accueil'));
		}
		else
		{
		    $datum = $this->authModel->getUserByCode($code_user);
//		    var_dump($datum);
//		    exit();
			if ($datum)
			{
				$data['query'] = $datum;
				$data['page_title'] = $this->lang->line('Accueil_title');

				$this->load->view('password', $data);
			}
			else
			{
				$this->session->set_flashdata('error', 'Authentification impossible !');
				redirect(site_url('Accueil'));
			}
		}
	}

	public function updatePassword()
	{	
		$id_user = $this->input->post('id_user');
		if (isset($_POST['envoyer']))
		{	
			if ($this->input->post('password') == $this->input->post('rpassword'))
			{	
				if (strlen($this->input->post('password')) >= 6)
				{
					$password = $this->input->post('password');
					$rpassword = $this->input->post('rpassword');	

				    $datum = $this->authModel->isIds($id_user);
					if ($datum) 
					{	
						
						$aJour = $this->authModel->changerPassword($id_user, $password);
						//$this->session->set_flashdata('success', $this->lang->line("notif_succes"));
						if ($aJour) 
			            {
			            	$this->session->set_flashdata('success', $this->lang->line("notif_succes"));
			            }
			            else
			            {	            	
							$this->session->set_flashdata('error', 'Operation impossible !');
			            }

						redirect("Accueil");
					}
					else
					{	
						$this->session->set_flashdata('error', 'Authentification impossible !');
						$data['query'] = $this->authModel->isIds($id_user);
						$data['page_title'] = $this->lang->line('Accueil_title');
						$this->load->view('password', $data);	
					}
				}
				else
				{
					$this->session->set_flashdata('error', 'Longueur mot de passe < 06 / Size password < 06 !');
					$data['query'] = $this->authModel->isIds($id_user);
					$data['page_title'] = $this->lang->line('Accueil_title');
					$this->load->view('password', $data);	
				}
			}
			else
			{
				$this->session->set_flashdata('error', 'Mot De Passe différents / Password are differents !');
				$data['query'] = $this->authModel->isIds($id_user);
				$data['page_title'] = $this->lang->line('Accueil_title');
				$this->load->view('password', $data);	
			}
		}
		else
		{	
			$this->session->set_flashdata('error', 'Authentification impossible !');

			$data['query'] = $this->authModel->isIds($id_user);
			$data['page_title'] = $this->lang->line('Accueil_title');
			$this->load->view('password', $data);	
		}
	}

	public function login()
	{  
		$login_user = $this->session->userdata('login_user');
        if ($login_user != NULL) 
        {
            $this->session->set_flashdata('success', 'Vous êtes connecté(e)s : You are connected !');
			redirect(site_url("Dashboard"));
        }
        else
        {
			$email = $this->input->post('login');
			$password = $this->input->post('password');
			$data = $this->authModel->isIdentifier($email, $password);
			//var_dump($data);
			//exit();
			if($data)
			{	
				if (!$this->session->userdata('site_lang')) {
					$this->session->set_userdata('site_lang', "francais");
				}

				$userdata = array(
				'id_user' => $data->id_user,
				'nom_user' => $data->nom_user,
				'prenoms_user' => $data->prenoms_user,
				'role_fk' => $data->role_fk,
				'login_user' => $data->login_user,
				'etat_user'  => $data->user_etat,
				'ent_raison'  => $data->ent_raison,
				'fk_ent'  => $data->fk_ent,
				'fk_pays'  => $data->pays_id,
				);
				$this->session->set_userdata($userdata);

				$libelle = "LOGIN";
	   	  		$description = "L'utilisateur se connecte";
				$this->globalModel->traces($libelle, $description, NULL);

				$this->session->set_flashdata('success', 'Vous êtes connecté(e)s : You are connected !');
				redirect(site_url("Dashboard"));

			}
			else
			{
				$this->session->set_flashdata('error', 'Authentification impossible !');
				redirect(site_url("Accueil"));
			}

        }
	}

	
}
