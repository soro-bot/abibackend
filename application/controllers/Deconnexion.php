<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Deconnexion extends CI_Controller {
	
	    function __construct()
	    {
	        //	Chargement des ressources pour tout le contrôleur
	        parent::__construct();
			$this->load->helper(array('url', 'assets', 'cookie'));
			$this->load->model('Global_model', 'globalModel');

			$_GET = decontaminate_data($_GET);
			$_POST = decontaminate_data($_POST);
	    }
	
	    function index()
	    {	
	    	//on vérifie si une session existe 
			$login_user = $this->session->userdata('login_user');
        	if ($login_user == NULL) 
			{	
				delete_cookie("ci_session");       
				$this->session->sess_destroy();
		    	redirect(base_url());
			}
			else
			{	
				delete_cookie("ci_session");       
	        	$libelle = "LOGOUT";
	   	  		$description = "L'utilisateur se déconnecte";
				$this->globalModel->traces($libelle, $description, NULL);

		    	$this->session->unset_userdata('login_user');
	        	$this->session->sess_destroy();
		    	redirect(base_url());
		    }
	    }
	}
?>
