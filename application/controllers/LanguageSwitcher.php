<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class LanguageSwitcher extends CI_Controller
{
    public function __construct() {
        parent::__construct();

		$_GET = decontaminate_data($_GET);
		$_POST = decontaminate_data($_POST);
    }

    function switchLang($language = "") {

        $language = ($language != "") ? $language : "francais";
        //var_dump($language);
        $this->session->set_userdata('site_lang', $language);

        redirect($_SERVER['HTTP_REFERER']);

    }
}
