<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
  		parent::__construct();
  		$this->load->model('Global_model', 'globalModel');
  		$this->load->model('Agence_model', 'agenceModel');
  		$this->load->model('Dashboard_model', 'dashModel');
        $this->load->model('Entreprise_model', 'entModel');

		$_GET = decontaminate_data($_GET);
		$_POST = decontaminate_data($_POST);
    }

	public function index()
	{
        $login_user = $this->session->userdata('login_user');
        if ($login_user == NULL) 
        {
            redirect(site_url("Accueil"));
        }
        else
        {	
        	  $date = new DateTime();
            $date_min = $date -> format('Y-m-01');
            $date_max = $date -> format('Y-m-d');

            $reqEntreprise = $this->entModel->getActiveEntreprises();
            $filtre_date = array('date_min' => $date_min, 'date_max' => $date_max);
            $this->session->set_userdata($filtre_date);


            $data = array('statsGlobal' => $this->dashModel->getTotalTrans(date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
                          'primeGlobal' => $this->dashModel->getTotalPrimes(date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
                          'todayGlobal' => $this->dashModel->getTotalPrimes(date("Y-m-d 00:00:00"), date("Y-m-d 23:59:59")),
                          'weekGlobal' => $this->dashModel->getTotalPrimes(date("Y-m-d 00:00:00", strtotime("last Monday")), date("Y-m-d 23:59:59")),
                          'monthGlobal' => $this->dashModel->getTotalPrimes(date("Y-m-01 00:00:00"), date("Y-m-t 23:59:59")),
                          'page_title' => $this->lang->line('Accueil_title'),
                          'liste_entreprises' => $reqEntreprise
                       );
          	$this->load->view('communs/dashboard', $data);
        }
	}

	public function rechercher()
	{
        $login_user = $this->session->userdata('login_user');
        if ($login_user == NULL) 
        {
            redirect(site_url("Accueil"));
        }
        else
        {	

        	  $date_min = $this->input->post('date_min');
            $date_max = $this->input->post('date_max');
            $entreprise = $this->input->post('entreprise');

        	  if (DateTime::createFromFormat('Y-m-d', $date_min) == FALSE OR 
                DateTime::createFromFormat('Y-m-d', $date_max) == FALSE)
            {
                // it's a date
                $this->session->set_flashdata('error_E','Echec, Format de date incorrect !'); 
                redirect(site_url('Dashboard'));
            }

            $filtre_date = array('date_min' => $date_min, 'date_max' => $date_max, 'entreprise' => $entreprise);
            $this->session->set_userdata($filtre_date);
            $reqEntreprise = $this->entModel->getActiveEntreprises();

            $data = array('statsGlobal' => $this->dashModel->getTotalTrans(date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)), $entreprise),
                          'primeGlobal' => $this->dashModel->getTotalPrimes(date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)), $entreprise),
                          'todayGlobal' => $this->dashModel->getTotalPrimes(date("Y-m-d 00:00:00"), date("Y-m-d 23:59:59"), $entreprise),
                          'weekGlobal' => $this->dashModel->getTotalPrimes(date("Y-m-d 00:00:00", strtotime("last Monday")), date("Y-m-d 23:59:59"), $entreprise),
                          'monthGlobal' => $this->dashModel->getTotalPrimes(date("Y-m-01 00:00:00"), date("Y-m-t 23:59:59"), $entreprise),
                          'page_title' => $this->lang->line('Accueil_title'),
                          'liste_entreprises' => $reqEntreprise
                        );

          	$this->load->view('communs/dashboard', $data);
        }
	}
}
