<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

	function __construct()
  {
      parent::__construct();
      $this->load->helper(array('url', 'assets'));
      $this->load->model('Auth_model', 'authModel');
      $this->load->model('Global_model','globalModel');
      $this->load->model('Trace_actions_model','tracesModel');             
      date_default_timezone_set('UTC');

	  $_GET = decontaminate_data($_GET);
	  $_POST = decontaminate_data($_POST);
  }

  public function index()
  {    
      //on vérifie si une session existe 
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL) 
      {
          redirect(site_url("Accueil"));
      }
      else
      {     
            $id_user = $this->session->userdata('id_user'); 
            $reqProfil = $this->authModel->isProfil($id_user);
            $data = array('myPerso' => $reqProfil,
                          'page_title' => $this->lang->line('Accueil_title')
                        );
            $this->load->view('communs/profil', $data);
      }
  }

  public function updatePassword()
  { 
      //on vérifie si une session existe 
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL) 
      {
          redirect(site_url("Accueil"));
      }
      else
      {   

          $id_user = $this->input->post('id_user');
          $data = $this->authModel->isIdentifier($login_user, $this->input->post('lastPassword'));
          if(empty($data))
          {
              $this->session->set_flashdata('error', 'Ancien Mot De Passe incorrect / Bad Old Password !');
              redirect("Profil");
          }

          if ($this->input->post('password') == $this->input->post('rpassword'))
          { 
              if (strlen($this->input->post('password')) >= 6)
              {
                    $password = $this->input->post('password');
                    $rpassword = $this->input->post('rpassword'); 

                    $datum = $this->authModel->isIds($id_user);
                    if ($datum) 
                    {   
                        $this->session->set_flashdata('success', $this->lang->line("notif_succes"));
                        $aJour = $this->authModel->changerPassword($id_user, $password);
                        if ($aJour) 
                        {
                            $libelle = "MODIFIER";
                            $description = "L'utilisateur modifie son password";
                            $this->globalModel->traces($libelle, $description, $id_user);
                        }
                        else
                        {               
                            $this->session->set_flashdata('error', 'Operation impossible !');
                        }
                        redirect("Profil");
                    }
                    else
                    { 
                        $this->session->set_flashdata('error', 'Authentification impossible !');
                        redirect("Profil");
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', 'Longueur mot de passe < 06 / Size password < 06 !');
                    redirect("Profil");
                }
          }
          else
          {
              $this->session->set_flashdata('error', 'Mot De Passe différents / Password are differents !');
              redirect("Profil");
          }
    }
}

  public function updateProfil()
  {    
      //on vérifie si une session existe 
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL) 
      {
          redirect(site_url("Accueil"));
      }
      else
      {
          $id_user = $this->input->post('id_user');
          $nom_user = $this->globalModel->pasDeCaractersBizares($this->input->post('nom_user'));
          $prenoms_user = $this->globalModel->pasDeCaractersBizares($this->input->post('prenoms_user'));
          
          $getProfil = $this->authModel->isProfil($id_user);

          if (empty($nom_user) OR empty($prenoms_user) OR empty($id_user)) 
          {
              $this->session->set_flashdata('error', 'Les champs sont requis / Fields are required !');
              redirect(site_url('Profil'));
          }
          
          if ($getProfil)
          {     
              $addIng = $this->authModel->modifierProfil($id_user, $nom_user, $prenoms_user);

              if ($addIng)
              {   
                  $this->session->set_flashdata('success', $this->lang->line("notif_succes"));
                  $libelle = "MODIFIER";
                  $description = "L'utilisateur modifie son profil";
                  $this->globalModel->traces($libelle, $description, $id_user);
              }
              else
              {                                          
                  $this->session->set_flashdata('error', 'Operation impossible !'); 
              } 

              redirect('Profil');              
          }
          else
          {
              $this->session->set_flashdata('error', 'Authentification impossible !');
              redirect('Profil'); 
          }   
      }
  }

  public function ajax_list()
  {        
      $date = new DateTime();
      $date_min = $date -> format('Y-m-01');
      $date_max = $date -> format('Y-m-d');

      $utilisateurs = $this->session->userdata('id_user'); 
      $list = $this->tracesModel->get_datatables($utilisateurs, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));

       $data = array();
       $no = $_POST['start'];
       foreach ($list as $trans) {
        $no++;
        $row = array();

        $row[] = $trans->trace_action;
        $row[] = $trans->nom_user.' '.$trans->prenoms_user;
        $row[] = $trans->trace_complement;
        $row[] = $trans->trace_date;

        $data[] = $row;
      }

       $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->tracesModel->count_all($utilisateurs, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
          "recordsFiltered" => $this->tracesModel->count_filtered($utilisateurs, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
          "data" => $data,
          );
  
      //output to json format
      echo json_encode($output);

  }

}
