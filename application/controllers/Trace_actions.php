<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trace_actions extends CI_Controller {

  function __construct()
  {
      parent::__construct();
      $this->load->helper(array('url', 'assets'));
      $this->load->model('Trace_actions_model','tracesModel');
      date_default_timezone_set('UTC');

	  $_GET = decontaminate_data($_GET);
	  $_POST = decontaminate_data($_POST);
  }

  public function index()
  {    
    //on vérifie si une session existe 
    $login_user = $this->session->userdata('login_user');
    if ($login_user == NULL) 
    {
        redirect(site_url("Accueil"));
    }
    else
    {   
        $date = new DateTime();
        $date_min = $date -> format('Y-m-01');
        $date_max = $date -> format('Y-m-t');
        $utilisateurs = '0';

        $filtre_date = array('date_min' => $date_min, 'date_max' => $date_max, 'utilisateurs' => $utilisateurs);
        $this->session->set_userdata($filtre_date);
        
        $query = $this->tracesModel->getActions($utilisateurs, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));
        $data = array('affiche_traces' => $query,
                      'page_title' => $this->lang->line('Accueil_title')
                     );
        $this->load->view('communs/trace_actions', $data);
    }
 }


  public function rechercher_actions()
  {    
    //on vérifie si une session existe 
    $email_user = $this->session->userdata('email_user');
    if ($email_user == NULL)
    {
      redirect(site_url('Accueil'));
    }
    else
    {
          $date_min = $this->input->post('date_min');
          $date_max = $this->input->post('date_max');
          $utilisateurs = '0';
          $search = 'search';

          if (DateTime::createFromFormat('Y-m-d', $date_min) == FALSE OR 
              DateTime::createFromFormat('Y-m-d', $date_max) == FALSE)
          {
              // it's a date
              $this->session->set_flashdata('error_E','Echec, Format de date incorrect !'); 
              redirect(site_url('Trace_actions'));
          }
          else
          {   

            $filtre_date = array('search' => $search, 'date_min' => $date_min, 'date_max' => $date_max, 'utilisateurs' => $utilisateurs);
            $this->session->set_userdata($filtre_date);

            $query = $this->tracesModel->getActions($utilisateurs, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));

            $data = array('affiche_traces' => $query,
                          'page_title' => $this->lang->line('Accueil_title')
                         );
              $this->load->view('communs/trace_actions', $data);
          }
        
      }
  }

  public function ajax_list()
  {        
      $search = $this->session->userdata('search');
      if (isset($search))
      {
           $utilisateurs = $this->session->userdata('utilisateurs');
           $date_min = $this->session->userdata('date_min');
           $date_max = $this->session->userdata('date_max');

           $list = $this->tracesModel->get_datatables($utilisateurs, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));

           $data = array();
           $no = $_POST['start'];
           foreach ($list as $trans) {
            $no++;
            $row = array();

            $row[] = $trans->trace_action;
            $row[] = $trans->nom_user.' '.$trans->prenoms_user;
            $row[] = $trans->trace_complement;
            $row[] = $trans->trace_date;

            $data[] = $row;
          }

           $output = array(
              "draw" => $_POST['draw'],
              "recordsTotal" => $this->tracesModel->count_all($utilisateurs, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
              "recordsFiltered" => $this->tracesModel->count_filtered($utilisateurs, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
              "data" => $data,
              );
      
          //output to json format
          echo json_encode($output);

      }
      else
      {

          $date = new DateTime();
          $date_min = $date -> format('Y-m-01');
          $date_max = $date -> format('Y-m-t');
          $utilisateurs = '0';

          $filtre_date = array('date_min' => $date_min, 'date_max' => $date_max, 'utilisateurs' => $utilisateurs);
          $this->session->set_userdata($filtre_date);

          $list = $this->tracesModel->get_datatables($utilisateurs, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));

           $data = array();
           $no = $_POST['start'];
           foreach ($list as $trans) {
            $no++;
            $row = array();
            
            $row[] = $trans->trace_action;
            $row[] = $trans->nom_user.' '.$trans->prenoms_user;
            $row[] = $trans->trace_complement;
            $row[] = $trans->trace_date;

            $data[] = $row;
          }

           $output = array(
              "draw" => $_POST['draw'],
              "recordsTotal" => $this->tracesModel->count_all($utilisateurs, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
              "recordsFiltered" => $this->tracesModel->count_filtered($utilisateurs, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
              "data" => $data,
              );
      
          //output to json format
          echo json_encode($output);

      }
  }




}
