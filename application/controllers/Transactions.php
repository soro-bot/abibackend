<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions extends CI_Controller {

	public function __construct()
	{
  		parent::__construct();
  		$this->load->library(array('Excel'));
  		$this->load->model('Global_model', 'globalModel');
        $this->load->model('Auth_model', 'authModel');
  		$this->load->model('Agence_model', 'agenceModel');
  		$this->load->model('Transactions_model', 'transModel');
        $this->load->model('Entreprise_model', 'entModel');

		$_GET = decontaminate_data($_GET);
		$_POST = decontaminate_data($_POST);
    }


    public function index()
    {
        $login_user = $this->session->userdata('login_user');
        if ($login_user == NULL)
        {
            redirect(site_url("Accueil"));
        }
        else
        {
            $date = new DateTime();
            $date_min = $date -> format('Y-m-01');
            $date_max = $date -> format('Y-m-d');
            $statut = '0';
            $type = '0';
            $provider = '0';

            $filtre_date = array('date_min' => $date_min, 'date_max' => $date_max, 'statut' => $statut, 'provider' => $provider);
            $this->session->set_userdata($filtre_date);

            $query = $this->transModel->getTransactions($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));

            $data = array('affiche_trans' => $query,
                'page_title' => $this->lang->line('Accueil_title')
            );
            $data['page'] = "transactions";

            $this->load->view('communs/transactions', $data);
        }
    }

    public function initier_transaction()
    {
        $login_user = $this->session->userdata('login_user');
        if ($login_user == NULL)
        {
            redirect(site_url("Accueil"));
        }
        else
        {
            $query = $this->entModel->getActiveEntreprises();
            //var_dump($query);
            $data = array('affiche_agence' => "",
                'modifier_agence' => "",
                'liste_entreprises' => $query,
                'ajouter_agence' => "ajouter_agence",
                'page_title' => $this->lang->line('Accueil_title')
            );


            $this->load->view('communs/initier_vie', $data);
        }
    }

	public function vie()
	{
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL)
      {
          redirect(site_url("Accueil"));
      }
      else
      {
        	  $date = new DateTime();
            $date_min = $date -> format('Y-m-01');
            $date_max = $date -> format('Y-m-d');
            $statut = '0';
            $type = 'VIE';
            $provider = '0';
            $this->session->set_userdata("type",$type);

            $filtre_date = array('date_min' => $date_min, 'date_max' => $date_max, 'statut' => $statut, 'provider' => $provider);
            $this->session->set_userdata($filtre_date);

            $query = $this->transModel->getTransactions($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));

            $data = array('affiche_trans' => $query,
                          'page_title' => $this->lang->line('Accueil_title')
                       );
          $data['page'] = "transactions_vie";

          $this->load->view('communs/transactions_vie', $data);
        }
	}

	public function iard()
	{
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL)
      {
          redirect(site_url("Accueil"));
      }
      else
      {
        	  $date = new DateTime();
            $date_min = $date -> format('Y-m-01');
            $date_max = $date -> format('Y-m-d');
            $statut = '0';
            $type = 'IARD';
		  $provider = $this->input->post('provider');
		  $this->session->set_userdata("type",$type);

            $filtre_date = array('date_min' => $date_min, 'date_max' => $date_max, 'statut' => $statut);
            $this->session->set_userdata($filtre_date);

            $query = $this->transModel->getTransactions($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));

            $data = array('affiche_trans' => $query,
                          'page_title' => $this->lang->line('Accueil_title')
                       );
          $data['page'] = "transactions_iard";

          $this->load->view('communs/transactions_iard', $data);
        }
	}

  public function exportFormat()
  {    
      //on vérifie si une session existe 
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL) 
      {
          redirect(site_url("Accueil"));
      }
      else
      {
          $this->load->library('PHPExcel');
          $statut = $this->session->userdata('statut');
          $date_min = $this->session->userdata('date_min');
          $date_max = $this->session->userdata('date_max');
          $type = $this->session->userdata("type");


          $libelle = "EXPORT";
          $description = "L'utilisateur exporte un fichier des transactions";
          $this->globalModel->traces($libelle, $description, NULL);

            // create file name
            $fileName = 'ABI-'.time();  
            // load excel library
            $empInfo = $this->transModel->getTransExport($statut, $type, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));

            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            // set Header
            $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'SycaReference');
            $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'AbiReferrence');
            $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Montant');
            $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Mobile');
            $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Status');
            $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'DateTransactions');       
            // set Row      
            // set Row
            $rowCount = 2;
            foreach ($empInfo as $element) {
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['reference_syca']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['atlantis_ref']);

                $transaction_montant = number_format($element['montant_trans'], 0, '.', ' ');
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $transaction_montant);

                if ($element['statut_trans'] == 'S')
                {
                   $statut_trans =  "Succès";
                }
                elseif($element['statut_trans'] == 'P')
                { 
                   $statut_trans =  "En attente";
                }
                else
                {
                   $statut_trans =  "Echec";
                }

                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['mobile_trans']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $statut_trans);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['date_create_trans']);
                $rowCount++;
            }

            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'. $fileName .'.xlsx"'); 
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
      }

  }


	public function rechercher()
	{
        $login_user = $this->session->userdata('login_user');
        if ($login_user == NULL) 
        {
            redirect(site_url("Accueil"));
        }
        else
        {	

        	$date_min = $this->input->post('date_min');
            $date_max = $this->input->post('date_max');
            $statut = $this->input->post('statut');
            $search = 'search';
            //$type = $this->session->userdata("type");
            $type = $this->input->post('type');
            $provider = $this->input->post('provider');

            if (DateTime::createFromFormat('Y-m-d', $date_min) == FALSE OR
                DateTime::createFromFormat('Y-m-d', $date_max) == FALSE)
            {
                // it's a date
                $this->session->set_flashdata('error_E','Echec, Format de date incorrect !'); 
                redirect(site_url('Transactions'));
            }

            $filtre_date = array('type' => $type, 'date_min' => $date_min, 'date_max' => $date_max, 'search' => $search, 'statut' => $statut, 'provider' => $provider);
            $this->session->set_userdata($filtre_date);

            $query = $this->transModel->getTransactions($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));

            $data = array('affiche_trans' => $query,
                          'page_title' => $this->lang->line('Accueil_title')
                       );

            if ($type == "VIE"){
                $data['page'] = "transactions_vie";
                $this->load->view('communs/transactions_vie', $data);
            }
            elseif ($type == "IARD") 
            {
                $data['page'] = "transactions_iard";
                $this->load->view('communs/transactions_iard', $data);
            }
            else
            {
                $data['page'] = "transactions";
                $this->load->view('communs/transactions', $data);
            }

        }
	}


    public function get_primes()
    {   
        header("Access-Control-Allow-Origin: *");
        $login_user = $this->session->userdata('login_user');
        if ($login_user == NULL) 
        {
            redirect(site_url("Accueil"));
        }
        else
        {   
            $id = NULL;
            $mobile = $this->input->post('mobile');
            $data = $this->authModel->getAssure2($mobile);
            if ($data) {
               $id = $data->id_assure;
            }
            $query = $this->transModel->getPrimes($mobile);

			if ($query["vie"] == null && $query["iard"] == null){
				echo json_encode(array(
					"status" => false,
					"message" => "Primes non récupérées"
				));
			} else {
				echo json_encode(array(
					"status" => true,
					'message' => "Primes récupérées",
					"assure" => $data,
					"vie" => $query["vie"],
					"iard" => $query["iard"]
				));
			}

        }
    }



    public function getTokens()
    {
        header("Access-Control-Allow-Origin: *");
        $login_user = $this->session->userdata('login_user');
        if ($login_user == NULL) 
        {
            redirect(site_url("Accueil"));
        }
        else
        {
            $chiffre = $this->input->post('chiffre');
            $type = $this->input->post('type');

            $code_prime = $this->input->post('code_prime');
            $id_assure = $this->input->post('id_assure');

            $code_inter = $this->input->post('code_inter');
            $telephone = $this->input->post('telephone');
            $label = $this->input->post('label');

            $paramtoken = array(
                "montant" => $chiffre,
                "curr" =>"XOF"
            );

            $headers = array (
                'X-SYCA-MERCHANDID: C_6033A4C37BA52',
                'X-SYCA-APIKEY: pk_syca_aaa56629b509320441d3483a04147af521cc7978',
                'X-SYCA-REQUEST-DATA-FORMAT: JSON',
                'X-SYCA-RESPONSE-DATA-FORMAT: JSON'
            );

            //$url = "https://secure.sycapay.com/login";
            $url = "https://secure.sycapay.com/login";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0 );
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $data_string = json_encode($paramtoken);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            $response = json_decode(curl_exec($ch),TRUE);
            curl_close($ch);
            if($response['code'] == 0)
            {
                $token = $response['token'];
            }

            if ($token)
            {
                $prefix = "";
                if ($type == "VIE"){
                    $prefix = "V_";
                } else if ($type == "IARD"){
                    $prefix = "I_";
                } else {
                    exit("");
                }

                $commande = $this->transModel->Guid($prefix);

                $ent_id = $this->session->userdata("fk_ent");
                $pays_id = $this->session->userdata("fk_pays");

                $id = $this->transModel->insert($chiffre, $commande, $ent_id, $pays_id, $code_prime, $id_assure,
                 $code_inter, $telephone, $label, $type);

                echo json_encode(array(
                    "token" => $token,
                    "commande" => $commande
                ));
 
            }
            else
            {
                echo "";
            }
        }

    }

	public function getTokens2()
	{
		header("Access-Control-Allow-Origin: *");
		$chiffre = $this->input->post('chiffre');
		$types = $this->input->post('types');
		$nb_primes = intval($this->input->post('nb_primes'));

		$types = explode("|", $types);

		//var_dump($types);

		$paramtoken = array(
			"montant" => str_replace(" ", "", $chiffre),
			//"montant" => 5,
			"currency" =>"XOF"
		);

		$headers = array (
			'X-SYCA-MERCHANDID: C_6033A4C37BA52',
			'X-SYCA-APIKEY: pk_syca_aaa56629b509320441d3483a04147af521cc7978',
			'X-SYCA-REQUEST-DATA-FORMAT: JSON',
			'X-SYCA-RESPONSE-DATA-FORMAT: JSON'
		);

		//$url = "https://dev.sycapay.net/api/login.php";
		$url = "https://dev.sycapay.com/login.php";
		//$url = "https://secure.sycapay.net/login";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0 );
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$data_string = json_encode($paramtoken);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		$response = json_decode(curl_exec($ch),TRUE);
		curl_close($ch);

		if($response['code'] == 0)
		{
			$token = $response['token'];
		}

		if ($token)
		{
			$commandes = "";
			for($i = 0; $i < $nb_primes ; $i++){
				$prefix = "";
				if ($types[$i] == "VIE"){
					$prefix = "V_";
				} else if ($types[$i] == "IARD"){
					$prefix = "I_";
				} else {
					exit("");
				}

				$commande = $this->authModel->Guid($prefix);

				if ($i > 0){
					$commandes .= "|";
				}

				$commandes .= $commande;
			}
			//echo $response->pinId;
			echo json_encode(array(
				"token" => $token,
				"commande" => $commandes
			));
		}
		else
		{
			echo "";
		}

	}

	public function paiement()
	{
		header("Access-Control-Allow-Origin: *");
		set_time_limit(300);
		$pays_id = $this->session->userdata('pays_id');

		$chiffre = 0;
		$commande = $this->input->post('commande');
		$montant = $this->input->post('montant');
		$id_assure = $this->input->post('id_assure');
		$telephone = $this->input->post('telephone');
		$assure = $this->input->post('assure');
		$type_concat = $this->input->post('type_concat');

		$code_int_concat = $this->input->post('code_int_concat');
		$num_police_concat = $this->input->post('num_police_concat');
		$nombre_mois_concat = $this->input->post('nombre_mois_concat');

		$num_avenant_concat = $this->input->post('num_avenant_concat');
		$num_quittance_concat = $this->input->post('num_quittance_concat');
		$provider = $this->input->post('provider');

		$commandes = explode("|", $commande);
		$montants = explode("|", $montant);
		$type_concats = explode("|", $type_concat);

		$code_int_concats = explode("|", $code_int_concat);
		$num_police_concats = explode("|", $num_police_concat);
		$nombre_mois_concats = explode("|", $nombre_mois_concat);

		$num_avenant_concats = explode("|", $num_avenant_concat);
		$num_quittance_concats = explode("|", $num_quittance_concat);

		$query = true;
		if ($query)
		{
			$id = false;
			foreach($commandes as $key => $com){

				$ent_id = $this->session->userdata("fk_ent");
				$pays_id = $this->session->userdata("fk_pays");


				$chiffre += $montants[$key];
				$id = $this->transModel->insertTrans($num_avenant_concats[$key], $num_quittance_concats[$key],
					$nombre_mois_concats[$key], $code_int_concats[$key], $num_police_concats[$key], $montants[$key], $com,
					$ent_id, $pays_id, $assure, $type_concats[$key], $provider, $id_assure, $telephone);

			}

			if ($id)
			{
				$paramtoken = array(
					"montant" => str_replace(" ", "", $chiffre),
					//"montant" => 5,
					"currency" =>"XOF"
				);

				$headers = array (
					'X-SYCA-MERCHANDID: C_6033A4C37BA52',
					'X-SYCA-APIKEY: pk_syca_aaa56629b509320441d3483a04147af521cc7978',
					'X-SYCA-REQUEST-DATA-FORMAT: JSON',
					'X-SYCA-RESPONSE-DATA-FORMAT: JSON'
				);
				$url = "https://secure.sycapay.com/login.php";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,$url);
				curl_setopt($ch, CURLOPT_VERBOSE, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0 );
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				$data_string = json_encode($paramtoken);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				$response = json_decode(curl_exec($ch),TRUE);
				curl_close($ch);
				if($response['code'] == 0)
				{
					$token = $response['token'];
				}

				$mobileSend = $this->input->post('mobile');
				if (substr($mobileSend, 0, 2) == '07')
				{
					//var_dump($mobileSend);
					$OTP = $this->input->post('OTP');
					$paramsend = array(
						"montant" => $chiffre,
						//"montant" => 10,
						"telephone" => $mobileSend,
						"name" => $this->session->userdata('nom_assure'),
						"pname" => $this->session->userdata('prenoms_assure'),
						"marchandid" => "C_6033A4C37BA52",
						"urlnotif" => site_url('Accueil/getRetoursPaiements'),
						"numcommande" => $this->input->post('commande'),
						//"token" => $this->input->post('token'),
						"token" => $token,
						"currency" =>"XOF",
						"otp" => $OTP,
					);

					$messageToRead = "Patientez SVP, vous serez notifié dans un instant !";

				}
				else
				{
					$paramsend = array(
						"montant" => $chiffre,
						//"montant" => 5,
						"telephone" => $mobileSend,
						"name" => $this->session->userdata('nom_assure'),
						"pname" => $this->session->userdata('prenoms_assure'),
						"marchandid" => "C_6033A4C37BA52",
						"urlnotif" => site_url('Accueil/getRetoursPaiements'),
						"numcommande" => $this->input->post('commande'),
						"token" => $token,
						"currency" =>"XOF",
					);

					$messageToRead = "Paiement en cours, consulter votre téléphone pour valider SVP !";
				}

				//$url = "https://dev.sycapay.net/api/checkoutpay.php";
				$url = "https://dev.sycapay.com/checkoutpay.php";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,$url);
				curl_setopt($ch, CURLOPT_VERBOSE, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json" ));
				$data_string = json_encode($paramsend);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				$response = json_decode(curl_exec($ch),TRUE);
				curl_close($ch);
				//var_dump($response);
				if($response['code'] == 0)
				{
					$codeSycapay = $response['transactionId'];
					$retourResponse = array('code' => $response['code'], 'message' => $messageToRead, 'transactionId' => $codeSycapay);
					echo json_encode($retourResponse);
				}
				elseif($response['code'] == -5)
				{
					$retourResponse = array('code' => $response['code'], 'message' => "Paiement echoué, le code saisi est incorrect");
					echo json_encode($retourResponse);
				}
				elseif($response['code'] == -3)
				{
					$retourResponse = array('code' => $response['code'], 'message' => "Paiement echoué, votre solde est insuffisant");
					echo json_encode($retourResponse);
				}
				else
				{
					echo json_encode(array('code' => $response['code'], 'message' => "Paiement echoué pour diverses raisons, veuillez réessayer!"));
				}
			}
			else
			{
				echo " ";
			}
		}
		else
		{
			echo "";
		}

	}

	public function paiement_cb()
	{
		header("Access-Control-Allow-Origin: *");
		set_time_limit(300);

		$chiffre = 0;
		$commande = $this->input->post('commande');
		$montant = $this->input->post('montant');
		$id_assure = $this->input->post('id_assure');
		$telephone = $this->input->post('telephone');
		$assure = $this->input->post('assure');
		$type_concat = $this->input->post('type_concat');

		$code_int_concat = $this->input->post('code_int_concat');
		$num_police_concat = $this->input->post('num_police_concat');
		$nombre_mois_concat = $this->input->post('nombre_mois_concat');

		$num_avenant_concat = $this->input->post('num_avenant_concat');
		$num_quittance_concat = $this->input->post('num_quittance_concat');
		$provider = $this->input->post('provider');

		$commandes = explode("|", $commande);
		$montants = explode("|", $montant);
		$type_concats = explode("|", $type_concat);

		$code_int_concats = explode("|", $code_int_concat);
		$num_police_concats = explode("|", $num_police_concat);
		$nombre_mois_concats = explode("|", $nombre_mois_concat);

		$num_avenant_concats = explode("|", $num_avenant_concat);
		$num_quittance_concats = explode("|", $num_quittance_concat);

		$query = true;
		if ($query)
		{
			$id = false;
			foreach($commandes as $key => $com){


				$ent_id = $this->session->userdata("fk_ent");
				$pays_id = $this->session->userdata("fk_pays");


				$chiffre += $montants[$key];
				$id = $this->transModel->insertTrans($num_avenant_concats[$key], $num_quittance_concats[$key],
					$nombre_mois_concats[$key], $code_int_concats[$key], $num_police_concats[$key], $montants[$key], $com,
					$ent_id, $pays_id, $assure, $type_concats[$key], $provider, $id_assure, $telephone);

			}

			if ($id)
			{

				echo true;

			}
			else
			{
				echo " ";
			}
		}
		else
		{
			echo "";
		}

	}

	public function GetStatus()
	{
		header("Access-Control-Allow-Origin: *");
		$ref = $this->input->post('code');

		$paramsend = array("ref" => $ref);
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://dev.sycapay.com/GetStatus.php',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => json_encode($paramsend),
			CURLOPT_HTTPHEADER => array("Content-Type: application/json" ),
		));

		$response = json_decode(curl_exec($curl));
		$err = curl_error($curl);
		curl_close($curl);

		if ($response)
		{
			if ($response->code == -200)
			{
				echo 'PE';
			}
			elseif ($response->code == 0)
			{
				$purchaseinfo = $response->orderId;
				$montant = $response->montant;
				$status = $response->code;
				$reference = $response->transactionID;
				$contact = $response->mobile;
				$operatorTrans = $response->operator;
				$policesConcatees = "";
				$moisTermes = "";
				$numeEncais = "";

				if ($operatorTrans == "MarterCardCI"){
					$operatorTrans = "MASTERCARD";
				} elseif ($operatorTrans == "EcobCI"){
					$operatorTrans = "VISA";
				}

				if ($purchaseinfo)
				{
					$commandes = explode("|", $purchaseinfo);
					$id = false;
					$nbre_mois = 0;
					foreach($commandes as $com)
					{
						$nbre_mois = $nbre_mois + 1;
						$id = $this->transModel->modifier($reference, $com, $contact, $status, $operatorTrans);
						if (isset($status))
						{
							if ($status == '0')
							{
								$getTrans = $this->transModel->getTransByRefPaie($com);
								if ($getTrans)
								{
									//Pour la génération du pdf
									$produit = $getTrans->produit_trans;
									$assure = $getTrans->assure_trans;
									$societe = $getTrans->ent_raison;
									$datepaie = $getTrans->date_create_trans;

									if ($nbre_mois > 1){
										$policesConcatees .= "|";
										$moisTermes .= "|";
										$numeEncais .= "|";
									}

									$policesConcatees .= $getTrans->assure_num_polices;
									$MOBILEMONEY = strtoupper(str_replace(array("CI", " ", "TG"), "", $operatorTrans));
									$TELEASSU = '+'.$getTrans->assureMobile;

									if ($getTrans->produit_trans == 'VIE')
									{
										$curl = curl_init();
										curl_setopt_array($curl, array(
											CURLOPT_URL => '197.159.217.53/abi/getListeTerme.php',
											CURLOPT_RETURNTRANSFER => true,
											CURLOPT_ENCODING => '',
											CURLOPT_MAXREDIRS => 10,
											CURLOPT_TIMEOUT => 0,
											CURLOPT_FOLLOWLOCATION => true,
											CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
											CURLOPT_CUSTOMREQUEST => 'POST',
											CURLOPT_POSTFIELDS => 'TELEASSU='.$TELEASSU.'&CODEINTE='.$getTrans->assure_code_inte.'&NUMEPOLI='.$getTrans->assure_num_polices.'&NOMBPERI='.$getTrans->nombre_choisis.'&REFEENCA='.$com.'&MONTENCA='.$getTrans->montant_trans.'&MOBILEMONEY='.$MOBILEMONEY,
											CURLOPT_HTTPHEADER => array(
												'Content-Type: application/x-www-form-urlencoded'
											),
										));

										$response = json_decode(curl_exec($curl), true);
										//var_dump($response);
										//var_dump('TELEASSU='.$TELEASSU.'&CODEINTE='.$getTrans->assure_code_inte.'&NUMEPOLI='.$getTrans->assure_num_polices.'&NOMBPERI='.$getTrans->nombre_choisis.'&REFEENCA='.$com.'&MONTENCA='.$getTrans->montant_trans.'&MOBILEMONEY='.$MOBILEMONEY);
										curl_close($curl);
										if (!isset($response['error']))
										{
											if (isset($response['data']))
											{
												$quittances = $response['data']['quittances'];

												if ($quittances)
												{
													foreach ($quittances as $gets)
													{
														$NUMEENCA = $gets['NUMEENCA'];
														$NUMEQUIT = $gets['NUMEQUIT'];
														$MOISTERM = $gets['MOISTERM'];
														$DATEEFFE = $gets['DATEEFFE'];
														$DATEECHE = $gets['DATEECHE'];
													}

													$moisTermes .= $MOISTERM;
													$numeEncais .= $NUMEENCA;

													$this->transModel->modifierTermesListes($com, $NUMEENCA,
														$NUMEQUIT, $MOISTERM, $DATEEFFE, $DATEECHE);
												}

											}

										}
									}
									else
									{
										$curl = curl_init();
										curl_setopt_array($curl, array(
											CURLOPT_URL => '197.159.217.53/abi/getListeQuittancesDirectes.php',
											CURLOPT_RETURNTRANSFER => true,
											CURLOPT_ENCODING => '',
											CURLOPT_MAXREDIRS => 10,
											CURLOPT_TIMEOUT => 0,
											CURLOPT_FOLLOWLOCATION => true,
											CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
											CURLOPT_CUSTOMREQUEST => 'POST',
											CURLOPT_POSTFIELDS => 'TELEASSU='.$TELEASSU.'&CODEINTE='.$getTrans->assure_code_inte.'&NUMEPOLI='.$getTrans->assure_num_polices.'&REFEENCA='.$com.'&MONTENCA='.$getTrans->montant_trans.'&MOBILEMONEY='.$MOBILEMONEY.'&NUMEQUIT='.$getTrans->NUMEQUIT.'&NUMEAVEN='.$getTrans->NUMEAVEN,
											CURLOPT_HTTPHEADER => array(
												'Content-Type: application/x-www-form-urlencoded'
											),
										));
										$response = json_decode(curl_exec($curl), true);
										curl_close($curl);
										if ($response['code'] == 1)
										{
											if (isset($response['data']))
											{
												$NUMEENCA = $response['data']['NUMEENCA'];
												if ($NUMEENCA)
												{
													$numeEncais .= $NUMEENCA;
													$this->transModel->modifierTermesListes($com, $NUMEENCA,
														$getTrans->NUMEQUIT, NULL, NULL, NULL);
												}

											}

										}
									}

								}
							}
						}

					}

					if ($reference)
					{
						require APPPATH . 'libraries/fpdf182/fpdf.php';

						$refpaie = $reference;
						$listeDesPolices = $policesConcatees;
						$moisTermes = $moisTermes;
						$numeEncais = $numeEncais;
						$trans_status =  "Succes";
						// create file name
						$path = 'assets/generatepdf/';
						if (!is_dir($path))
						{
							//create the folder if it's not already exists
							mkdir($path, 0755, TRUE);
							$path = $path;
						}

						$fichiers_def = $refpaie.'.pdf';
						$html = '<p style="text-align:center";><a href="http://www.sycapay.net">Confirmation de paiement</a></p><br><br><br><br>       	     
								 <span style="text-align:center";>REFERENCE : '.$refpaie.'</span><br><br><br>
								 <span style="text-align:center";>MONTANT : '.$montant.'</span><br><br><br>
								 <span style="text-align:center";>ASSURE : '.$assure.'</span><br><br><br>
								 <span style="text-align:center";>LISTE DES QUITTANCES : '.$listeDesPolices.'</span><br><br><br>
								 <span style="text-align:center";>STATUS PAIEMENT: '.$trans_status.'</span><br><br><br>
								 <span style="text-align:center";>DATE DU PAIEMENT : '.$datepaie.'</span><br><br><br><br>
								 <span style="text-align:center";>SOCIETE : ABI</span><br><br><br>
								 <span style="text-align:center";>PRODUIT : '.$produit.'</span><br><br><br>
								 <span style="text-align:center";>MOIS TERMES : '.$moisTermes.'</span><br><br><br>
								 <span style="text-align:center";>NUMERO ENCAISSEMENT : '.$numeEncais.'</span><br><br><br>
								 ';

						$pdf = new FPDF();
						// Première page
						$pdf->AddPage();
						$pdf->SetFont('Arial','',14);
						$pdf->WriteHTML($html);
						$pdf->Output('F', $path.$fichiers_def);

						echo $refpaie;

					}
					else
					{
						echo $reference;
					}

				}
				else
				{
					echo "";
				}
			}
			else
			{
				echo "";
			}
		}
		else
		{
			echo "";
		}

	}

	public function getCheckOperateurs()
	{
		$mobile = $this->input->post('mobile');
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://soaga.sycaretail.com/index.php/Topup/accessToken',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_HTTPHEADER => array(
				'Cookie: ci_session=l7i1isuogd6kp2rrs6naptrg802dlq5o'
			),
		));
		$response = json_decode(curl_exec($curl), true);
		curl_close($curl);
		if ($response['status'] == true)
		{
			$toakenOP = $response['token'];
			$ch = curl_init();
			curl_setopt_array($ch, array(
				CURLOPT_URL => 'https://soaga.sycaretail.com/index.php/Topup/CheickMobileOperators',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_POSTFIELDS => 'client_auth='.$toakenOP.'&client_mobile='.$mobile.'&iso_code='.$this->session->userdata("fk_pays"),
				CURLOPT_HTTPHEADER => array(
					'Content-Type: application/x-www-form-urlencoded',
					'Cookie: ci_session=l7i1isuogd6kp2rrs6naptrg802dlq5o'
				),
			));

			$response = json_decode(curl_exec($ch), true);
			curl_close($ch);

			if ($response['status'] == true)
			{
				if (strpos($response['OperatorName'], "Orange") === false)
				{
					echo "NO";
				}
				else
				{
					echo "OK";
				}
			}
			else
			{
				echo "";
			}
		}
		else
		{
			echo "";
		}

	}

	public function getRetoursPaiements()
	{
//    var_dump($this->session->all_userdata());
		$purchaseinfo = $this->input->post('purchaseinfo');
		$montant = $this->input->post('montant');
		$status = $this->input->post('status');
		$reference = $this->input->post('reference');
		$contact = $this->input->post('contact');
		$operatorTrans = $this->input->post('provider');
		$policesConcatees = "";
		$moisTermes = "";
		$numeEncais = "";

//	if ($operatorTrans == "MarterCardCI"){
//
//    } elseif ($operatorTrans == "EcobCI"){
//
//    } elseif ($operatorTrans == "OrangeCI"){
//
//    } elseif ($operatorTrans == "MtnCI"){
//
//    } elseif ($operatorTrans == "MoovCI"){
//
//    }

		if ($operatorTrans == "MarterCardCI"){
			$operatorTrans = "MASTERCARD";
		} elseif ($operatorTrans == "EcobCI"){
			$operatorTrans = "VISA";
		}

		if ($purchaseinfo)
		{

			$commandes = explode("|", $purchaseinfo);
			$id = false;
			$nbre_mois = 0;
			foreach($commandes as $com)
			{
				$nbre_mois = $nbre_mois + 1;
				$id = $this->transModel->modifier($reference, $com, $contact, $status, $operatorTrans);
				if (isset($status))
				{
					if ($status == '0')
					{
						$getTrans = $this->transModel->getTransByRefPaie($com);
						if ($getTrans)
						{
							//Pour la génération du pdf
							$produit = $getTrans->produit_trans;
							$assure = $getTrans->assure_trans;
							$societe = $getTrans->ent_raison;
							$datepaie = $getTrans->date_create_trans;

							if ($nbre_mois > 1){
								$policesConcatees .= "|";
								$moisTermes .= "|";
								$numeEncais .= "|";
							}

							$policesConcatees .= $getTrans->assure_num_polices;
							$MOBILEMONEY = strtoupper(str_replace(array("CI", " ", "TG"), "", $operatorTrans));
							$TELEASSU = '+'.$getTrans->assureMobile;

							if ($getTrans->produit_trans == 'VIE')
							{
								$curl = curl_init();
								curl_setopt_array($curl, array(
									CURLOPT_URL => '197.159.217.53/abi/getListeTerme.php',
									CURLOPT_RETURNTRANSFER => true,
									CURLOPT_ENCODING => '',
									CURLOPT_MAXREDIRS => 10,
									CURLOPT_TIMEOUT => 0,
									CURLOPT_FOLLOWLOCATION => true,
									CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
									CURLOPT_CUSTOMREQUEST => 'POST',
									CURLOPT_POSTFIELDS => 'TELEASSU='.$TELEASSU.'&CODEINTE='.$getTrans->assure_code_inte.'&NUMEPOLI='.$getTrans->assure_num_polices.'&NOMBPERI='.$getTrans->nombre_choisis.'&REFEENCA='.$com.'&MONTENCA='.$getTrans->montant_trans.'&MOBILEMONEY='.$MOBILEMONEY,
									CURLOPT_HTTPHEADER => array(
										'Content-Type: application/x-www-form-urlencoded'
									),
								));

								$response = json_decode(curl_exec($curl), true);
								curl_close($curl);
								if ($response['code'] == 1)
								{
									if (isset($response['data']))
									{
										$quittances = $response['data']['quittances'];

										if ($quittances)
										{
											foreach ($quittances as $gets)
											{
												$NUMEENCA = $gets['NUMEENCA'];
												$NUMEQUIT = $gets['NUMEQUIT'];
												$MOISTERM = $gets['MOISTERM'];
												$DATEEFFE = $gets['DATEEFFE'];
												$DATEECHE = $gets['DATEECHE'];
											}

											$moisTermes .= $MOISTERM;
											$numeEncais .= $NUMEENCA;

											$this->transModel->modifierTermesListes($com, $NUMEENCA,
												$NUMEQUIT, $MOISTERM, $DATEEFFE, $DATEECHE);
										}

									}

								}
							}
							else
							{
								$curl = curl_init();
								curl_setopt_array($curl, array(
									CURLOPT_URL => '197.159.217.53/abi/getListeQuittancesDirectes.php',
									CURLOPT_RETURNTRANSFER => true,
									CURLOPT_ENCODING => '',
									CURLOPT_MAXREDIRS => 10,
									CURLOPT_TIMEOUT => 0,
									CURLOPT_FOLLOWLOCATION => true,
									CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
									CURLOPT_CUSTOMREQUEST => 'POST',
									CURLOPT_POSTFIELDS => 'TELEASSU='.$TELEASSU.'&CODEINTE='.$getTrans->assure_code_inte.'&NUMEPOLI='.$getTrans->assure_num_polices.'&REFEENCA='.$com.'&MONTENCA='.$getTrans->montant_trans.'&MOBILEMONEY='.$MOBILEMONEY.'&NUMEQUIT='.$getTrans->NUMEQUIT.'&NUMEAVEN='.$getTrans->NUMEAVEN,
									CURLOPT_HTTPHEADER => array(
										'Content-Type: application/x-www-form-urlencoded'
									),
								));
								$response = json_decode(curl_exec($curl), true);
								curl_close($curl);
								if ($response['code'] == 1)
								{
									if (isset($response['data']))
									{
										$NUMEENCA = $response['data']['NUMEENCA'];
										if ($NUMEENCA)
										{
											$numeEncais .= $NUMEENCA;
											$this->transactionsModel->modifierTermesListes($com, $NUMEENCA,
												$getTrans->NUMEQUIT, NULL, NULL, NULL);
										}

									}

								}
							}

						}
					}
				}

			}


			if ($reference)
			{
				$nbre_mois = 0;
				$montant_trans = 0;
				$montant_trans = "";
				$assure_trans = "";
				$produit = "";
				$policesConcatees = "";
				$moisTermes = "";
				$numeEncais = "";
				$datepaie = date("Y-m-d H:i:s");

				$trans = $this->transModel->getReferenceSycapay($reference);
				if (!empty($trans))
				{
					$assureID = $trans[0]->assureID;
					foreach ($trans as $valors)
					{
						$nbre_mois = $nbre_mois + 1;
						if ($nbre_mois > 1)
						{
							$policesConcatees .= "|";
							$moisTermes .= "|";
							$numeEncais .= "|";
						}

						$policesConcatees .= $valors->assure_num_polices;
						$moisTermes .= $valors->MOISTERM;
						$numeEncais .= $valors->NUMEENCA;
						$montant_trans += $valors->montant_trans;
						$assure_trans = $valors->assure_trans;
						$ent_raison = $valors->ent_raison;
						$produit = $valors->produit_trans;
						$datepaie = $valors->date_create_trans;
					}

					//$this->session->set_userdata(array("getFatures" => $trans));
					$data['page_title'] = $this->lang->line('Accueil_title');
					$data['assure'] = $assure_trans;
					$data['societe'] = $ent_raison;
					$data['produit'] = $produit;
					$data['nbre_mois'] = $nbre_mois;
					$data['montant'] = $montant_trans;
					$data['refpaie'] = $reference;
					$data['datepaie'] = $datepaie;
					$data['moisTermes'] = $moisTermes;
					$data['numeEncais'] = $numeEncais;
					$data['listeDesPolices'] = $policesConcatees;


					$this->load->view('succes', $data);
				}
				else
				{
					$this->session->set_flashdata('error', 'Ce paiement est inexistant');
					redirect(site_url("Accueil"));
				}
			}
			else
			{
				$this->session->set_flashdata('error', 'Ce paiement est inexistant');
				redirect(site_url("Accueil"));
			}
		}
		else
		{
			$this->session->set_flashdata('error', 'Ce paiement est inexistant');
			redirect(site_url("Accueil"));
		}

	}

	public function getAnnulations($commande)
	{
		$commande = str_replace(array("__", "%", "%7C", "|", "+"), "|", $commande);
		if ($commande)
		{
			$commandes = explode("|", $commande);


			foreach($commandes as $com)
			{
				$id = $this->transModel->majStatusEchecs($com);
			}


			$this->session->set_flashdata('error', 'Paiement annulé !');
			redirect(site_url("Accueil"));
		}
		else
		{
			$this->session->set_flashdata('error', 'Paiement annulé !');
			redirect(site_url("Accueil"));
		}
	}

    public function getRetours()
    {   
        header("Access-Control-Allow-Origin: *");

        $purchaseinfo = $this->input->post('purchaseinfo');
        $montant = $this->input->post('montant');
        $status = $this->input->post('status');
        $reference = $this->input->post('reference');
        $contact = $this->input->post('contact');
        $operatorTrans = $this->input->post('provider');
        $query = $this->transModel->isCommande($purchaseinfo);

		if ($operatorTrans == "MarterCardCI"){
			$operatorTrans = "MASTERCARD";
		} elseif ($operatorTrans == "EcobCI"){
			$operatorTrans = "VISA";
		}

        if ($query)
        {   
            $data = $this->authModel->isIds($query->usersBackID);
            $userdata = array(
                'id_user' => $data->id_user,
                'nom_user' => $data->nom_user,
                'prenoms_user' => $data->prenoms_user,
                'role_fk' => $data->role_fk,
                'login_user' => $data->login_user,
                'etat_user'  => $data->user_etat,
                'ent_raison'  => $data->ent_raison,
                'fk_ent'  => $data->fk_ent,
                'fk_pays'  => $data->pays_id,
                );
            $this->session->set_userdata($userdata);
            
            $id = $this->transModel->modifier($reference, $purchaseinfo, $contact, $status);
            if ($id)
            {     
                if ($status == '0') 
                {
                    $MOBILEMONEY = strtoupper(str_replace(array("CI", " ", "TG"), "", $operatorTrans));
                    $TELEASSU = '+'.$query->assureMobile;
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                      CURLOPT_URL => '197.159.217.53/abi/getListeTerme.php',
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => '',
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 0,
                      CURLOPT_FOLLOWLOCATION => true,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => 'POST',
                      CURLOPT_POSTFIELDS => 'TELEASSU='.$TELEASSU.'&CODEINTE='.$query->assure_code_inte.'&NUMEPOLI='.$query->assure_num_polices.'&NOMBPERI=%201&REFEENCA='.$purchaseinfo.'&MONTENCA='.$query->montant_trans.'&MOBILEMONEY='.$MOBILEMONEY,
                      CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/x-www-form-urlencoded'
                      ),
                    ));
                    $response = json_decode(curl_exec($curl), true);
                    curl_close($curl);

                    if ($response['code'] == 1)
                    {
                        if (isset($response['data'])) 
                        {
                            $quittances = $response['data']['quittances'];

                            if ($quittances) 
                            {
                                foreach ($quittances as $gets) 
                                {
                                    $NUMEENCA = $gets['NUMEENCA'];
                                    $NUMEQUIT = $gets['NUMEQUIT'];
                                    $MOISTERM = $gets['MOISTERM'];
                                    $DATEEFFE = $gets['DATEEFFE'];
                                    $DATEECHE = $gets['DATEECHE'];
                                }

                                $moisTermes .= $MOISTERM;
                                $numeEncais .= $NUMEENCA;

                                $this->transModel->modifierTermesListes($purchaseinfo, $NUMEENCA, $NUMEQUIT, 
                                $MOISTERM, $DATEEFFE, $DATEECHE);
                            }

                        }
                    }
                }
                            
                $this->session->set_flashdata('success','Paiement effectué avec succès');
                redirect(site_url('Transactions'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Impossible de changer le status de la transaction !');
                redirect(site_url('Transactions'));
            }
        }
        else
        {
            $this->session->set_flashdata('error', 'Cette référence est inexistante !');
            redirect(site_url('Transactions'));
        }
    }

   public function ajax_list()
   {    
        $type = NULL;
        $search = $this->session->userdata('search');
        if (isset($search))
        {
            $statut = $this->session->userdata('statut');
            $provider = $this->session->userdata('provider');
            $date_min = $this->session->userdata('date_min');
            $date_max = $this->session->userdata('date_max');

            $list = $this->transModel->get_datatables($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));

            $data = array();
            $no = $_POST['start'];
            foreach ($list as $sous) {
              $no++;
              $row = array();
              $row[] = $sous->reference_syca;
              $row[] = $sous->atlantis_ref;
              $row[] = number_format($sous->montant_trans, 0, '.', ' ');
              $row[] = $sous->provider;
              $row[] = $sous->mobile_trans;

              if ($sous->statut_trans == 'E')
              {
                  $statut_trans =  '<span style=\'color:red;\'>'.$this->lang->line('label_echec').'</span>';
              }
              elseif ($sous->statut_trans == 'S') 
              {
                  $statut_trans =  '<span style=\'color:green;\'>'.$this->lang->line('label_succes').'</span>';
              }
              else
              {
                  $statut_trans =  '<span style=\'color:blue;\'>'.$this->lang->line('label_pending').'</span>';
              }

              $row[] = $statut_trans;
              $row[] = $sous->date_create_trans;

              $data[] = $row;
            }

            $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->transModel->count_all($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
                    "recordsFiltered" => $this->transModel->count_filtered($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
                    "data" => $data,
                    );
            
            //output to json format
            echo json_encode($output);
        }
        else
        {   

            $date = new DateTime();
            $date_min = $date -> format('Y-m-01');
            $date_max = $date -> format('Y-m-t');
            $statut = '0';
            $provider = '0';

            $list = $this->transModel->get_datatables($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));

            $data = array();
            $no = $_POST['start'];
            foreach ($list as $sous) {
              $no++;
              $row = array();
              $row[] = $sous->reference_syca;
              $row[] = $sous->atlantis_ref;
              $row[] = number_format($sous->montant_trans, 0, '.', ' ');
				$row[] = $sous->provider;
				$row[] = $sous->mobile_trans;

              if ($sous->statut_trans == 'E')
              {
                  $statut_trans =  '<span style=\'color:red;\'>'.$this->lang->line('label_echec').'</span>';
              }
              elseif ($sous->statut_trans == 'S') 
              {
                  $statut_trans =  '<span style=\'color:green;\'>'.$this->lang->line('label_succes').'</span>';
              }
              else
              {
                  $statut_trans =  '<span style=\'color:blue;\'>'.$this->lang->line('label_pending').'</span>';
              }

              $row[] = $statut_trans;
              $row[] = $sous->date_create_trans;

              $data[] = $row;
            }

            $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->transModel->count_all($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
                    "recordsFiltered" => $this->transModel->count_filtered($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
                    "data" => $data,
                    );
            
            //output to json format
            echo json_encode($output);
        }
    }

   public function ajax_list_vie()
   {
        $search = $this->session->userdata('search');
       $type = "VIE";

       if (isset($search))
        {
            $statut = $this->session->userdata('statut');
            $date_min = $this->session->userdata('date_min');
            $date_max = $this->session->userdata('date_max');
			$provider = $this->session->userdata('provider');

            $list = $this->transModel->get_datatables($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));

            $data = array();
            $no = $_POST['start'];
            foreach ($list as $sous) {
              $no++;
              $row = array();
              $row[] = $sous->reference_syca;
              $row[] = $sous->atlantis_ref;
              $row[] = number_format($sous->montant_trans, 0, '.', ' ');
				$row[] = $sous->provider;
				$row[] = $sous->mobile_trans;

              if ($sous->statut_trans == 'E')
              {
                  $statut_trans =  '<span style=\'color:red;\'>'.$this->lang->line('label_echec').'</span>';
              }
              elseif ($sous->statut_trans == 'S')
              {
                  $statut_trans =  '<span style=\'color:green;\'>'.$this->lang->line('label_succes').'</span>';
              }
              else
              {
                  $statut_trans =  '<span style=\'color:blue;\'>'.$this->lang->line('label_pending').'</span>';
              }

              $row[] = $statut_trans;
              $row[] = $sous->date_create_trans;

              $data[] = $row;
            }

            $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->transModel->count_all($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
                    "recordsFiltered" => $this->transModel->count_filtered($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
                    "data" => $data,
                    );

            //output to json format
            echo json_encode($output);
        }
        else
        {

            $date = new DateTime();
            $date_min = $date -> format('Y-m-01');
            $date_max = $date -> format('Y-m-t');
            $statut = '0';
            $provider = '0';

            $list = $this->transModel->get_datatables($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));

            $data = array();
            $no = $_POST['start'];
            foreach ($list as $sous) {
              $no++;
              $row = array();
              $row[] = $sous->reference_syca;
              $row[] = $sous->atlantis_ref;
              $row[] = number_format($sous->montant_trans, 0, '.', ' ');
				$row[] = $sous->provider;
				$row[] = $sous->mobile_trans;

              if ($sous->statut_trans == 'E')
              {
                  $statut_trans =  '<span style=\'color:red;\'>'.$this->lang->line('label_echec').'</span>';
              }
              elseif ($sous->statut_trans == 'S')
              {
                  $statut_trans =  '<span style=\'color:green;\'>'.$this->lang->line('label_succes').'</span>';
              }
              else
              {
                  $statut_trans =  '<span style=\'color:blue;\'>'.$this->lang->line('label_pending').'</span>';
              }

              $row[] = $statut_trans;
              $row[] = $sous->date_create_trans;

              $data[] = $row;
            }

            $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->transModel->count_all($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
                    "recordsFiltered" => $this->transModel->count_filtered($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
                    "data" => $data,
                    );

            //output to json format
            echo json_encode($output);
        }
    }

   public function ajax_list_iard()
   {
        $search = $this->session->userdata('search');
       $type = "IARD";

       if (isset($search))
        {
            $statut = $this->session->userdata('statut');
            $date_min = $this->session->userdata('date_min');
            $date_max = $this->session->userdata('date_max');
			$provider = $this->session->userdata('provider');

            $list = $this->transModel->get_datatables($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));

            $data = array();
            $no = $_POST['start'];
            foreach ($list as $sous) {
              $no++;
              $row = array();
              $row[] = $sous->reference_syca;
              $row[] = $sous->atlantis_ref;
              $row[] = number_format($sous->montant_trans, 0, '.', ' ');
				$row[] = $sous->provider;
				$row[] = $sous->mobile_trans;

              if ($sous->statut_trans == 'E')
              {
                  $statut_trans =  '<span style=\'color:red;\'>'.$this->lang->line('label_echec').'</span>';
              }
              elseif ($sous->statut_trans == 'S')
              {
                  $statut_trans =  '<span style=\'color:green;\'>'.$this->lang->line('label_succes').'</span>';
              }
              else
              {
                  $statut_trans =  '<span style=\'color:blue;\'>'.$this->lang->line('label_pending').'</span>';
              }

              $row[] = $statut_trans;
              $row[] = $sous->date_create_trans;

              $data[] = $row;
            }

            $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->transModel->count_all($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
                    "recordsFiltered" => $this->transModel->count_filtered($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
                    "data" => $data,
                    );

            //output to json format
            echo json_encode($output);
        }
        else
        {

            $date = new DateTime();
            $date_min = $date -> format('Y-m-01');
            $date_max = $date -> format('Y-m-t');
            $statut = '0';
            $provider = '0';

            $list = $this->transModel->get_datatables($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));

            $data = array();
            $no = $_POST['start'];
            foreach ($list as $sous) {
              $no++;
              $row = array();
              $row[] = $sous->reference_syca;
              $row[] = $sous->atlantis_ref;
              $row[] = number_format($sous->montant_trans, 0, '.', ' ');
				$row[] = $sous->provider;
				$row[] = $sous->mobile_trans;

              if ($sous->statut_trans == 'E')
              {
                  $statut_trans =  '<span style=\'color:red;\'>'.$this->lang->line('label_echec').'</span>';
              }
              elseif ($sous->statut_trans == 'S')
              {
                  $statut_trans =  '<span style=\'color:green;\'>'.$this->lang->line('label_succes').'</span>';
              }
              else
              {
                  $statut_trans =  '<span style=\'color:blue;\'>'.$this->lang->line('label_pending').'</span>';
              }

              $row[] = $statut_trans;
              $row[] = $sous->date_create_trans;

              $data[] = $row;
            }

            $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->transModel->count_all($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
                    "recordsFiltered" => $this->transModel->count_filtered($statut, $type, $provider, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
                    "data" => $data,
                    );

            //output to json format
            echo json_encode($output);
        }
    }
}
