<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

  public function __construct()
  {
      parent::__construct();
      $this->load->library(array('form_validation', 'mailjet'));
      $this->load->model('Global_model', 'globalModel');
      $this->load->model('Entreprise_model', 'entModel');
      $this->load->model('Agence_model', 'agenceModel');
      $this->load->model('Roles_model', 'rolesModel');
	  $this->load->model('Primes_model', 'primesModel');
      $this->load->model('Users_model', 'usersModel');

	//  $_GET = decontaminate_data($_GET);
	  // $_POST = decontaminate_data($_POST);
  }

  public function index()
  {
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL) 
      {
          redirect(site_url("Accueil"));
      }
      else
      {
          $role_fk = $this->session->userdata('role_fk');
		  $prime_fk = $this->session->userdata('prime_fk');
          $query = $this->usersModel->getUsers();
          if (empty($query)) {
              $query = 'query';
          }

          $libelle = "USERS";
          $description = "L'utilisateur consulte le menu users";
          $this->globalModel->traces($libelle, $description, NULL);

          if ($role_fk == 1){

              $data = array('affiche_users' => $query,
                  'modifier_users' => "",
                  'ajouter_users' => "",
                  'page_title' => $this->lang->line('Accueil_title')
              );
              $this->load->view('communs/administrateurs', $data);

          } else {

              $data = array('affiche_users' => $query,
                  'modifier_users' => "",
                  'ajouter_users' => "",
                  'page_title' => $this->lang->line('Accueil_title')
              );
              $this->load->view('communs/users', $data);

          }
        }
  }

  public function addUsers()
  {
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL) 
      {
          redirect(site_url("Accueil"));
      }
      else
      {
          $role_fk = $this->session->userdata('role_fk');
		  $prime_fk = $this->session->userdata('prime_fk');

          $reqAgences = $this->agenceModel->getActiveAgences();
          $reqRoles = $this->rolesModel->getOthersRoles();
		  $reqPrimes = $this->primesModel->getOthersPrimes();
          $reqEntreprise = $this->entModel->getActiveEntreprises();


          if ($role_fk == 1){

              $reqRoles = $this->rolesModel->getAdminRoles();
			  $reqPrimes = $this->primesModel->getAdminPrimes();
              $data = array('affiche_users' => "",
                  'modifier_users' => "",
                  'liste_agences' => $reqAgences,
                  'liste_roles' => $reqRoles,
                  'liste_primes'=> $reqPrimes,
                  'liste_entreprises' => $reqEntreprise,
                  'ajouter_users' => "ajouter_users",
                  'page_title' => $this->lang->line('Accueil_title')
              );
              $this->load->view('communs/administrateurs', $data);

          } else {


              $data = array('affiche_users' => "",
                  'modifier_users' => "",
                  'liste_agences' => $reqAgences,
                  'liste_roles' => $reqRoles,
				  'liste_primes'=> $reqPrimes,
                  'liste_entreprises' => $reqEntreprise,
                  'ajouter_users' => "ajouter_users",
                  'page_title' => $this->lang->line('Accueil_title')
              );
              $this->load->view('communs/users', $data);

          }

        }
  }

  public function insertUsers()
  {
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL) 
      {
          redirect(site_url("Accueil"));
      }
      else
      {     
            $nom_user = $this->globalModel->pasDeCaractersBizares($this->input->post('nom_user'));
            $prenoms_user = $this->globalModel->pasDeCaractersBizares($this->input->post('prenoms_user'));
            $agences_fk = $this->input->post('agences_fk');
            $fk_ent = $this->input->post('fk_ent');
            $login_user = $this->input->post('login_user');
//            $password = $this->input->post('password');
            $role_fk = $this->input->post('role_fk');
		    $prime_fk = $this->input->post('prime_fk');
            if (empty($agences_fk) AND empty($fk_ent)) 
            {
               $this->session->set_flashdata('error', $this->lang->line('text_select'));
               redirect(site_url('Users'));
            }

            $queryUsers = $this->usersModel->existeUsers($login_user);

            if (empty($nom_user) OR empty($prenoms_user) OR empty($login_user)) 
            {
                $this->session->set_flashdata('error', 'Les champs sont requis / Fields are required !');
                redirect(site_url('Users'));
            }

            $verify_email = filter_var($login_user, FILTER_VALIDATE_EMAIL);
            if($verify_email == FALSE) 
            {
              $this->session->set_flashdata('error','Format d\'email incorrect !'); 
              redirect(site_url('Users'));
            }

            if (empty($queryUsers)) 
            {   
                  if ($agences_fk) 
                  {
                      $queryAgence = $this->agenceModel->isIdAgences($agences_fk);
                      if ($queryAgence) 
                      {
                          $fk_ent = $queryAgence->entreprise_fk;
                      }
                      else
                      {
                          $this->session->set_flashdata('error', 'Agence Not Exist !');
                          redirect(site_url('Agence'));
                      }
                  }

                  $addIng = $this->usersModel->ajouterUsers($role_fk, $nom_user, $prenoms_user, $login_user, $fk_ent,$prime_fk);

                  if ($addIng) 
                  {   
                      $this->usersModel->doAffectations($agences_fk, $addIng);

                      $url = site_url("Accueil/definePassword/".sha1($addIng));

                      $base_path = base_url();
                      $html_message = base_url()."assets/emails/template-1.html";
                      $message = str_replace(array("user_id", "LINK_URL", "SITE_URL"), array($addIng, $base_path, $url), file_get_contents($html_message));

                      $mailto = $this->mailjet->emailing($login_user, $this->lang->line('notif_creation'), $message);
                      if ($mailto) 
                      {
                          $this->session->set_flashdata('success', $this->lang->line("notif_succes"));
                      }
                      else
                      {               
                          $this->session->set_flashdata('error', 'Mailing impossible !');
                      }

                      $libelle = "ADDING";
                      $description = "L'utilisateur ajoute l'utilisateur";
                      $this->globalModel->traces($libelle, $description, $addIng);
                  }
                  else
                  {
                     $this->session->set_flashdata('error', 'Operation impossible !');
                  }

                  redirect(site_url('Users'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Users Existe !');
                redirect(site_url('Users'));
            }
      }
  }

  public function disableUsers($id_user)
  {
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL) 
      {
          redirect(site_url("Accueil"));
      }
      else
      {     
            $query = $this->usersModel->getProfil($id_user);
            if ($query)
            {
                $addIng = $this->usersModel->desactiverUsers($id_user);

                if ($addIng) 
                {
                    $this->session->set_flashdata('success', $this->lang->line("notif_succes"));
                    $libelle = "DISABLE";
                    $description = "L'utilisateur désactive l'utilisateur";
                    $this->globalModel->traces($libelle, $description, $id_user);
                }
                else
                {
                   $this->session->set_flashdata('error', 'Operation impossible !');
                }

                redirect(site_url('Users'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Users Not Exist !');
                redirect(site_url('Users'));
            }
      }
  }

  public function enableUsers($id_user)
  {
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL) 
      {
          redirect(site_url("Accueil"));
      }
      else
      {     
            $query = $this->usersModel->getProfil($id_user);
            if ($query)
            {
                $addIng = $this->usersModel->activerUsers($id_user);

                if ($addIng) 
                {
                    $this->session->set_flashdata('success', $this->lang->line("notif_succes"));
                    $libelle = "ENABLE";
                    $description = "L'utilisateur active l'utilisateur";
                    $this->globalModel->traces($libelle, $description, $id_user);
                }
                else
                {
                   $this->session->set_flashdata('error', 'Operation impossible !');
                }

                redirect(site_url('Users'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Users Not Exist !');
                redirect(site_url('Users'));
            }
      }
  }

  public function majUsers($id_user)
  {
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL) 
      {
          redirect(site_url("Accueil"));
      }
      else
      {

          $role_fk = $this->session->userdata('role_fk');
		  $prime_fk = $this->session->userdata('prime_fk');


          $reqUsers = $this->usersModel->getProfil($id_user);
          $reqAgences = $this->agenceModel->getActiveAgences();
		  $reqPrimes = $this->primesModel->getActivePrimes();
          $reqRoles = $this->rolesModel->getActiveRoles();
          $reqEntreprise = $this->entModel->getActiveEntreprises();


          if ($role_fk == 1){

              $reqRoles = $this->rolesModel->getAdminRoles();
			  $reqPrimes = $this->primesModel->getAdminPrimes();

              $data = array('affiche_users' => "",
                  'modifier_users' => $reqUsers,
                  'liste_agences' => $reqAgences, 
                  'liste_roles' => $reqRoles,
                  'liste_primes'=>$reqPrimes,
                  'liste_entreprises' => $reqEntreprise,
                  'ajouter_users' => "",
                  'page_title' => $this->lang->line('Accueil_title')
              );
              $this->load->view('communs/administrateurs', $data);

          } else {



              $data = array('affiche_users' => "",
                  'modifier_users' => $reqUsers,
                  'liste_agences' => $reqAgences,
                  'liste_roles' => $reqRoles,
				  'liste_primes'=>$reqPrimes,
                  'liste_entreprises' => $reqEntreprise,
                  'ajouter_users' => "",
                  'page_title' => $this->lang->line('Accueil_title')
              );
              $this->load->view('communs/users', $data);

          }
      }
  }

  public function updateUsers()
  {
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL) 
      {
          redirect(site_url("Accueil"));
      }
      else
      {     
            $nom_user = $this->globalModel->pasDeCaractersBizares($this->input->post('nom_user'));
            $prenoms_user = $this->globalModel->pasDeCaractersBizares($this->input->post('prenoms_user'));
            $agences_fk = $this->input->post('agences_fk');
            $fk_ent = $this->input->post('fk_ent');
            $id_user = $this->input->post('id_user');
            $password = $this->input->post('password');
            $role_fk = $this->input->post('role_fk');
		    $prime_fk = $this->input->post('prime_fk');
            if (empty($agences_fk) AND empty($fk_ent)) 
            {
               $this->session->set_flashdata('error', $this->lang->line('text_select'));
               redirect(site_url('Users'));
            }

            $queryUsers = $this->usersModel->getProfil($id_user);

            if (empty($nom_user) OR empty($prenoms_user)) 
            {
                $this->session->set_flashdata('error', 'Les champs sont requis / Fields are required !');
                redirect(site_url('Users'));
            }

            if ($queryUsers) 
            {   
                  if ($agences_fk) 
                  {
                      $queryAgence = $this->agenceModel->isIdAgences($agences_fk);
                      if ($queryAgence) 
                      {
                          $fk_ent = $queryAgence->entreprise_fk;        
                      }
                      else
                      {
                          $this->session->set_flashdata('error', 'Agence Not Exist !');
                          redirect(site_url('Agence'));
                      }
                  }

                  $addIng = $this->usersModel->modifierUsers($role_fk, $nom_user, $prenoms_user, $id_user, $password, $fk_ent,$prime_fk);

                  if ($addIng) 
                  {    
                      $getAffecter = $this->usersModel->existeAffecation($agences_fk, $id_user);
                      if ($getAffecter) 
                      {
                          $this->usersModel->retirerAffecter($id_user);
                      }

                      $this->usersModel->doAffectations($agences_fk, $id_user);

                      $libelle = "MODIFIER";
                      $description = "L'utilisateur modifie l'utilisateur";
                      $this->globalModel->traces($libelle, $description, $id_user);
                  }
                  else
                  {
                     $this->session->set_flashdata('error', 'Operation impossible !');
                  }

                  redirect(site_url('Users'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Users Inexistant !');
                redirect(site_url('Users'));
            }
      }
  }


   public function ajax_list()
   { 
        $list = $this->usersModel->get_datatables();

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $sous) {
          $no++;
          $row = array();
          $row[] = $sous->nom_user;
          $row[] = $sous->prenoms_user;
          $row[] = $sous->login_user;

            $role_fk = $this->session->userdata('role_fk');
			$prime_fk = $this->session->userdata('prime_fk');
            if ($role_fk == 1){
                $row[] = $sous->ent_raison;
            } else {
                $row[] = $sous->libelle_role;

            }


            if ($sous->user_etat == 'I')
          {
              $user_etat =  '<span style=\'color:red;\'>'.$this->lang->line('label_desactiver').'</span>';
              $activeStrap =  "btn btn-xs btn-default";
              $desactiveStrap =  "btn btn-xs btn-danger";

          }
          else
          {   
              $activeStrap = "btn btn-xs btn-primary";
              $desactiveStrap =  "btn btn-xs btn-default";
              $user_etat =  '<span style=\'color:blue;\'>'.$this->lang->line('label_activer').'</span>';
          }

          $row[] = $user_etat;
          $row[] = $sous->date_create_user;                

          $row[] = '<a class="btn-success btn-xs" href="'.site_url("Users/majUsers")."/".$sous->id_user.'"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;&nbsp;<div class="btn-group btn-toggle"><a href="'.site_url('Users/enableUsers')."/".$sous->id_user.'"  class="'.$activeStrap.'" onClick="return confirm("'.$this->lang->line('confirm_modal_text').'")">'.$this->lang->line('label_activer').'</a><a href="'.site_url("Users/disableUsers")."/".$sous->id_user.'" class="'.$desactiveStrap.'" onClick="return confirm("'.$this->lang->line('confirm_modal_text').'")">'.$this->lang->line('label_desactiver').'</a></div>';

          $data[] = $row;
        }

        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->agenceModel->count_all(),
                "recordsFiltered" => $this->agenceModel->count_filtered(),
                "data" => $data,
                );
        
        //output to json format
        echo json_encode($output);
    }
}
