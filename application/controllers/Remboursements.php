<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Remboursements extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Global_model', 'globalModel');
        $this->load->model('Agence_model', 'agenceModel');
        $this->load->model('Transactions_model', 'transModel');
        $this->load->model('Entreprise_model', 'entModel');
        $this->load->model('Refund_model', 'refundModel');
        $this->load->library('excel');

		$_GET = decontaminate_data($_GET);
		$_POST = decontaminate_data($_POST);
    }

    public function index()
    {
        $login_user = $this->session->userdata('login_user');
        if ($login_user == NULL) {
            redirect(site_url("Accueil"));
        } else {
            $date = new DateTime();
            $date_min = $date->format('Y-m-01');
            $date_max = $date->format('Y-m-d');
            $statut = '0';
            $type = 'VIE';
            $this->session->set_userdata("type", $type);

            $filtre_date = array('date_min' => $date_min, 'date_max' => $date_max, 'statut' => $statut);
            $this->session->set_userdata($filtre_date);

            $query = $this->refundModel->liste_lots("vie");

            $data = array('affiche_trans' => $query,
                'page_title' => $this->lang->line('Accueil_title')
            );
            $data['page'] = "remboursements";

            $this->load->view('communs/remboursements', $data);
        }
    }

    public function rechercher2()
    {
        $login_user = $this->session->userdata('login_user');
        if ($login_user == NULL) {
            redirect(site_url("Accueil"));
        } else {
            $date_min = $this->input->post("date_min");
			$date_max = $this->input->post("date_max");
            $statut = $this->input->post("statut");
            $type = 'VIE';
            $this->session->set_userdata("type", $type);

            $filtre_date = array('date_min' => $date_min, 'date_max' => $date_max, 'statut' => $statut);
            $this->session->set_userdata($filtre_date);

            $query = $this->refundModel->liste_lots("vie");

            $data = array('affiche_trans' => $query,
                'page_title' => $this->lang->line('Accueil_title')
            );
            $data['page'] = "remboursements";

            $this->load->view('communs/remboursements', $data);
        }
    }

    public function demandes($id_lot)
    {
        $login_user = $this->session->userdata('login_user');
        if ($login_user == NULL) {
            redirect(site_url("Accueil"));
        } elseif (!isset($id_lot)) {
            redirect(site_url("Remboursements"));
        } else {
            $date = new DateTime();
            $date_min = $date->format('Y-m-01');
            $date_max = $date->format('Y-m-d');
            $statut = '0';
            $type = 'VIE';
            $this->session->set_userdata("type", $type);

            $filtre_date = array('date_min' => $date_min, 'date_max' => $date_max, 'statut' => $statut);
            $this->session->set_userdata($filtre_date);

            $query = $this->refundModel->liste_demandes($id_lot);
			//var_dump($query);
            $data = array('affiche_trans' => $query,
                'page_title' => "Demandes du lot " . $id_lot
            );
            $data['page'] = "remboursements";
            $data['demandes'] = $query;

            $this->load->view('communs/demandes', $data);
        }
    }

    public function sample()
    {
        $login_user = $this->session->userdata('login_user');
        if ($login_user == NULL) {
            redirect(site_url("Accueil"));
        } else {
			$this->load->helper('download');

			$this->_push_file(FCPATH."/assets/sample.csv", "sample.csv");

			redirect(site_url("Remboursements"));
        }
    }

	function _push_file($path, $name)
	{
		// make sure it's a file before doing anything!
		if(is_file($path))
		{
			// required for IE
			if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off'); }

			// get the file mime type using the file extension
			$this->load->helper('file');

			$mime = get_mime_by_extension($path);

			// Build the headers to push out the file properly.
			header('Pragma: public');     // required
			header('Expires: 0');         // no cache
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($path)).' GMT');
			header('Cache-Control: private',false);
			header('Content-Type: '.$mime);  // Add the mime type from Code igniter.
			header('Content-Disposition: attachment; filename="'.basename($name).'"');  // Add the file name
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: '.filesize($path)); // provide file size
			header('Connection: close');
			readfile($path); // push it out
			exit();
		}
	}

    public function import()
    {
        $ent_fk = $this->session->userdata('fk_ent');

        $lot_id = uniqid();
        $nbre = 1;
        if (isset($_FILES["file"]["name"])) {
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach ($object->getWorksheetIterator() as $worksheet) {
                $highestRow = $worksheet->getHighestRow();
                $nbre = $highestRow - 1;
                $highestColumn = $worksheet->getHighestColumn();
                for ($row = 2; $row <= $highestRow; $row++) {
                    $ligne = explode(";", $worksheet->getCellByColumnAndRow(0, $row)->getValue());
                	$beneficiaire = $ligne[0];
                    $sinistre = $ligne[1];
                    $mobile = $ligne[2];
                    $details = $ligne[3];
                    $montant = $ligne[4];
                    $quittance = $ligne[5];
//
//                    var_dump($beneficiaire);
//                    var_dump($sinistre);
//                    var_dump($mobile);
//                    var_dump($details);
//                    var_dump($montant);
                    //exit();

                    $this->refundModel->inserer_demande($mobile, $montant, $details, $beneficiaire, $sinistre, $quittance, $lot_id);
                    $data[] = array(
                        'beneficiaire' => $beneficiaire,
                        'numero_sinistre' => $sinistre,
                        'mobile_demande' => $mobile,
                        'details_demande' => $details,
                        'montant_demande' => $montant,
                        'quittance_sinistre' => $quittance,
                        'id_lot' => $lot_id
                    );
                }
            }
			$role_fk = $this->session->userdata('role_fk');

			if ($role_fk == "4") {
				$this->refundModel->insert_lot2($lot_id, $nbre, $ent_fk);
			} else {
				$this->refundModel->insert_lot($lot_id, $nbre, $ent_fk);
			}
            $this->session->set_flashdata('success', $this->lang->line("notif_succes"));
            //echo 'Data Imported successfully';
            redirect(site_url("Remboursements"));
        }
    }

    public function vie()
    {
        $login_user = $this->session->userdata('login_user');
        if ($login_user == NULL) {
            redirect(site_url("Accueil"));
        } else {
            $date = new DateTime();
            $date_min = $date->format('Y-m-01');
            $date_max = $date->format('Y-m-d');
            $statut = '0';
            $type = 'VIE';
            $this->session->set_userdata("type", $type);

            $filtre_date = array('date_min' => $date_min, 'date_max' => $date_max, 'statut' => $statut);
            $this->session->set_userdata($filtre_date);

            $query = $this->refundModel->liste_lots("vie");

            $data = array('affiche_trans' => $query,
                'page_title' => $this->lang->line('Accueil_title')
            );
            $data['page'] = "remboursements_vie";

            $this->load->view('communs/remb_vie', $data);
        }
    }

    public function iard()
    {
        $login_user = $this->session->userdata('login_user');
        if ($login_user == NULL) {
            redirect(site_url("Accueil"));
        } else {
            $date = new DateTime();
            $date_min = $date->format('Y-m-01');
            $date_max = $date->format('Y-m-d');
            $statut = '0';
            $type = 'IARD';
            $this->session->set_userdata("type", $type);

            $filtre_date = array('date_min' => $date_min, 'date_max' => $date_max, 'statut' => $statut);
            $this->session->set_userdata($filtre_date);

            $query = $this->refundModel->liste_lots("vie");

            $data = array('affiche_trans' => $query,
                'page_title' => $this->lang->line('Accueil_title')
            );
            $data['page'] = "remboursements_iard";

            $this->load->view('communs/remb_iard', $data);
        }
    }

    public function exportFormat()
    {
        //on vérifie si une session existe
        $login_user = $this->session->userdata('login_user');
        if ($login_user == NULL) {
            redirect(site_url("Accueil"));
        } else {
            $this->load->library('PHPExcel');
            $statut = $this->session->userdata('statut');
            $date_min = $this->session->userdata('date_min');
            $date_max = $this->session->userdata('date_max');
            $type = $this->session->userdata("type");


            $libelle = "EXPORT";
            $description = "L'utilisateur exporte un fichier des transactions";
            $this->globalModel->traces($libelle, $description, NULL);

            // create file name
            $fileName = 'ABI-' . time();
            // load excel library
            $empInfo = $this->transModel->getTransExport($statut, $type, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));

            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            // set Header
            $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'SycaReference');
            $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'AbiReferrence');
            $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Montant');
            $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Mobile');
            $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Status');
            $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'DateTransactions');
            // set Row      
            // set Row
            $rowCount = 2;
            foreach ($empInfo as $element) {
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['reference_syca']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['atlantis_ref']);

                $transaction_montant = number_format($element['montant_trans'], 0, '.', ' ');
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $transaction_montant);

                if ($element['statut_trans'] == 'S') {
                    $statut_trans = "Succès";
                } elseif ($element['statut_trans'] == 'P') {
                    $statut_trans = "En attente";
                } else {
                    $statut_trans = "Echec";
                }

                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['mobile_trans']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $statut_trans);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['date_create_trans']);
                $rowCount++;
            }

            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $fileName . '.xlsx"');
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
        }

    }


    public function rechercher()
    {
        $login_user = $this->session->userdata('login_user');
        if ($login_user == NULL) {
            redirect(site_url("Accueil"));
        } else {

            $date_min = $this->input->post('date_min');
            $date_max = $this->input->post('date_max');
            $statut = $this->input->post('statut');
            $search = 'search';
            $type = $this->session->userdata("type");

            if (DateTime::createFromFormat('Y-m-d', $date_min) == FALSE OR
                DateTime::createFromFormat('Y-m-d', $date_max) == FALSE
            ) {
                // it's a date
                $this->session->set_flashdata('error_E', 'Echec, Format de date incorrect !');
                redirect(site_url('Transactions'));
            }

            $filtre_date = array('date_min' => $date_min, 'date_max' => $date_max, 'search' => $search, 'statut' => $statut);
            $this->session->set_userdata($filtre_date);

            $query = $this->transModel->getTransactions($statut, $type, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));

            $data = array('affiche_trans' => $query,
                'page_title' => $this->lang->line('Accueil_title')
            );

            if ($type == "VIE") {
                $data['page'] = "transactions_vie";
                $this->load->view('communs/transactions_vie', $data);
            } else {
                $data['page'] = "transactions_iard";
                $this->load->view('communs/transactions_iard', $data);
            }

        }
    }

    public function getTokens()
    {
        header("Access-Control-Allow-Origin: *");
        $chiffre = $this->input->post('chiffre');
        $type = $this->input->post('type');

        $paramtoken = array(
            "montant" => (int)$chiffre,
            "curr" => "XOF"
        );

        $headers = array(
            'X-SYCA-MERCHANDID: C_5696ADED4C7FD',
            'X-SYCA-APIKEY: pk_syca_c29210c11f69e50e16a0ba472638b9d05641ae24',
            'X-SYCA-REQUEST-DATA-FORMAT: JSON',
            'X-SYCA-RESPONSE-DATA-FORMAT: JSON'
        );

        //$url = "https://secure.sycapay.com/login";
        $url = "https://secure.sycapay.net/login";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $data_string = json_encode($paramtoken);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        $response = json_decode(curl_exec($ch), TRUE);
        curl_close($ch);
        if ($response['code'] == 0) {
            $token = $response['token'];
        }

        if ($token) {
            $prefix = "";
            if ($type == "VIE") {
                $prefix = "V_";
            } else if ($type == "IARD") {
                $prefix = "I_";
            } else {
                exit("");
            }

            $commande = $this->transModel->Guid($prefix);

            $ent_id = $this->session->userdata("fk_ent");
            $pays_id = $this->session->userdata("fk_pays");

            $id = $this->transModel->insert($chiffre, $commande, $ent_id, $pays_id);

            //echo $response->pinId;
            echo json_encode(array(
                "token" => $token,
                "commande" => $commande
            ));
        } else {
            echo "";
        }

    }

    public function valider($id_lot)
    {
        header("Access-Control-Allow-Origin: *");

        $ent_id = $this->session->userdata("fk_ent");
        $id_user = $this->session->userdata("id_user");

        $lot = $this->refundModel->get_lot_by_id($id_lot, $ent_id, $id_user);

        if ($lot) {

            $ret = $this->refundModel->valider_lot_1($lot->id_lot, $id_user);

            if ($ret){
                $msg = "Validation lot ".$id_lot." effectuée ";
                $this->session->set_flashdata("success", $msg);
            } else {
                $msg = "Validation lot ".$id_lot." non effectuée ";
                $this->session->set_flashdata("error", $msg);
            }
            redirect(site_url("Remboursements"));

        } else {
            $msg = "Lot ".$id_lot." non trouvé !";
            $this->session->set_flashdata("error", $msg);
            redirect(site_url("Remboursements"));
        }

    }

    public function valider2($id_lot)
    {
        //$this->refundModel->inserer();
        //$som = $this->refundModel->validation($type);
        //var_dump($som["taille"]);
        //if ($som["content"] != "") {
        $id_user = $this->session->userdata("id_user");
        $ret = $this->refundModel->valider_lot_2($id_lot, $id_user, "S");

        $status = "done";
        //$valid = $som["content"] . "|*" . $id = $this->session->userdata("id_user") . "*";
        //$this->refundModel->inserer($type, $valid, $status);
        //Effectué les tranferts
        $donnees = $this->refundModel->list_dem2($id_lot);
        foreach ($donnees as $item) {

            $contact = $item->mobile_demande;
            $amount = $item->montant_demande;

            try {
				$customer_id = "C_6033A4C37BA52";
				$secretkey = 'sk_syca_0b7f76b49a2fcba42f40ff6857836b645d36dd2e';
				$apikey = 'pk_syca_aaa56629b509320441d3483a04147af521cc7978';
                $data = $amount . $customer_id . $secretkey;
                $signature = hash_hmac('sha256', $data, $secretkey);
                // Token Calling, it'll use to call transfert API
                $headers = array(
                    'X-SYCA-MERCHANDID: ' . $customer_id,
                    'X-SYCA-APIKEY: ' . $apikey,
                    'X-SYCA-REQUEST-DATA-FORMAT: JSON',
                    'X-SYCA-RESPONSE-DATA-FORMAT: JSON'
                );
                $paramsend = array(
                    "language" => "fr"
                );
                //$url = "https://secure.sycapay.net/authentification";
                $url = "http://www.sycaretail.com/delivery/v1/authentification";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_VERBOSE, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
                //curl_setopt($ch, CURLOPT_SSLVERSION ,CURL_SSLVERSION_TLSv1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                $data_json = json_encode($paramsend);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
                $response = json_decode(curl_exec($ch), TRUE);
                if (empty($response)) {
                    echo "Error Number:" . curl_errno($ch) . "<br>";
                    echo "Error String:" . curl_error($ch);
                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                }
                curl_close($ch);
                if ($response['code'] == 0) {
                    $token = $response['token'];
                    //echo $token;
                } else {
                    echo $response['code'];
                    echo $response['token'];
                }

                // Tranfert API Calling

                $headers_trs = array(
                    'X-SYCA-MERCHANDID: ' . $customer_id,
                    'TOKEN: ' . $token,
                    'AUTHORIZATION: ' . $signature,
                    'X-SYCA-REQUEST-DATA-FORMAT: JSON',
                    'X-SYCA-RESPONSE-DATA-FORMAT: JSON'
                );
                $paramsender_trs = array(
                    "orderid" => uniqid(),
                    "typetransfer" => 1,
                    "orderparty" => "sycatransf",
                    "connecteduser" => $contact,
                    "recipientident" => $contact,
                    "amount" => $amount,
                    "currency" => "XOF",
                    "language" => "FR",
                    "urlcancel" => "https://abi.sycapay.net/api/V1/cashout_return.php?id_demande=" . $item->id_demande . "&lot=" . $id_lot,
                    "urlsuccess" => "https://abi.sycapay.net/api/V1/cashout_return.php?id_demande=" . $item->id_demande . "&lot=" . $id_lot,
                    "recipientmobile" => "+225" . $contact,
                    "commentaire" => "Remboursement ALLIANZ"
                );
                //$url = "https://secure.sycapay.net/checktransfer";
                $url = "http://www.sycaretail.com/delivery/v1/checktransfer";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_VERBOSE, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
                //curl_setopt($ch, CURLOPT_SSLVERSION ,CURL_SSLVERSION_TLSv1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers_trs);
                $data_json = json_encode($paramsender_trs);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
                $response = json_decode(curl_exec($ch), TRUE);
                if (empty($response)) {
                    echo "Error Number:" . curl_errno($ch) . "<br>";
                    echo "Error String:" . curl_error($ch);
                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                }

//				log_message("DEBUG", $response);
//				var_dump($response);
				//exit();
                //var_dump($response);
                curl_close($ch);

                if ($response["code"] == 0){
					$this->refundModel->traitement($item->id_demande, "T", $response["description"], $response["referencetransfer"]);
				} else {
					$this->refundModel->traitement($item->id_demande, "E", $response["description"], $response["referencetransfer"]);
				}

			} catch (Exception $e) {
                $response["success"] = 0;
                $response["error"] = 1;
                $response["msg"] = $e->getMessage();
                echo json_encode($response);
            }
        }


        //} else {
        //    $status = "pending";
        //    $valid = "*" . $id = $this->session->userdata("id_user") . "*";
        //    $this->refundModel->inserer($type, $valid, $status);
        //}
        redirect(site_url("Remboursements"));
    }

    public function refuser($id_lot)
    {
        header("Access-Control-Allow-Origin: *");

        $ent_id = $this->session->userdata("fk_ent");
        $id_user = $this->session->userdata("id_user");

        $lot = $this->refundModel->get_lot_by_id($id_lot, $ent_id, $id_user);

        if ($lot) {

            $ret = $this->refundModel->refuser_lot_1($lot->id_lot, $id_user);

            if ($ret){
                $msg = "Validation lot ".$id_lot." effectuée ";
                $this->session->set_flashdata("success", $msg);
            } else {
                $msg = "Validation lot ".$id_lot." non effectuée ";
                $this->session->set_flashdata("error", $msg);
            }
            redirect(site_url("Remboursements"));

        } else {
            $msg = "Lot ".$id_lot." non trouvé !";
            $this->session->set_flashdata("error", $msg);
            redirect(site_url("Remboursements"));
        }

    }

    public function refuser2($id_lot)
    {
		header("Access-Control-Allow-Origin: *");

		$ent_id = $this->session->userdata("fk_ent");
		$id_user = $this->session->userdata("id_user");

		$lot = $this->refundModel->get_lot_by_id2($id_lot, $ent_id, $id_user);

		if ($lot) {

			$ret = $this->refundModel->refuser_lot_2($lot->id_lot, $id_user);

			if ($ret){
				$msg = "Refus du lot ".$id_lot." effectuée ";
				$this->session->set_flashdata("success", $msg);
			} else {
				$msg = "Refus du lot ".$id_lot." non effectuée ";
				$this->session->set_flashdata("error", $msg);
			}
			redirect(site_url("Remboursements"));

		} else {
			$msg = "Lot ".$id_lot." non trouvé !";
			$this->session->set_flashdata("error", $msg);
			redirect(site_url("Remboursements"));
		}
    }

    public function test_cashout()
    {
		$amount = $this->input->post("montant");
		$contact = $this->input->post("contact");

            try {
                $customer_id = "C_6033A4C37BA52";
                $secretkey = 'sk_syca_0b7f76b49a2fcba42f40ff6857836b645d36dd2e';
                $apikey = 'pk_syca_aaa56629b509320441d3483a04147af521cc7978';
                $data = $amount . $customer_id . $secretkey;
                $signature = hash_hmac('sha256', $data, $secretkey);
                var_dump($signature);
                // Token Calling, it'll use to call transfert API
                $headers = array(
                    'X-SYCA-MERCHANDID: ' . $customer_id,
                    'X-SYCA-APIKEY: ' . $apikey,
                    'X-SYCA-REQUEST-DATA-FORMAT: JSON',
                    'X-SYCA-RESPONSE-DATA-FORMAT: JSON'
                );
                $paramsend = array(
                    "language" => "fr"
                );
                //$url = "https://secure.sycapay.net/authentification";
                $url = "http://www.sycaretail.com/delivery/v1/authentification";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_VERBOSE, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
                //curl_setopt($ch, CURLOPT_SSLVERSION ,CURL_SSLVERSION_TLSv1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                $data_json = json_encode($paramsend);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
                $response = json_decode(curl_exec($ch), TRUE);
                if (empty($response)) {
                    echo "Error Number:" . curl_errno($ch) . "<br>";
                    echo "Error String:" . curl_error($ch);
                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                }
                curl_close($ch);
                if ($response['code'] == 0) {
                    $token = $response['token'];
                    //echo $token;
                } else {
                    echo $response['code'];
                    echo $response['token'];
                }
				var_dump($response);
                // Tranfert API Calling

                $headers_trs = array(
                    'X-SYCA-MERCHANDID: ' . $customer_id,
                    'TOKEN: ' . $token,
                    'AUTHORIZATION: ' . $signature,
                    'X-SYCA-REQUEST-DATA-FORMAT: JSON',
                    'X-SYCA-RESPONSE-DATA-FORMAT: JSON'
                );
                $paramsender_trs = array(
                    "orderid" => uniqid(),
                    "typetransfer" => 1,
                    "orderparty" => "sycatransf",
                    "connecteduser" => $contact,
                    "recipientident" => $contact,
                    "amount" => $amount,
                    "currency" => "XOF",
                    "language" => "FR",
                    "urlcancel" => "fff",
                    "urlsuccess" => "fff",
                    "recipientmobile" => "+225" . $contact,
                    "commentaire" => "Remboursement ALLIANZ"
                );
                //$url = "https://secure.sycapay.net/checktransfer";
                $url = "http://www.sycaretail.com/delivery/v1/checktransfer";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_VERBOSE, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
                //curl_setopt($ch, CURLOPT_SSLVERSION ,CURL_SSLVERSION_TLSv1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers_trs);
                $data_json = json_encode($paramsender_trs);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
                $response = json_decode(curl_exec($ch), TRUE);
                if (empty($response)) {
                    echo "Error Number:" . curl_errno($ch) . "<br>";
                    echo "Error String:" . curl_error($ch);
                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                }

				var_dump($response);
				//exit();
                //var_dump($response);
                curl_close($ch);
            } catch (Exception $e) {
                $response["success"] = 0;
                $response["error"] = 1;
                $response["msg"] = $e->getMessage();
                echo json_encode($response);
            }


    }


    public function ajax_list()
    {
        $search = $this->session->userdata('search');
        //$type = "VIE";

        if (isset($search)) {
            $statut = $this->session->userdata('statut');
            $date_min = $this->session->userdata('date_min');
            $date_max = $this->session->userdata('date_max');

            $list = $this->refundModel->get_datatables($statut, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));

            $data = array();
            $no = $_POST['start'];
            foreach ($list as $sous) {
                $no++;
                $row = array();
                $row[] = $sous->id_lot;
                $row[] = $sous->nbre_remb;
                $row[] = $sous->date_ajout_lot;
                //$row[] = $sous->type_lot;

                if ($sous->statut_lot == 'E') {
                    $statut_trans = '<span style=\'color:red;\'>' . $this->lang->line('label_echec') . '</span>';
                } elseif ($sous->statut_lot == 'S') {
                    $statut_trans = '<span style=\'color:green;\'>' . $this->lang->line('label_succes') . '</span>';
                } elseif ($sous->statut_lot == 'M') {
                    $statut_trans = '<span style=\'color:darkorange;\'>' . "Mixte" . '</span>';
                } elseif ($sous->statut_lot == 'P1') {
                    $statut_trans = '<span style=\'color:blue;\'>' . "En attente de votre validation" . '</span>';
                } elseif ($sous->statut_lot == 'P2') {

                    $role_fk = $this->session->userdata('role_fk');

                    if ($role_fk == "3"){
                        $statut_trans = '<span style=\'color:blue;\'>' . "En attende de validation du CC" . '</span>';
                    } elseif ($role_fk == "4"){
                        $statut_trans = '<span style=\'color:blue;\'>' . "En attende de votre validation" . '</span>';
                    }

                } elseif ($sous->statut_lot == 'R1') {

					$role_fk = $this->session->userdata('role_fk');

					if ($role_fk == "3"){
						$statut_trans = '<span style=\'color:red;\'>' . "Réfusé" . '</span>';
					} elseif ($role_fk == "4"){
						$statut_trans = '<span style=\'color:red;\'>' . "Réfusé par le trésorier" . '</span>';
					}
                } elseif ($sous->statut_lot == 'R2') {

                    $role_fk = $this->session->userdata('role_fk');

                    if ($role_fk == "3"){
                        $statut_trans = '<span style=\'color:red;\'>' . "Réfusé par le CC" . '</span>';
                    } elseif ($role_fk == "4"){
                        $statut_trans = '<span style=\'color:red;\'>' . "Réfusé" . '</span>';
                    }

                } else {
                    $statut_trans = '<span style=\'color:blue;\'>' . $this->lang->line('label_pending') . '</span>';
                }

                $row[] = $statut_trans;

                if ($sous->statut_lot == 'E') {
                    $row[] = "<a href='".site_url("Remboursements/demandes/".$sous->id_lot)."' title='Liste des demandes'><span class='fa fa-list blue'></span></a>";
                } elseif ($sous->statut_lot == 'M') {
                    $row[] = "<a href='".site_url("Remboursements/demandes/".$sous->id_lot)."' title='Liste des demandes'><span class='fa fa-list blue'></span></a>";
                } elseif ($sous->statut_lot == 'S') {
                    $row[] = "<a href='".site_url("Remboursements/demandes/".$sous->id_lot)."' title='Liste des demandes'><span class='fa fa-list blue'></span></a>";
                    $statut_trans = '<span style=\'color:green;\'>' . $this->lang->line('label_succes') . '</span>';
                } elseif ($sous->statut_lot == 'P1') {
                    $row[] = "<a href='" . site_url("Remboursements/valider/".$sous->id_lot) . "' title='Valider le lot'><span class='fa fa-check green'></span></a>&nbsp;&nbsp;&nbsp;<a href='" . site_url("Remboursements/refuser/".$sous->id_lot) . "'><span class='fa fa-times red'></span></a>&nbsp;&nbsp;&nbsp;<a href='".site_url("Remboursements/demandes/".$sous->id_lot)."' title='Liste des demandes'><span class='fa fa-list blue'></span></a>";
                } elseif ($sous->statut_lot == 'P2') {

                    $role_fk = $this->session->userdata('role_fk');

                    if ($role_fk == "3"){
                        $row[] = "<a href='".site_url("Remboursements/demandes/".$sous->id_lot)."' title='Liste des demandes'><span class='fa fa-list blue'></span></a>";
                    } elseif ($role_fk == "4"){
                        $row[] = "<a href='" . site_url("Remboursements/valider2/".$sous->id_lot) . "' title='Valider le lot'><span class='fa fa-check green'></span></a>&nbsp;&nbsp;&nbsp;<a href='" . site_url("Remboursements/refuser2/".$sous->id_lot) . "'><span class='fa fa-times red'></span></a>&nbsp;&nbsp;&nbsp;<a href='".site_url("Remboursements/demandes/".$sous->id_lot)."' title='Liste des demandes'><span class='fa fa-list blue'></span></a>";
                    }

                } elseif ($sous->statut_lot == 'R1') {
					$row[] = "<a href='".site_url("Remboursements/demandes/".$sous->id_lot)."' title='Liste des demandes'><span class='fa fa-list blue'></span></a>";
                } elseif ($sous->statut_lot == 'R2') {
					$row[] = "<a href='".site_url("Remboursements/demandes/".$sous->id_lot)."' title='Liste des demandes'><span class='fa fa-list blue'></span></a>";
				}
                //$row[] = $sous->date_create_trans;

                $data[] = $row;
            }

            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->refundModel->count_all($statut, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
                "recordsFiltered" => $this->refundModel->count_filtered($statut, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
                "data" => $data,
            );

            //output to json format
            echo json_encode($output);
        } else {

            $date = new DateTime();
            $date_min = $date->format('Y-m-01');
            $date_max = $date->format('Y-m-t');
            $statut = '0';

            $list = $this->refundModel->get_datatables($statut, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max)));

            $data = array();
            $no = $_POST['start'];
            foreach ($list as $sous) {
                $no++;
                $row = array();
                $row[] = $sous->id_lot;
                $row[] = $sous->nbre_remb;
                $row[] = $sous->date_ajout_lot;

                if ($sous->statut_lot == 'E') {
                    $statut_trans = '<span style=\'color:red;\'>' . $this->lang->line('label_echec') . '</span>';
                } elseif ($sous->statut_lot == 'S') {
                    $statut_trans = '<span style=\'color:green;\'>' . $this->lang->line('label_succes') . '</span>';
                } elseif ($sous->statut_lot == 'M') {
					$statut_trans = '<span style=\'color:darkorange;\'>' . "Mixte" . '</span>';
				} elseif ($sous->statut_lot == 'P1') {
                    $statut_trans = '<span style=\'color:blue;\'>' . "En attente de votre validation" . '</span>';
                } elseif ($sous->statut_lot == 'P2') {

                    $role_fk = $this->session->userdata('role_fk');

                    if ($role_fk == "3"){
                        $statut_trans = '<span style=\'color:blue;\'>' . "En attende de validation du CC" . '</span>';
                    } elseif ($role_fk == "4"){
                        $statut_trans = '<span style=\'color:blue;\'>' . "En attende de votre validation" . '</span>';
                    }

                } elseif ($sous->statut_lot == 'R1') {

					$role_fk = $this->session->userdata('role_fk');

					if ($role_fk == "3"){
						$statut_trans = '<span style=\'color:red;\'>' . "Réfusé" . '</span>';
					} elseif ($role_fk == "4"){
						$statut_trans = '<span style=\'color:red;\'>' . "Réfusé par le trésorier" . '</span>';
					}
				} elseif ($sous->statut_lot == 'R2') {

					$role_fk = $this->session->userdata('role_fk');

					if ($role_fk == "3"){
						$statut_trans = '<span style=\'color:red;\'>' . "Réfusé par le CC" . '</span>';
					} elseif ($role_fk == "4"){
						$statut_trans = '<span style=\'color:red;\'>' . "Réfusé" . '</span>';
					}

				} else {
                    $statut_trans = '<span style=\'color:blue;\'>' . $this->lang->line('label_pending') . '</span>';
                }

                $row[] = $statut_trans;

				if ($sous->statut_lot == 'E') {
					$row[] = "<a href='".site_url("Remboursements/demandes/".$sous->id_lot)."' title='Liste des demandes'><span class='fa fa-list blue'></span></a>";
				} elseif ($sous->statut_lot == 'M') {
					$row[] = "<a href='".site_url("Remboursements/demandes/".$sous->id_lot)."' title='Liste des demandes'><span class='fa fa-list blue'></span></a>";
				} elseif ($sous->statut_lot == 'S') {
					$row[] = "<a href='".site_url("Remboursements/demandes/".$sous->id_lot)."' title='Liste des demandes'><span class='fa fa-list blue'></span></a>";
					$statut_trans = '<span style=\'color:green;\'>' . $this->lang->line('label_succes') . '</span>';
				} elseif ($sous->statut_lot == 'P1') {
                    $row[] = "<a href='" . site_url("Remboursements/valider/".$sous->id_lot) . "' title='Valider le lot'><span class='fa fa-check green'></span></a>&nbsp;&nbsp;&nbsp;<a href='" . site_url("Remboursements/refuser/".$sous->id_lot) . "'><span class='fa fa-times red'></span></a>&nbsp;&nbsp;&nbsp;<a href='".site_url("Remboursements/demandes/".$sous->id_lot)."' title='Liste des demandes'><span class='fa fa-list blue'></span></a>";
                } elseif ($sous->statut_lot == 'P2') {

                    $role_fk = $this->session->userdata('role_fk');

                    if ($role_fk == "3"){
                        $row[] = "<a href='".site_url("Remboursements/demandes/".$sous->id_lot)."' title='Liste des demandes'><span class='fa fa-list blue'></span></a>";
                    } elseif ($role_fk == "4"){
                        $row[] = "<a href='" . site_url("Remboursements/valider2/".$sous->id_lot) . "' title='Valider le lot'><span class='fa fa-check green'></span></a>&nbsp;&nbsp;&nbsp;<a href='" . site_url("Remboursements/refuser2/".$sous->id_lot) . "'><span class='fa fa-times red'></span></a>&nbsp;&nbsp;&nbsp;<a href='".site_url("Remboursements/demandes/".$sous->id_lot)."' title='Liste des demandes'><span class='fa fa-list blue'></span></a>";
                    }

                } elseif ($sous->statut_lot == 'R1') {
					$row[] = "<a href='".site_url("Remboursements/demandes/".$sous->id_lot)."' title='Liste des demandes'><span class='fa fa-list blue'></span></a>";
				} elseif ($sous->statut_lot == 'R2') {
					$row[] = "<a href='".site_url("Remboursements/demandes/".$sous->id_lot)."' title='Liste des demandes'><span class='fa fa-list blue'></span></a>";
				}

                //$row[] = $sous->date_create_trans;

                $data[] = $row;
            }

            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->refundModel->count_all($statut, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
                "recordsFiltered" => $this->refundModel->count_filtered($statut, date("Y-m-d 00:00:00", strtotime($date_min)), date("Y-m-d 23:59:59", strtotime($date_max))),
                "data" => $data,
            );


            //output to json format
            echo json_encode($output);
        }
    }
}
