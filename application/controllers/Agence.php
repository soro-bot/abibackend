<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agence extends CI_Controller {

	public function __construct()
	{
  		parent::__construct();
  		$this->load->model('Global_model', 'globalModel');
  		$this->load->model('Agence_model', 'agenceModel');
  		$this->load->model('Entreprise_model', 'entModel');

		$_GET = decontaminate_data($_GET);
		$_POST = decontaminate_data($_POST);
	}

	public function index()
	{
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL) 
      {
          redirect(site_url("Accueil"));
      }
      else
      {	
            $query = $this->agenceModel->getAgences();
            if (empty($query)) {
              $query = 'query';
            }

            $libelle = "AGENCES";
            $description = "L'utilisateur consulte le menu agence";
            $this->globalModel->traces($libelle, $description, NULL);

            $data = array('affiche_agence' => $query, 
                          'modifier_agence' => "", 
                          'ajouter_agence' => "",             
                          'page_title' => $this->lang->line('Accueil_title')
                          );
          	$this->load->view('communs/agence', $data);
        }
	}

  public function addAgences()
  {
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL) 
      {
          redirect(site_url("Accueil"));
      }
      else
      { 
            $query = $this->entModel->getActiveEntreprises();
          //var_dump($query);
            $data = array('affiche_agence' => "", 
                          'modifier_agence' => "",
                          'liste_entreprises' => $query, 
                          'ajouter_agence' => "ajouter_agence",             
                          'page_title' => $this->lang->line('Accueil_title')
                          );
            $this->load->view('communs/agence', $data);
        }
  }

  public function insertAgences()
  {
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL) 
      {
          redirect(site_url("Accueil"));
      }
      else
      {     
            $nom_agence = $this->globalModel->pasDeCaractersBizares($this->input->post('nom_agence'));
            $localisation_agence = $this->globalModel->pasDeCaractersBizares($this->input->post('localisation_agence'));
            $entreprise_fk = $this->input->post('entreprise_fk');
            $query = $this->agenceModel->existeAgences($nom_agence, $entreprise_fk);

            if (empty($nom_agence) OR empty($localisation_agence) OR empty($entreprise_fk)) 
            {
                $this->session->set_flashdata('error', 'Les champs sont requis / Fields are required !');
                redirect(site_url('Agence'));
            }
            elseif (empty($query)) 
            {
                $addIng = $this->agenceModel->ajouterAgences($nom_agence, $localisation_agence, $entreprise_fk);

                if ($addIng) 
                {
                    $this->session->set_flashdata('success', $this->lang->line("notif_succes"));
                    $libelle = "ADDING";
                    $description = "L'utilisateur ajoute l'agence";
                    $this->globalModel->traces($libelle, $description, $addIng);
                }
                else
                {
                   $this->session->set_flashdata('error', 'Operation impossible !');
                }

                redirect(site_url('Agence'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Agence Existe !');
                redirect(site_url('Agence'));
            }
      }
  }

  public function disableAgence($id_agence)
  {
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL) 
      {
          redirect(site_url("Accueil"));
      }
      else
      {     
            $query = $this->agenceModel->isAgences($id_agence);
            if ($query)
            {
                $addIng = $this->agenceModel->desactiverAgences($id_agence);

                if ($addIng) 
                {
                    $this->session->set_flashdata('success', $this->lang->line("notif_succes"));
                    $libelle = "DISABLE";
                    $description = "L'utilisateur désactive l'agence";
                    $this->globalModel->traces($libelle, $description, $id_agence);
                }
                else
                {
                   $this->session->set_flashdata('error', 'Operation impossible !');
                }

                redirect(site_url('Agence'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Agence Not Exist !');
                redirect(site_url('Agence'));
            }
      }
  }

  public function enableAgence($id_agence)
  {
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL) 
      {
          redirect(site_url("Accueil"));
      }
      else
      {     
            $query = $this->agenceModel->isAgences($id_agence);
            if ($query)
            {
                $addIng= $this->agenceModel->activerAgences($id_agence);

                if ($addIng) 
                {
                    $this->session->set_flashdata('success', $this->lang->line("notif_succes"));
                    $libelle = "ENABLE";
                    $description = "L'utilisateur active l'agence";
                    $this->globalModel->traces($libelle, $description, $id_agence);
                }
                else
                {
                   $this->session->set_flashdata('error', 'Operation impossible !');
                }

                redirect(site_url('Agence'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Agence Not Exist !');
                redirect(site_url('Agence'));
            }
      }
  }

  public function majAgence($id_agence)
  {
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL) 
      {
          redirect(site_url("Accueil"));
      }
      else
      {     
            $reqAgence = $this->agenceModel->isAgences($id_agence);
            $reqEntreprise = $this->entModel->getActiveEntreprises();

            $data = array('affiche_agence' => "", 
                          'modifier_agence' => $reqAgence,
                          'liste_entreprises' => $reqEntreprise, 
                          'ajouter_agence' => "",             
                          'page_title' => $this->lang->line('Accueil_title')
                          );
            $this->load->view('communs/agence', $data);
      }
  }

  public function updateAgences()
  {
      $login_user = $this->session->userdata('login_user');
      if ($login_user == NULL) 
      {
          redirect(site_url("Accueil"));
      }
      else
      {     
            $id_agence = $this->input->post('id_agence');
            $nom_agence = $this->globalModel->pasDeCaractersBizares($this->input->post('nom_agence'));
            $localisation_agence = $this->globalModel->pasDeCaractersBizares($this->input->post('localisation_agence'));
            $entreprise_fk = $this->input->post('entreprise_fk');
            $query = $this->agenceModel->isAgences($id_agence);

            if (empty($nom_agence) OR empty($localisation_agence) OR empty($entreprise_fk)) 
            {
                $this->session->set_flashdata('error', 'Les champs sont requis / Fields are required !');
                redirect(site_url('Agence'));
            }
            elseif ($query)
            {
                $addIng= $this->agenceModel->modifierAgences($id_agence, $nom_agence, $localisation_agence, $entreprise_fk);

                if ($addIng) 
                {
                    $this->session->set_flashdata('success', $this->lang->line("notif_succes"));
                    $libelle = "MODIFIER";
                    $description = "L'utilisateur modifie l'agence";
                    $this->globalModel->traces($libelle, $description, $id_agence);
                }
                else
                {
                   $this->session->set_flashdata('error', 'Operation impossible !');
                }

                redirect(site_url('Agence'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Agence Existe !');
                redirect(site_url('Agence'));
            }
      }
  }


   public function ajax_list()
   { 
        $list = $this->agenceModel->get_datatables();

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $sous) {
          $no++;
          $row = array();
          $row[] = $sous->nom_agence;
          $row[] = $sous->localisation_agence;

          if ($sous->etat_agence == 'I')
          {
              $etat_agence =  '<span style=\'color:red;\'>'.$this->lang->line('label_desactiver').'</span>';
              $activeStrap =  "btn btn-xs btn-default";
              $desactiveStrap =  "btn btn-xs btn-danger";

          }
          else
          {   
              $activeStrap = "btn btn-xs btn-primary";
              $desactiveStrap =  "btn btn-xs btn-default";
              $etat_agence =  '<span style=\'color:blue;\'>'.$this->lang->line('label_activer').'</span>';
          }

          $row[] = $etat_agence;
          $row[] = $sous->date_create_agence;                

          $row[] = '<a class="btn-success btn-xs" href="'.site_url("Agence/majAgence")."/".$sous->id_agence.'"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;&nbsp;<div class="btn-group btn-toggle"><a href="'.site_url('Agence/enableAgence')."/".$sous->id_agence.'"  class="'.$activeStrap.'" onClick="return confirm("'.$this->lang->line('confirm_modal_text').'")">'.$this->lang->line('label_activer').'</a><a href="'.site_url("Agence/disableAgence")."/".$sous->id_agence.'" class="'.$desactiveStrap.'" onClick="return confirm("'.$this->lang->line('confirm_modal_text').'")">'.$this->lang->line('label_desactiver').'</a></div>';

          $data[] = $row;
        }

        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->agenceModel->count_all(),
                "recordsFiltered" => $this->agenceModel->count_filtered(),
                "data" => $data,
                );
        
        //output to json format
        echo json_encode($output);
    }
}
